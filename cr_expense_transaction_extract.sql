set define on;
set echo off;
set head off;
set feedback off;
set trimspool on;
set pagesize 0;
set linesize 500;

spool TransExpenses.lis;

select the_data.dataline from
 (select 1, 'TransactionUniqueId'||'|'||   
'TransactionDate'||'|'||
'PersonUniqueID'||'|'||
'Name'||'|'||
'AmountSpent'||'|'||
'CurrencySpent'||'|'||
'LocationSpent'||'|'||
'ExpenseCode'||'|'||
'MatterNumber'||'|'||
'BusinessPurpose'||'|'||
'Description'||'|'||
'AppendToDescription'||'|'||
'ExtraText'||'|'||
'IsFirmPaid'||'|'||
'IsAmountDisabled'||'|'||
'IsCurrencyDisabled'||'|'|| 
'IsDeleteDisabled'||'|'||
'IsReceiptNeeded'||'|'||
'VAT'||'|'||
'VATAmount'||'|'||
'HasVATReceipt'||'|'||
'UserDefinedFields'||'|'||
'UserDefinedField2' as dataline
 from dual
union
select 2, null||'|'||   
to_char(sysdate,'mm/dd/yyyy')||'|'||
fzreexp_vend_id||'|'||
'Cash Advance Request'||'|'||
fzrexpd_check_amt * -1||'|'||
'USD'||'|'||
null ||'|'||
'Cash Advance Request'||'|'||
fzreexp_index||'|'||
fzreexp_inv_desc||'|'||
null||'|'||
null||'|'||
null||'|'||
null||'|'||
null||'|'||
null||'|'||
null||'|'||
null||'|'||
null||'|'||
null||'|'||
null||'|'||
null||'|'||
null
from fzrexpd,fzreexp
where fzreexp_inv_num = fzrexpd_inv_num
and fzreexp_feed_doc_id = fzrexpd_feed_doc_id
and fzreexp_inv_type = 'ADV'
and fzreexp_status = 'C'
and trunc(fzreexp_feed_date) = trunc(sysdate)
and trunc(fzrexpd_feed_date) = trunc(sysdate)
union
select 3, null||'|'||
to_char(sysdate,'mm/dd/yyyy')||'|'||
fzreexp_vend_id||'|'||
'Cash Advance Request'||'|'||
tbrmisd_amount||'|'||
'USD'||'|'||
null||'|'||
'Cash Advance Request'||'|'||
fzreexp_index||'|'||
fzreexp_inv_desc||'|'||
null||'|'||
null||'|'||
null||'|'||
null||'|'||
null||'|'||
null||'|'||
null||'|'||
null||'|'||
null||'|'||
null||'|'||
null||'|'||
null||'|'||
null
from fzreexp, taismgr.tbrmisd
where 'CR'||tbrmisd_payment_id = fzreexp_inv_num
and fzreexp_inv_type = 'ADV'
and fzreexp_status = 'C'
and tbrmisd_charge_detail_code = 'CRAR'
and trunc(tbrmisd_cshr_end_date) = trunc(sysdate)
order by 1) the_data;

spool off;
exit;

