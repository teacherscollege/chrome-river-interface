set serveroutput on;

declare
p_msg varchar2(300);

begin
  fzkeexp.p_reset_error(p_msg,'A');
  dbms_output.put_line(p_msg);
exception
  when others then dbms_output.put_line(sqlerrm);
end;
/
show errors;
exit;

