CREATE OR REPLACE PACKAGE BODY baninst1.fzkeexp
AS
    /*******************************************************************************

       NAME:       FZKEEXP

       PURPOSE:    This package is Chrome River Interface to Banner.

                   Procedure p_ProcessInvoice and creates Invoice calling Banner Invoice API
                                   fb_invoice_header.p_create_header
                                   fb_invoice_item.p_create_item
                                   fb_invoice_acctg.p_create_accounting


       REVISIONS:
       Ver  Date        Author        Description
       ---  ----------  ------------  ------------------------------------
       1.0  06/06/2017  D. Semar      Initial.
            02/27/2018  D. Semar      Production ready
            03/19/2018  D. Semar      Added calls to p_send_error_email
            03/20/2018  D. Semar      Enhanced error email messages
            04/13/2018  D. Semar      Check if vendor record exists and create if not found <DGS 041318>
            05/11/2018  D. Semar      Back-date transaction date during the first close of year end <DGS 05112018>
            05/29/2018  D. Semar      Change accounting line creation to sum by foapl <DGS 05292018>
            06/07/2018  D. Semar      Added p_Reprocess_DupeBatch to create a new batch and reprocess a non-dupe batch which
                                      errored out with the duplicate batch error message.  This occurs when two or more external batches
                                      are processed under the same batch id and one of the external batches has previously been
                                      imported. This utility is run from Sql Developer. <DGS 06042018>
           08/20/2018   D. Semar      Tighten up error message handling for fatal error 'PL/SQL numeric or value error..' <DGS 08202018>
           06/06/2019   J. Asbury     Added new function f_chrome_multi_sup_code to
                                      allow for multiple supervisors in CR.  Also
                                      modifications to function f_daa_lkup
           05/12/2021   J. Asbury     Added new cursor--c_sub_with_mult_sups-- in function f_chrome_multi_sup_code to loop through all
                                      subordinates of employee and check if any one had more than supervisor.  This impacts how employee
                                      is coded in entity1 field in user file
           06/06/2024   J. Asbury     Per INC0241665 replaced Kristin Gorski - KG2366 replaced by Seth Moncrease - SRM2250                            
    *******************************************************************************/
    --
    g_header_rec   fb_invoice_header.invoice_header_rec;
    g_comm_rec     fb_invoice_item.invoice_item_rec;
    g_acctg_rec    fb_invoice_acctg.invoice_acctg_rec;
    --
    g_cr_memo_ind  fabinvh.fabinvh_cr_memo_ind%TYPE;
    g_pmt_due_date fabinvh.fabinvh_pmt_due_date%TYPE;
    --
    g_single_acctg_ind fpbpohd.fpbpohd_single_acctg_ind%TYPE;
    --
    g_inv_user     fabinvh.fabinvh_user_id%TYPE := 'APMASTER';
    g_inv_code     fabinvh.fabinvh_code%TYPE;
    g_vend_inv_code fabinvh.fabinvh_vend_inv_code%TYPE;
    g_invoice_date fabinvh.fabinvh_invoice_date%TYPE;
    g_trans_date   fabinvh.fabinvh_trans_date%TYPE;
    g_bank_code    fabinvh.fabinvh_bank_code%TYPE;
    --
    g_vend_pidm    fpbpohd.fpbpohd_vend_pidm%TYPE;
    g_vend_id      spriden.spriden_id%TYPE;
    g_vend_uni     spriden.spriden_id%TYPE;
    g_vend_tnum    spriden.spriden_id%TYPE;
    g_amx_chid     spriden.spriden_id%TYPE;
    g_user_id      fobprof.fobprof_user_id%TYPE;
    g_user_name    fobprof.fobprof_user_name%TYPE;
    g_user_coas    fobprof.fobprof_coas_code%TYPE;
    g_inv_type     fzreexp.fzreexp_inv_type%TYPE;
    g_inv_prefix   VARCHAR2 (2);
    g_req_orgn     VARCHAR2 (2);

    g_nsf_appr_byp CHAR (1) := 'N';
    g_prc_name     VARCHAR2 (100);
    g_currency     VARCHAR2 (3) := 'USD';
    g_orig         VARCHAR2 (10) := 'CHROMERVR';
    g_doc_status   VARCHAR2 (1) := NULL;
    g_err_msg      VARCHAR2 (500) := NULL;
    g_atyp_seq_no  spraddr.spraddr_seqno%TYPE;
    g_atyp_code    spraddr.spraddr_atyp_code%TYPE;
    g_banner       VARCHAR2 (10);
    --VR 9/16/2019 INC0089249 g_r_rucl_code      VARCHAR(4);
    g_r_rucl_code  VARCHAR (4);

    --==============================================================================
    --
    --==============================================================================
    --
    FUNCTION f_fspd_open (p_userid VARCHAR2, p_date DATE)
        RETURN BOOLEAN
    IS
        s              VARCHAR2 (1);
    BEGIN
        SELECT ftvfspd_prd_status_ind
          INTO s
          FROM ftvfspd, fobprof
         WHERE ftvfspd_coas_code = fobprof_coas_code
           AND fobprof_user_id = UPPER (p_userid)
           AND p_date BETWEEN TRUNC (ftvfspd_prd_start_date)
                          AND TRUNC (ftvfspd_prd_end_date);

        IF s = 'O'
        THEN
            RETURN TRUE;
        ELSE
            RETURN FALSE;
        END IF;
    EXCEPTION
        WHEN OTHERS
        THEN
            RETURN FALSE;
    END;

    ----==============================================================================
    FUNCTION f_updt_status (p_batch_id IN VARCHAR2
                           ,p_crnum_in IN VARCHAR2
                           ,p_status_in IN VARCHAR2)
        RETURN VARCHAR2
    IS
        p_msg          VARCHAR2 (100) := NULL;
    BEGIN
        UPDATE fzreexp
           SET fzreexp_status = p_status_in
         WHERE fzreexp_batch_id = p_batch_id AND fzreexp_inv_num = p_crnum_in;

        p_msg := 'Updated status for' || p_crnum_in || ' to ' || p_status_in;
        RETURN p_msg;
    EXCEPTION
        WHEN OTHERS
        THEN
            p_msg := SUBSTR (SQLERRM, 1, 80) || ' for ' || p_crnum_in;
            RETURN p_msg;
    END;

    ----==============================================================================

    FUNCTION f_daa_lkup (orgn_in IN VARCHAR2, pidm_in NUMBER)
        RETURN VARCHAR2
    IS
        daa_uni        VARCHAR2 (9);
        orgn_ts        ftvorgn.ftvorgn_orgn_code%TYPE;
    BEGIN
        /* Override orgn_in for certain faculty */
        SELECT CASE
                   WHEN pidm_in = 64472
                   THEN
                       --Sharon Kagan
                       '111883'
                   WHEN pidm_in = 215031
                   THEN
                       --Douglas Ready
                       '117109'
                   WHEN pidm_in = 270548
                   THEN
                       --Peter Bergman
                       '117121'
                   --5/6/19 by JA: removed override for Dr. Han - INC0086206
                   --her primary job org of 111429 will be passed in and set in override below
                   --to UNI 'PG2201'
                   --   when pidm_in = 242186  then '117172'
                   WHEN pidm_in = 82586
                   THEN
                       --Jeanne Brooks-Gunn
                       '111883'
                   WHEN pidm_in = 942
                   THEN
                       --Thomas Bailey
                       '117121'
                   WHEN pidm_in = 63028
                   THEN
                       --Lening Liu
                       '117174'
                   --5/6/19 by JA: removed override org here for Dr. Han - INC0086206
                   --Her primary job org of 111429 is being passed in and set in override below
                   --for org to PG2201 who Dr. Han reports to, which is not correct.
                   --So there is another override at bottom of this function that will set her
                   --supervisor as UNI 'TJ2180'
                   --when pidm_in = 242186  then '117174'
                   --5/6/19 by JA: remove override for Gary Natriello because it was null
                   --anyway; the supervisor on his primary job will be used
                   --when pidm_in = 1138  then null
                   ELSE
                       orgn_in
               END
          INTO orgn_ts
          FROM DUAL;

        SELECT CASE
                   WHEN orgn_ts IN ('111401'
                                   ,'111411'
                                   ,'111412'
                                   ,'111413'
                                   ,'111414'
                                   ,'111415'
                                   ,'111416'
                                   ,'111417'
                                   ,'111418'
                                   ,'111419'
                                   ,'111420'
                                   ,'111421'
                                   ,'111422'
                                   ,'111423'
                                   ,'111424'
                                   ,'111425'
                                   ,'111426'
                                   ,'111427'
                                   ,'111428'
                                   ,'111429'
                                   ,'111430'
                                   ,'111431'
                                   ,'111432'
                                   ,'111433'
                                   ,'111434'
                                   ,'111435'
                                   ,'111451'
                                   ,'111452'
                                   ,'111453'
                                   ,'111454'
                                   ,'111462'
                                   ,'111463'
                                   ,'111464'
                                   ,'111466'
                                   ,'111467'
                                   ,'111468'
                                   ,'111469'
                                   ,'111470')
                   THEN
                       --Updated to Emily Riddles 9/22/21 per INC0136036
                       --RIDDLES1 ER2748 309682 T90603907 Riddles Emily Ruth
                       'ER2748'
                   --'PG2201'
                   WHEN orgn_ts IN ('111701'
                                   ,'111711'
                                   ,'111712'
                                   ,'111713'
                                   ,'111714'
                                   ,'111715'
                                   ,'111716'
                                   ,'111717'
                                   ,'111718'
                                   ,'111719'
                                   ,'111720'
                                   ,'111721'
                                   ,'111722'
                                   ,'111723'
                                   ,'111724'
                                   ,'111725'
                                   ,'111726'
                                   ,'111727')
                   THEN
                       'MEL31'
                   WHEN orgn_ts IN ('111201'
                                   ,'111211'
                                   ,'111212'
                                   ,'111213'
                                   ,'111214'
                                   ,'111215'
                                   ,'111216'
                                   ,'111217'
                                   ,'111261'
                                   ,'111262')
                   THEN
                       'EDD2001'
                   WHEN orgn_ts IN ('111301'
                                   ,'111311'
                                   ,'111312'
                                   ,'111313'
                                   ,'111314'
                                   ,'111315'
                                   ,'111316'
                                   ,'111317'
                                   ,'111318'
                                   ,'111319'
                                   ,'111320'
                                   ,'111351'
                                   ,'111352'
                                   ,'111353'
                                   ,'111354'
                                   ,'111355'
                                   ,'111361')
                   THEN
                       'AMA2119'
                   WHEN orgn_ts IN ('111001'
                                   ,'111011'
                                   ,'111012'
                                   ,'111013'
                                   ,'111014'
                                   ,'111015'
                                   ,'111016'
                                   ,'111017')
                   THEN
                       'LMF2174'
                   WHEN orgn_ts IN ('111501'
                                   ,'111511'
                                   ,'111512'
                                   ,'111513'
                                   ,'111514'
                                   ,'111515'
                                   ,'111516'
                                   ,'111517'
                                   ,'111518'
                                   ,'111551'
                                   ,'111561')
                   THEN
                       'AS5312'
                   WHEN orgn_ts IN ('111801'
                                   ,'111811'
                                   ,'111812'
                                   ,'111813'
                                   ,'111814'
                                   ,'111815'
                                   ,'111816')
                   THEN
                       'CJG2164'
                   WHEN orgn_ts IN ('111101', '111111', '111112', '111113')
                   THEN
                       'DM136'
                   WHEN orgn_ts IN ('111601'
                                   ,'111611'
                                   ,'111612'
                                   ,'111613'
                                   ,'111614'
                                   ,'111615'
                                   ,'111616'
                                   ,'111617'
                                   ,'111618'
                                   ,'111619'
                                   ,'111620'
                                   ,'111621'
                                   ,'111651')
                   THEN
                       'SRM2250'
                   WHEN orgn_ts IN ('111901'
                                   ,'111911'
                                   ,'111912'
                                   ,'111913'
                                   ,'111914'
                                   ,'111915'
                                   ,'111916'
                                   ,'111917'
                                   ,'111918'
                                   ,'111919'
                                   ,'111920'
                                   ,'111921'
                                   ,'111922'
                                   ,'111923'
                                   ,'111924'
                                   ,'111925'
                                   ,'111926'
                                   ,'111927'
                                   ,'111928'
                                   ,'111929'
                                   ,'111930'
                                   ,'111931'
                                   ,'111932'
                                   ,'111933'
                                   ,'111934'
                                   ,'111935'
                                   ,'111936'
                                   ,'111937'
                                   ,'111938'
                                   ,'111939'
                                   ,'111940'
                                   ,'111941'
                                   ,'111942'
                                   ,'111943'
                                   ,'111944'
                                   ,'111945'
                                   ,'111946'
                                   ,'111947'
                                   ,'111948')
                   THEN
                       'DW2237'
                   ELSE
                       NULL
               END
          INTO daa_uni
          FROM DUAL;

        -- override the overide
        IF pidm_in = 976
        THEN
            --Lucy Calkins
            daa_uni := 'BLN6';
        --Beth Neville
        END IF;

        IF pidm_in = 942
        THEN
            --Thomas Bailey
            daa_uni := 'CME11';
        --Henry Perkowski
        END IF; --from 'LTR2'

        IF pidm_in = 64472
        THEN
            --Sharon Kagan
            daa_uni := 'AMP155';
        --Aaron Pallas
        END IF;

        IF pidm_in = 270548
        THEN
            --Peter Bergman
            daa_uni := 'LTR2';
        --Lisa Rothman
        END IF;

        --11/17/20 by JA: added below
        IF pidm_in = 381868
        THEN
            --Stephanie Rowley
            daa_uni := 'TB3';
        --Thomas Bailey
        END IF;

        --5/7/19 by JA: updated Dr. Han override per INC0086206; she is a chair and
        --the org on her primary job would mean that she was being assigned as supervisor
        --to herself and the org in override above is not correct because PG2201 reports
        --to Dr. Han
        --8/15/19 by JA: override for Dr. Han changed to Stephanie Rowley per INC0092368
        --4/18/22 by JA: removing override for Dr. Han, per Jennie Wilson email
        --        IF pidm_in = 242186
        --        THEN
        --            daa_uni := 'SJR2192';
        --Stephanie Rowley
        --daa_uni := 'TJ2180';
        --Thomas James
        --END IF;

        --if pidm_in = 242186 then daa_uni := 'DS3010'; end if;
        IF pidm_in = 82586
        THEN
            --Jeanne Brooks-Gunn
            daa_uni := 'JEC34';
        --James Corter
        END IF;

        IF pidm_in = 63028
        THEN
            --Lening Liu
            daa_uni := 'DS3010';
        --Debi Slatkin
        END IF;

        --8/7/19 by JA: Added entry for Douglas Ready per Jennie Wilson
        IF pidm_in = 215031
        THEN
            --Douglas Ready
            daa_uni := 'AMP155';
        --Aaron Pallas
        END IF;

        --12/17/20 by JA: Added entry for William Baldwin per Jennie Wilson
        IF pidm_in = 60886
        THEN
            --Willam Baldwin
            daa_uni := 'DW2237';
        --Deborah Walden
        END IF;



        RETURN daa_uni;
    EXCEPTION
        WHEN OTHERS
        THEN
            RETURN NULL;
    END;

    ----==============================================================================

    PROCEDURE p_get_email_route (p_msg_in IN VARCHAR2
                                ,p_route_to   OUT VARCHAR2)
    IS
    BEGIN
        p_route_to :=
            CASE
                WHEN p_msg_in LIKE '%Fund code%'
                THEN
                    CASE
                        WHEN SUBSTR (p_msg_in
                                    ,INSTR (p_msg_in, 'Fund code') + 10
                                    ,1) =
                             '5'
                        THEN
                            'CR_GRANTS_ERROR'
                        ELSE
                            'CR_FINANCE_ERROR'
                    END
                WHEN p_msg_in LIKE '%Organization code%not valid%'
                THEN
                    CASE
                        WHEN SUBSTR (p_msg_in
                                    ,INSTR (p_msg_in, 'Fund code') + 18
                                    ,1) =
                             '5'
                        THEN
                            'CR_GRANTS_ERROR'
                        ELSE
                            'CR_FINANCE_ERROR'
                    END
                WHEN p_msg_in LIKE '%Index%invalid%'
                THEN
                    CASE
                        WHEN SUBSTR (p_msg_in
                                    ,INSTR (p_msg_in, 'Index') + 6
                                    ,1) =
                             '5'
                        THEN
                            'CR_GRANTS_ERROR'
                        ELSE
                            'CR_FINANCE_ERROR'
                    END
                WHEN p_msg_in LIKE '%Invalid value for vendor%'
                THEN
                    'CR_PO_OFFICE'
                WHEN p_msg_in LIKE '%Vendor address%'
                THEN
                    'CR_PO_OFFICE'
                WHEN p_msg_in LIKE '%Addr Err:%'
                THEN
                    'CR_PO_OFFICE'
                ELSE
                    'CR ERROR EMAIL'
            END;
    EXCEPTION
        WHEN OTHERS
        THEN
            p_route_to := '';
    END;

    PROCEDURE p_send_error_email (p_msg VARCHAR2)
    IS
        l_from         VARCHAR2 (100);
        l_subj         VARCHAR2 (500);
        l_to           VARCHAR2 (32000);
        l_cc           VARCHAR2 (32000);
        v_mail_successful VARCHAR2 (300) := 'T';
        v_mail_errmsg  VARCHAR2 (300) := 'F';
        v_error_type   VARCHAR2 (100);
        v_error_code   VARCHAR2 (100);
        v_error_message VARCHAR2 (3000);
        v_to_list      VARCHAR2 (100);
        p_route_to     VARCHAR2 (100);
        crlf           VARCHAR2 (2) := CHR (13) || CHR (10);
    BEGIN
        BEGIN
            SELECT SYS_CONTEXT ('USERENV', 'DB_NAME') INTO g_banner FROM DUAL;
        EXCEPTION
            WHEN OTHERS
            THEN
                NULL;
        END;

        -- VR Start PRJ0014600
        BEGIN
            IF LENGTH (NVL (g_vend_id, '')) < 9 -- FZREEXP_VEND_ID is not a T# its a uni
            THEN
                g_vend_tnum :=
                    tcapp.tc_argos_util.tc_get_t_from_uni (g_vend_id);
            ELSIF LENGTH (NVL (g_vend_id, '')) = 9
            THEN
                g_vend_tnum := g_vend_id;
            END IF;
        EXCEPTION
            WHEN OTHERS
            THEN
                NULL;
        END;

        --
        --
        BEGIN
            IF LENGTH (NVL (g_vend_id, '')) < 9 -- FZREEXP_VEND_ID is not a T# its a uni
            THEN
                g_vend_uni := g_vend_id;
            ELSIF LENGTH (NVL (g_vend_id, '')) = 9
            THEN
                g_vend_uni := g_amx_chid;
            END IF;
        EXCEPTION
            WHEN OTHERS
            THEN
                NULL;
        END;

        -- VR END PRJ0014600
        l_subj :=
            'CHROME RIVER: ' || g_banner || ' Error in request (FZKEEXP) ';
        --
        /* determine where to route the email to */

        p_get_email_route (p_msg, p_route_to);
        v_to_list := NVL (p_route_to, 'CR ERROR EMAIL');

        --    tcapp.util_msg.LogEntry('Send Mail To: '||v_to_list);

        FOR e
            IN (  SELECT ftvsdat_title email, ftvsdat_code_level code_level
                    FROM ftvsdat
                   WHERE ftvsdat_sdat_code_entity = 'FZKEEXP'
                     AND UPPER (ftvsdat_sdat_code_attr) = v_to_list
                     AND UPPER (ftvsdat_sdat_code_opt_1) = 'FROM'
                     AND ftvsdat_status_ind = 'A'
                     AND ftvsdat_eff_date <= SYSDATE
                     AND NVL (ftvsdat_term_date, SYSDATE + 1) > SYSDATE
                     AND NVL (ftvsdat_nchg_date, SYSDATE + 1) > SYSDATE
                ORDER BY ftvsdat_code_level)
        LOOP
            l_from := e.email;

            FOR t
                IN (SELECT ftvsdat_title     email
                      FROM ftvsdat
                     WHERE ftvsdat_sdat_code_entity = 'FZKEEXP'
                       AND UPPER (ftvsdat_sdat_code_attr) = v_to_list
                       AND UPPER (ftvsdat_sdat_code_opt_1) = 'TO'
                       AND ftvsdat_status_ind = 'A'
                       AND ftvsdat_eff_date <= SYSDATE
                       AND NVL (ftvsdat_term_date, SYSDATE + 1) > SYSDATE
                       AND NVL (ftvsdat_nchg_date, SYSDATE + 1) > SYSDATE
                       AND ftvsdat_code_level = e.code_level)
            LOOP
                tcapp.util_msg.logentry
                (
                    'Send Mail To: ' || t.email || ' ' || l_subj
                );

                IF l_from IS NOT NULL AND t.email IS NOT NULL
                THEN
                    sokemal.p_sendemail
                    (
                        email_addr  => t.email
                       ,email_to_name => NULL
                       ,email_from_addr => l_from
                       ,email_from_name =>
                            'Chrome River ' || g_banner || ' - NO REPLY'
                       ,email_host  => 'SUNMAIL.TC.COLUMBIA.EDU'
                       ,email_subject => l_subj
                       ,email_message =>
                               p_msg
                            || crlf
                            || crlf
                            || --VR Start PRJ0014600
                               'INVH Code:   '
                            || NVL (g_inv_code, '')
                            || crlf
                            || 'Vendor TNUM: '
                            || NVL (g_vend_tnum, '')
                            || crlf
                            || 'Vendor UNI:  '
                            || NVL (g_vend_uni, '')
                            || crlf
                            || 'E-mail Addr: '
                            || CASE
                                   WHEN g_vend_uni IS NULL
                                   THEN
                                       ''
                                   ELSE
                                       g_vend_uni || '@tc.columbia.edu'
                               END
                       , --VR END PRJ0014600
                        email_success => v_mail_successful
                       ,email_reply => v_mail_errmsg
                       ,email_error_type => v_error_type
                       ,email_error_code => v_error_code
                       ,email_error_message => v_error_message
                    );
                ELSE
                    DBMS_OUTPUT.put_line
                    (
                        'ERROR in p_send_error_email: Can not send email!!!'
                    );
                    tcapp.util_msg.logentry
                    (
                        'ERROR in p_send_error_email: Can not send email!!!'
                    );
                END IF;
            END LOOP;
        END LOOP;
    EXCEPTION
        WHEN OTHERS
        THEN
            DBMS_OUTPUT.put_line ('ERROR in p_send_error_email: ' || SQLERRM);
    END p_send_error_email;

    --
    ----==============================================================================
    PROCEDURE p_send_nsf_invoice_email (p_invoice_code IN VARCHAR2
                                       ,p_msg IN OUT VARCHAR2
                                       ,p_send_to_grants IN VARCHAR2
                                       ,p_send_to_budget IN VARCHAR2)
    IS
        l_from         VARCHAR2 (100);
        l_subj         VARCHAR2 (500);
        l_to           VARCHAR2 (32000);
        l_cc           VARCHAR2 (32000);
        v_mail_successful VARCHAR2 (300) := 'T';
        v_mail_errmsg  VARCHAR2 (300) := 'F';
        v_error_type   VARCHAR2 (100);
        v_error_code   VARCHAR2 (100);
        v_error_message VARCHAR2 (3000);
        crlf           VARCHAR2 (2) := CHR (13) || CHR (10);
    BEGIN
        --
        BEGIN
            SELECT SYS_CONTEXT ('USERENV', 'DB_NAME') INTO g_banner FROM DUAL;
        EXCEPTION
            WHEN OTHERS
            THEN
                NULL;
        END;

        l_subj :=
               g_banner
            || ' ChromeRiver: Invoice '
            || p_invoice_code
            || ' is NSF.';

        --
        -- VR Start PRJ0014600
        BEGIN
            IF LENGTH (NVL (g_vend_id, '')) < 9 -- FZREEXP_VEND_ID is not a T# its a uni
            THEN
                g_vend_tnum :=
                    tcapp.tc_argos_util.tc_get_t_from_uni (g_vend_id);
            ELSIF LENGTH (NVL (g_vend_id, '')) = 9
            THEN
                g_vend_tnum := g_vend_id;
            END IF;
        EXCEPTION
            WHEN OTHERS
            THEN
                NULL;
        END;

        --
        --
        BEGIN
            IF LENGTH (NVL (g_vend_id, '')) < 9 -- FZREEXP_VEND_ID is not a T# its a uni
            THEN
                g_vend_uni := g_vend_id;
            ELSIF LENGTH (NVL (g_vend_id, '')) = 9
            THEN
                g_vend_uni := g_amx_chid;
            END IF;
        EXCEPTION
            WHEN OTHERS
            THEN
                NULL;
        END;

        -- VR END PRJ0014600
        --
        IF p_send_to_budget IS NOT NULL
        THEN
            FOR e
                IN (  SELECT UPPER (ftvsdat_sdat_code_opt_1)     opt
                            ,ftvsdat_title                       email
                        FROM ftvsdat
                       WHERE ftvsdat_sdat_code_entity = 'FZKEEXP'
                         AND UPPER (ftvsdat_sdat_code_attr) = p_send_to_budget
                         AND UPPER (ftvsdat_sdat_code_opt_1) IN
                                 ('FROM', 'TO', 'CC')
                         AND ftvsdat_status_ind = 'A'
                         AND ftvsdat_eff_date <= SYSDATE
                         AND NVL (ftvsdat_term_date, SYSDATE + 1) > SYSDATE
                         AND NVL (ftvsdat_nchg_date, SYSDATE + 1) > SYSDATE
                    ORDER BY ftvsdat_code_level)
            LOOP
                IF e.opt = 'FROM'
                THEN
                    l_from := e.email;
                ELSIF e.opt = 'TO'
                THEN
                    l_to := l_to || TRIM (e.email) || ', ';
                ELSIF e.opt = 'CC'
                THEN
                    l_cc := l_cc || TRIM (e.email) || ', ';
                END IF;
            END LOOP;

            l_to := RTRIM (l_to, ', ');
            l_cc := RTRIM (l_cc, ', ');

            IF l_from IS NOT NULL AND l_to IS NOT NULL
            THEN
                sokemal.p_sendemail
                (
                    email_addr  => l_to
                   ,email_to_name => NULL
                   ,email_from_addr => l_from
                   ,email_from_name => 'Chrome River - NO REPLY'
                   ,email_host  => 'SUNMAIL.TC.COLUMBIA.EDU'
                   ,email_subject => l_subj
                   ,email_message =>
                           p_msg
                        || crlf
                        || crlf
                        || -- VR Start PRJ0014600
                           'INVH Code:   '
                        || NVL (g_inv_code, '')
                        || crlf
                        || 'Vendor TNUM: '
                        || NVL (g_vend_tnum, '')
                        || crlf
                        || 'Vendor UNI:  '
                        || NVL (g_vend_uni, '')
                        || crlf
                        || 'E-mail Addr: '
                        || CASE
                               WHEN g_vend_uni IS NULL
                               THEN
                                   ''
                               ELSE
                                   g_vend_uni || '@tc.columbia.edu'
                           END
                   , -- VR End PRJ0014600
                    email_success => v_mail_successful
                   ,email_reply => v_mail_errmsg
                   ,email_error_type => v_error_type
                   ,email_error_code => v_error_code
                   ,email_error_message => v_error_message
                );
            ELSE
                DBMS_OUTPUT.put_line
                (
                    'ERROR in p_send_nsf_invoice_email: Can not send email!!!'
                );
                tcapp.util_msg.logentry
                (
                    'ERROR in p_send_nsf_invoice_email: Can not send email!!!'
                );
            END IF;
        END IF;

        IF p_send_to_grants IS NOT NULL
        THEN
            FOR f
                IN (  SELECT UPPER (ftvsdat_sdat_code_opt_1)     opt
                            ,ftvsdat_title                       email
                        FROM ftvsdat
                       WHERE ftvsdat_sdat_code_entity = 'FZKEEXP'
                         AND UPPER (ftvsdat_sdat_code_attr) = p_send_to_grants
                         AND UPPER (ftvsdat_sdat_code_opt_1) IN
                                 ('FROM', 'TO', 'CC')
                         AND ftvsdat_status_ind = 'A'
                         AND ftvsdat_eff_date <= SYSDATE
                         AND NVL (ftvsdat_term_date, SYSDATE + 1) > SYSDATE
                         AND NVL (ftvsdat_nchg_date, SYSDATE + 1) > SYSDATE
                    ORDER BY ftvsdat_code_level)
            LOOP
                IF f.opt = 'FROM'
                THEN
                    l_from := f.email;
                ELSIF f.opt = 'TO'
                THEN
                    l_to := l_to || TRIM (f.email) || ', ';
                ELSIF f.opt = 'CC'
                THEN
                    l_cc := l_cc || TRIM (f.email) || ', ';
                END IF;
            END LOOP;

            l_to := RTRIM (l_to, ', ');
            l_cc := RTRIM (l_cc, ', ');

            IF l_from IS NOT NULL AND l_to IS NOT NULL
            THEN
                sokemal.p_sendemail
                (
                    email_addr  => l_to
                   ,email_to_name => NULL
                   ,email_from_addr => l_from
                   ,email_from_name => 'Chrome River - NO REPLY'
                   ,email_host  => 'SUNMAIL.TC.COLUMBIA.EDU'
                   ,email_subject => l_subj
                   ,email_message =>
                           p_msg
                        || crlf
                        || crlf
                        || 'INVH Code:   '
                        || NVL (g_inv_code, '')
                        || crlf
                        || 'Vendor TNUM: '
                        || NVL (g_vend_tnum, '')
                        || crlf
                        || 'Vendor UNI:  '
                        || NVL (g_vend_uni, '')
                        || crlf
                        || 'E-mail Addr: '
                        || CASE
                               WHEN g_vend_uni IS NULL
                               THEN
                                   ''
                               ELSE
                                   g_vend_uni || '@tc.columbia.edu'
                           END
                   ,email_success => v_mail_successful
                   ,email_reply => v_mail_errmsg
                   ,email_error_type => v_error_type
                   ,email_error_code => v_error_code
                   ,email_error_message => v_error_message
                );
            ELSE
                DBMS_OUTPUT.put_line
                (
                    'ERROR in p_send_nsf_invoice_email: Can not send email!!!'
                );
                tcapp.util_msg.logentry
                (
                    'ERROR in p_send_nsf_invoice_email: Can not send email!!!'
                );
                tcapp.util_msg.logentry
                (
                    'Grants email list set: ' || p_send_to_grants
                );
                tcapp.util_msg.logentry
                (
                    'Budget email list set: ' || p_send_to_budget
                );
            END IF;
        END IF;
    --
    EXCEPTION
        WHEN OTHERS
        THEN
            DBMS_OUTPUT.put_line
            (
                'ERROR in p_send_nsf_invoice_email: ' || SQLERRM
            );
            tcapp.util_msg.logentry
            (
                'ERROR in p_send_nsf_invoice_email: ' || SQLERRM
            );
    END p_send_nsf_invoice_email;

    ------------------------------------------------------------------------------------
    PROCEDURE p_check_nsf_invoice (p_invoice_code IN VARCHAR2)
    IS
        l_msg          VARCHAR2 (4000) := NULL;
        crlf           VARCHAR2 (2) := CHR (13) || CHR (10);

        CURSOR nsf_cursor (c_invoice_code VARCHAR2)
        IS
              SELECT farinva_acci_code
                    ,farinva_acct_code
                    ,LPAD (TRIM (TO_CHAR (farinva_appr_amt, '9,999,999.00'))
                          ,14)    AS appr_amt
                FROM farinva
               WHERE farinva_invh_code = p_invoice_code
                 AND farinva_nsf_susp_ind = 'Y'
            ORDER BY 1;

        nsf_info       nsf_cursor%ROWTYPE;
        send_to_grants VARCHAR2 (30) := NULL;
        send_to_budget VARCHAR2 (30) := NULL;
    BEGIN
        OPEN nsf_cursor (p_invoice_code);

        LOOP
            FETCH nsf_cursor INTO nsf_info;

            EXIT WHEN nsf_cursor%NOTFOUND;
            l_msg :=
                   l_msg
                || nsf_info.farinva_acci_code
                || '-'
                || nsf_info.farinva_acct_code
                || nsf_info.appr_amt
                || crlf;

            IF SUBSTR (nsf_info.farinva_acci_code, 1, 1) = '5'
            THEN
                send_to_grants := 'CR_GRANTS_ERROR';
            ELSE
                send_to_budget := 'CR_INVNSF_MAIL';
            END IF;
        END LOOP;

        CLOSE nsf_cursor;

        IF l_msg IS NOT NULL
        THEN
            l_msg :=
                   'Invoice '
                || p_invoice_code
                || ' has been created by Chrome River and is in NSF status.'
                || crlf
                || crlf
                || 'Below is a list of the index/account combinations for this invoice:'
                || crlf
                || crlf
                || l_msg
                || crlf
                || 'End of list.'
                || crlf;

            p_send_nsf_invoice_email (p_invoice_code
                                     ,l_msg
                                     ,send_to_grants
                                     ,send_to_budget);
        END IF;

        RETURN;
    EXCEPTION
        WHEN OTHERS
        THEN
            DBMS_OUTPUT.put_line ('ERROR in p_send_error_email: ' || SQLERRM);
    END p_check_nsf_invoice;

    -------------------------------------------------------------
    PROCEDURE p_processbatch
    IS
        CURSOR dupe_inv_chk (batch_id_in VARCHAR2)
        IS
            SELECT DISTINCT a.fzreexp_inv_num
              FROM fzreexp a
             WHERE fzreexp_batch_id = batch_id_in
               AND EXISTS
                       (SELECT 'Y'
                          FROM fzreexp b
                         WHERE a.fzreexp_batch_id = b.fzreexp_batch_id
                           AND a.fzreexp_external_batch_id <>
                               b.fzreexp_external_batch_id
                           AND a.fzreexp_inv_num = b.fzreexp_inv_num
                           AND b.fzreexp_batch_id = batch_id_in);

        dupe_inv       fzreexp.fzreexp_inv_num%TYPE;
        dupe_external_batch_id fzreexp.fzreexp_external_batch_id%TYPE;
        l_sum          NUMBER;
        l_count        NUMBER;
        l_msg          VARCHAR2 (500);
        new_batch_id   NUMBER;
        batch_record_count NUMBER;
        batch_total_amt NUMBER;
    BEGIN
        l_msg := NULL;

        SELECT fimsmgr.cr_batch_seq.NEXTVAL INTO new_batch_id FROM DUAL;

        UPDATE fzbeexp
           SET fzbeexp_batch_id = new_batch_id
         WHERE fzbeexp_status = 'N' AND fzbeexp_batch_id IS NULL;

        UPDATE fzreexp
           SET fzreexp_batch_id = new_batch_id
         WHERE fzreexp_status = 'N' AND fzreexp_batch_id IS NULL;

        SELECT SUM (fzbeexp_record_count), SUM (fzbeexp_batch_total)
          INTO batch_record_count, batch_total_amt
          FROM fzbeexp
         WHERE fzbeexp_batch_id = new_batch_id;

        SELECT SUM (fzreexp_appr_amt), COUNT (*)
          INTO l_sum, l_count
          FROM fzreexp
         WHERE fzreexp_batch_id = new_batch_id;

        -- Make sure the batch loaded correctly
        IF l_sum <> batch_total_amt
        THEN
            l_msg :=
                   'Batch '
                || new_batch_id
                || ' totals do not match. ACK Total: '
                || batch_total_amt
                || ' Import total: '
                || l_sum;
        ELSIF l_count <> batch_record_count
        THEN
            l_msg :=
                   'Batch '
                || new_batch_id
                || ' counts do not match. ACK Count: '
                || batch_record_count
                || ' Import count: '
                || l_count;
        END IF;

        IF l_msg IS NULL
        THEN
            BEGIN
                -- Check for duplicate load
                SELECT 'Duplicate load error for batch:' || new_batch_id
                  INTO l_msg
                  FROM fzbeexp a
                 WHERE EXISTS
                           (SELECT 'y'
                              FROM fzbeexp b
                             WHERE a.fzbeexp_external_batch_id =
                                   b.fzbeexp_external_batch_id
                               AND a.fzbeexp_batch_id <> b.fzbeexp_batch_id
                               AND a.fzbeexp_batch_id = new_batch_id);

                UPDATE fzreexp
                   SET fzreexp_status = 'X'
                 WHERE fzreexp_batch_id = new_batch_id;
            EXCEPTION
                WHEN NO_DATA_FOUND
                THEN
                    l_msg := NULL;
                WHEN TOO_MANY_ROWS
                THEN
                    l_msg := 'Duplicate load error for ' || new_batch_id;

                    UPDATE fzreexp
                       SET fzreexp_status = 'X'
                     WHERE fzreexp_batch_id = new_batch_id;
                WHEN OTHERS
                THEN
                    l_msg := SUBSTR (SQLERRM, 1, 80);
            END;
        END IF;

        IF l_msg IS NULL
        THEN
            BEGIN
                /* check for duplicate invoices within the batch */
                OPEN dupe_inv_chk (new_batch_id);

                FETCH dupe_inv_chk INTO dupe_inv;

                WHILE dupe_inv_chk%FOUND
                LOOP
                    SELECT MAX (fzreexp_external_batch_id)
                      INTO dupe_external_batch_id
                      FROM fzreexp
                     WHERE fzreexp_batch_id = new_batch_id
                       AND fzreexp_inv_num = dupe_inv;

                    UPDATE fzreexp
                       SET fzreexp_status = 'X'
                          ,fzreexp_err_msg =
                               'Duplicate invoice found in batch'
                     WHERE fzreexp_batch_id = new_batch_id
                       AND fzreexp_inv_num = dupe_inv
                       AND fzreexp_external_batch_id <>
                           dupe_external_batch_id;
                END LOOP;

                CLOSE dupe_inv_chk;
            EXCEPTION
                WHEN OTHERS
                THEN
                    l_msg := SUBSTR (SQLERRM, 1, 80);
            END;
        END IF;

        IF l_msg IS NULL
        THEN
            p_renumber_batch (new_batch_id, l_msg);

            IF l_msg IS NULL
            THEN
                p_processinvoice (new_batch_id);

                UPDATE fzbeexp
                   SET fzbeexp_status = 'C'
                 WHERE fzbeexp_batch_id = new_batch_id;
            ELSE
                p_send_error_email (l_msg);

                UPDATE fzbeexp
                   SET fzbeexp_status = 'E', fzbeexp_err_msg = l_msg
                 WHERE fzbeexp_batch_id = new_batch_id;

                UPDATE fzreexp
                   SET fzreexp_status = 'E', fzreexp_err_msg = l_msg
                 WHERE fzreexp_batch_id = new_batch_id
                   AND fzreexp_status <> 'X';
            END IF;
        ELSE
            p_send_error_email (l_msg);

            UPDATE fzbeexp
               SET fzbeexp_status = 'E', fzbeexp_err_msg = l_msg
             WHERE fzbeexp_batch_id = new_batch_id;

            UPDATE fzreexp
               SET fzreexp_status = 'E', fzreexp_err_msg = l_msg
             WHERE fzreexp_batch_id = new_batch_id AND fzreexp_status <> 'X';
        END IF;
    EXCEPTION
        WHEN OTHERS
        THEN
            DBMS_OUTPUT.put_line (SQLERRM);
    END;

    ----------------------------------------------------------------
    PROCEDURE p_get_inv_header (p_msg IN OUT VARCHAR2, appr_amt IN VARCHAR2)
    IS
        l_msg          VARCHAR2 (100);
        -- VR 9/30/2019 update for INC0089249 Added parameter 'appr_amt' (from p_processInvoice)  for l_sum
        -- VR 9/30/2019 update for INC0089249 This is the subtotal approved amount grouped on fzreexp_inv_num
        l_sum          NUMBER (17, 2) := appr_amt;
    BEGIN
        --
        g_prc_name := 'p_get_inv_header';


        IF NOT f_fspd_open (g_inv_user, g_trans_date)
        THEN
            g_trans_date := SYSDATE;
            g_pmt_due_date := SYSDATE + 1;
        END IF;

        --
        -- Check if Invoice already exists

        BEGIN
            SELECT    'Vendor Invoice '
                   || fabinvh_code
                   || '/'
                   || g_vend_inv_code
                   || ' already exists'
              -- This Vendor Invoice already exists for this vendor
              INTO l_msg
              FROM fabinvh
             WHERE fabinvh_vend_pidm = g_vend_pidm
               AND fabinvh_vend_inv_code = g_vend_inv_code
               AND fabinvh_cancel_date IS NULL;

            p_msg := l_msg;
            RETURN; -- Return!!!
        EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
                l_msg := NULL;
        END;

        -- If summary subtotal is negative then this is a credit memo.
        -- VR 9/30/2019 update for INC0089249 set values for negative transactions for INC0089249
        IF l_sum < 0
        THEN
            g_cr_memo_ind := 'Y'; -- Credit Memo
            g_r_rucl_code := 'INNC'; -- Credit Memo without Encumbrance
        ELSE
            g_cr_memo_ind := 'N'; -- Debit transaction
            g_r_rucl_code := 'INNI';
        END IF;

        -- Select the address

        BEGIN
            IF g_inv_type = 'AMX'
            THEN
                SELECT ftvvend_vend_check_atyp_code
                      ,ftvvend_vend_check_addr_seqno
                  INTO g_atyp_code, g_atyp_seq_no
                  FROM ftvvend
                 WHERE ftvvend_pidm = g_vend_pidm;
            ELSE
                SELECT spraddr_atyp_code, spraddr_seqno
                  INTO g_atyp_code, g_atyp_seq_no
                  FROM spraddr
                 WHERE spraddr_pidm = g_vend_pidm
                   AND ROWID = f_get_address_fnc (g_vend_pidm
                                                 ,'CRADDR'
                                                 ,'A'
                                                 ,SYSDATE
                                                 ,1
                                                 ,'P'
                                                 ,'');
            END IF;
        EXCEPTION
            WHEN OTHERS
            THEN
                p_msg := 'Addr Err: ' || SUBSTR (SQLERRM, 1, 80) || '. ';
                RETURN;
        END;

        --------------------------------------------------------------------------------
        g_header_rec.r_code := g_inv_code;
        g_header_rec.r_pohd_code := NULL;
        g_header_rec.r_vend_pidm := g_vend_pidm;
        g_header_rec.r_open_paid_ind := NULL;
        g_header_rec.r_user_id := g_inv_user;
        g_header_rec.r_vend_inv_code := g_vend_inv_code;
        g_header_rec.r_invoice_date := g_invoice_date;
        g_header_rec.r_pmt_due_date := g_pmt_due_date;
        g_header_rec.r_trans_date := g_trans_date;
        g_header_rec.r_cr_memo_ind := g_cr_memo_ind;
        g_header_rec.r_adjt_code := NULL;
        g_header_rec.r_adjt_date := NULL;
        g_header_rec.r_1099_ind := 'N'; -- VER2113 INC0102106
        g_header_rec.r_1099_id := NULL;
        g_header_rec.r_ityp_seq_code := NULL;
        g_header_rec.r_text_ind := NULL;
        g_header_rec.r_appr_ind := NULL; -- not in use
        g_header_rec.r_complete_ind := NULL;
        g_header_rec.r_disc_code := NULL;
        g_header_rec.r_trat_code := NULL;
        g_header_rec.r_addl_chrg_amt := NULL;
        g_header_rec.r_hold_ind := NULL;
        g_header_rec.r_susp_ind := NULL;
        g_header_rec.r_susp_ind_addl := NULL;
        g_header_rec.r_cancel_ind := NULL;
        g_header_rec.r_cancel_date := NULL;
        g_header_rec.r_post_date := NULL;
        g_header_rec.r_atyp_code := NULL;
        g_header_rec.r_atyp_seq_num := NULL;
        g_header_rec.r_grouping_ind := NULL;
        g_header_rec.r_bank_code := g_bank_code;
        g_header_rec.r_ruiv_ind := NULL;
        g_header_rec.r_edit_defer_ind := NULL;
        g_header_rec.r_override_tax_amt := NULL;
        g_header_rec.r_tgrp_code := NULL;
        g_header_rec.r_submission_number := NULL;
        g_header_rec.r_vend_check_pidm := NULL;
        g_header_rec.r_invoice_type_ind := NULL;
        g_header_rec.r_curr_code := g_currency;
        g_header_rec.r_disb_agent_ind := NULL;
        g_header_rec.r_atyp_code_vend := g_atyp_code;
        g_header_rec.r_atyp_seq_num_vend := g_atyp_seq_no;
        g_header_rec.r_nsf_on_off_ind := NULL;
        g_header_rec.r_single_acctg_ind := 'Y'; -- <DGS 052918> Changed from 'N'
        g_header_rec.r_one_time_vend_name := NULL;
        g_header_rec.r_one_time_house_number := NULL;
        g_header_rec.r_one_time_vend_addr1 := NULL;
        g_header_rec.r_one_time_vend_addr2 := NULL;
        g_header_rec.r_one_time_vend_addr3 := NULL;
        g_header_rec.r_one_time_vend_addr4 := NULL;
        g_header_rec.r_one_time_vend_city := NULL;
        g_header_rec.r_one_time_vend_state := NULL;
        g_header_rec.r_one_time_vend_zip := NULL;
        g_header_rec.r_one_time_vend_natn := NULL;
        g_header_rec.r_delivery_point := NULL;
        g_header_rec.r_correction_digit := NULL;
        g_header_rec.r_carrier_route := NULL;
        g_header_rec.r_ruiv_installment_ind := NULL;
        g_header_rec.r_multiple_inv_ind := NULL;
        g_header_rec.r_phone_area := NULL;
        g_header_rec.r_phone_number := NULL;
        g_header_rec.r_phone_ext := NULL;
        g_header_rec.r_email_addr := NULL;
        g_header_rec.r_ctry_code_fax := NULL;
        g_header_rec.r_fax_area := NULL;
        g_header_rec.r_fax_number := NULL;
        g_header_rec.r_fax_ext := NULL;
        g_header_rec.r_cancel_code := NULL;
        g_header_rec.r_attention_to := NULL;
        g_header_rec.r_vendor_contact := NULL;
        g_header_rec.r_invd_re_establish_ind := NULL;
        g_header_rec.r_ach_override_ind := NULL;
        g_header_rec.r_acht_code := NULL;
        g_header_rec.r_origin_code := g_orig;
        g_header_rec.r_match_required := NULL;
        g_header_rec.r_create_user := NULL;
        g_header_rec.r_create_date := NULL;
        g_header_rec.r_complete_user := NULL;
        g_header_rec.r_complete_date := NULL;
        g_header_rec.r_data_origin := NULL;
        g_header_rec.r_create_source := NULL;
        g_header_rec.r_cancel_user := NULL;
        g_header_rec.r_cancel_activity_date := NULL;
        g_header_rec.r_ctry_code_phone := NULL;
        g_header_rec.r_vend_hold_ovrd_ind := NULL;
        g_header_rec.r_vend_hold_ovrd_user := NULL;
        g_header_rec.r_internal_record_id := NULL;
    --
    EXCEPTION
        WHEN OTHERS
        THEN
            p_msg :=
                   SUBSTR (SQLERRM, 1, 100)
                || ' in p_get_inv_header '
                || g_vend_inv_code;
    END p_get_inv_header;

    --
    --==============================================================================
    --
    PROCEDURE p_create_items (p_msg IN OUT VARCHAR2)
    AS
        l_item_rec     fb_invoice_item.invoice_item_rec;
        l_msg          VARCHAR2 (4000);
        l_item         farinvc.farinvc_item%TYPE;
        --
        l_tot_item_appr_amt farinvc.farinvc_appr_qty%TYPE;
        l_tot_item_addl_amt NUMBER;
        l_tot_item_disc_amt NUMBER;
        l_tot_item_tax_amt NUMBER;
        l_tot_actg_amt NUMBER;
        l_tot_actg_appr_amt NUMBER;
        l_tot_actg_addl_amt NUMBER;
        l_tot_actg_disc_amt NUMBER;
        l_tot_actg_tax_amt NUMBER;

        l_fund_code    ftvacci.ftvacci_fund_code%TYPE;
        l_orgn_code    ftvacci.ftvacci_orgn_code%TYPE;
        l_prog_code    ftvacci.ftvacci_prog_code%TYPE;

        --

        -- VR 9/30/19 INC0089249 added case stment to inv_comm_cursor
        -- VR 9/30/19 INC0089249 when its a credit memo (negative amt when gouped by fzreexp_inv_num) we dont want the amt to be a negative here, unless it was a positive on statement
        --CURSOR inv_comm_cursor IS
        --SELECT fzreexp_item_no,
        --       fzreexp_comm_desc,
        --       fzreexp_appr_amt
        --  FROM fzreexp
        -- WHERE fzreexp_inv_num = TRIM (g_vend_inv_code) AND fzreexp_status = 'N';
        CURSOR inv_comm_cursor IS
            SELECT fzreexp_item_no
                  ,fzreexp_comm_desc
                  ,CASE
                       WHEN (SELECT SUM (b.fzreexp_appr_amt)
                               FROM fzreexp b
                              WHERE b.fzreexp_inv_num =
                                    TRIM (g_vend_inv_code)
                                AND fzreexp_status = 'N') <
                            0
                       THEN
                           CASE
                               WHEN fzreexp_appr_amt < 0
                               THEN
                                   ABS (fzreexp_appr_amt)
                               WHEN fzreexp_appr_amt > 0
                               THEN
                                   (fzreexp_appr_amt * -1)
                               ELSE
                                   fzreexp_appr_amt
                           END
                       ELSE
                           fzreexp_appr_amt
                   END    AS fzreexp_appr_amt
              FROM fzreexp
             WHERE fzreexp_inv_num = TRIM (g_vend_inv_code)
               AND fzreexp_status = 'N';

        /* <DGS 05292018>  Replaced cursor below
        cursor inv_acctg_cursor is
        select fzreexp_item_no, fzreexp_index, fzreexp_acct_code, fzreexp_appr_amt appr_amt, fzreexp_foapl foapl
        from fzreexp
        where fzreexp_inv_num = trim(g_vend_inv_code)
        and fzreexp_status = 'N';
        */
        --CURSOR inv_acctg_cursor IS
        --  SELECT MAX (fzreexp_index)    fzreexp_index,
        --         fzreexp_acct_code,
        --    SUM (fzreexp_appr_amt) appr_amt,
        --         fzreexp_foapl
        --    FROM fzreexp
        --   WHERE fzreexp_inv_num = TRIM (g_vend_inv_code) AND fzreexp_status = 'N'
        --GROUP BY fzreexp_foapl, fzreexp_acct_code;

        -- VR 9/30/19 INC0089249 added case stment to inv_acctg_cursor
        -- VR 9/30/19 INC0089249 when its a credit memo (negative amt when gouped by fzreexp_inv_num) we need to flip the values of the acct lines.
        CURSOR inv_acctg_cursor IS
              SELECT MAX (fzreexp_index)    fzreexp_index
                    ,fzreexp_acct_code
                    ,CASE
                         WHEN invoicesum < 0
                         THEN
                             CASE
                                 WHEN SUM (fzreexp_appr_amt) < 0
                                 THEN
                                     ABS (SUM (fzreexp_appr_amt))
                                 WHEN SUM (fzreexp_appr_amt) > 0
                                 THEN
                                     (SUM (fzreexp_appr_amt) * -1)
                                 ELSE
                                     SUM (fzreexp_appr_amt)
                             END
                         ELSE
                             SUM (fzreexp_appr_amt)
                     END                    AS appr_amt
                    ,fzreexp_foapl
                FROM fzreexp
                    , (SELECT SUM (b.fzreexp_appr_amt)     invoicesum
                         FROM fzreexp b
                        WHERE b.fzreexp_inv_num = TRIM (g_vend_inv_code)
                          AND fzreexp_status = 'N') invoice
               WHERE fzreexp_inv_num = TRIM (g_vend_inv_code)
                 AND fzreexp_status = 'N'
            GROUP BY fzreexp_foapl, fzreexp_acct_code, invoicesum;

        /* <DGS 05292018>  New Cursor for acctg error */
        CURSOR acctg_error_cursor (p_foapl VARCHAR2, p_acct_code VARCHAR2)
        IS
              SELECT fzreexp_trans_date
                    ,fzreexp_comm_desc
                    ,SUM (fzreexp_appr_amt)                        appr_amt
                    ,fzreexp_foapl || '-' || fzreexp_acct_code     foapl_code
                FROM fzreexp
               WHERE fzreexp_inv_num = TRIM (g_vend_inv_code)
                 AND fzreexp_foapl = p_foapl
                 AND fzreexp_acct_code = p_acct_code
                 AND fzreexp_status = 'N'
            GROUP BY fzreexp_trans_date
                    ,fzreexp_comm_desc
                    ,fzreexp_foapl || '-' || fzreexp_acct_code
            ORDER BY fzreexp_trans_date;


        g_eoy_accr_status_ind ftvfsyr.ftvfsyr_eoy_accr_status_ind%TYPE;
        g_fsyr_code    ftvfspd.ftvfspd_fsyr_code%TYPE;
        g_fspd_code    ftvfspd.ftvfspd_fspd_code%TYPE;

        acctg_seq      NUMBER := 0;
    BEGIN -- p_create_items
        --
        g_prc_name := 'p_create_items';

        --
        SELECT ftvfsyr_eoy_accr_status_ind
              ,ftvfspd.ftvfspd_fsyr_code
              ,ftvfspd.ftvfspd_fspd_code
          INTO g_eoy_accr_status_ind, g_fsyr_code, g_fspd_code
          FROM ftvfsyr, ftvfspd
         WHERE ftvfsyr_coas_code = ftvfspd_coas_code
           AND ftvfsyr_coas_code = '1'
           AND ftvfsyr_fsyr_code = ftvfspd_fsyr_code
           AND TRUNC (g_trans_date) BETWEEN ftvfspd_prd_start_date
                                        AND ftvfspd_prd_end_date;

        FOR i IN inv_comm_cursor
        LOOP
            -- create g_comm_rec
            g_comm_rec.r_invh_code := g_inv_code;
            g_comm_rec.r_pohd_code := NULL;
            g_comm_rec.r_item := i.fzreexp_item_no;
            g_comm_rec.r_po_item := NULL;
            g_comm_rec.r_open_paid_ind := 'O';
            g_comm_rec.r_user_id := g_inv_user;
            g_comm_rec.r_comm_code := NULL;
            g_comm_rec.r_comm_desc := i.fzreexp_comm_desc;
            g_comm_rec.r_uoms_code := NULL;
            g_comm_rec.r_adjt_code := NULL;
            g_comm_rec.r_adjt_date := NULL;
            g_comm_rec.r_part_pmt_ind := NULL;
            g_comm_rec.r_prev_paid_qty := 1;
            g_comm_rec.r_recvd_qty := NULL;
            g_comm_rec.r_invd_unit_price := NULL;
            g_comm_rec.r_invd_qty := NULL;
            g_comm_rec.r_accept_qty := NULL;
            g_comm_rec.r_accept_unit_price := NULL;
            g_comm_rec.r_disc_amt := 0;
            g_comm_rec.r_tax_amt := 0;
            g_comm_rec.r_appr_qty := 1;
            g_comm_rec.r_appr_unit_price := i.fzreexp_appr_amt;
            g_comm_rec.r_tol_override_ind := NULL;
            g_comm_rec.r_hold_ind := 'N';
            g_comm_rec.r_susp_ind := 'N';
            g_comm_rec.r_ttag_num := NULL;
            g_comm_rec.r_addl_chrg_amt := NULL;
            g_comm_rec.r_convert_unit_price := NULL;
            g_comm_rec.r_convert_disc_amt := NULL;
            g_comm_rec.r_convert_tax_amt := NULL;
            g_comm_rec.r_convert_addl_chrg_amt := NULL;
            g_comm_rec.r_tgrp_code := NULL;
            g_comm_rec.r_tag_cap_code := 'N';
            g_comm_rec.r_vend_inv_code := g_vend_inv_code;
            g_comm_rec.r_vend_inv_date := g_invoice_date;
            g_comm_rec.r_vend_inv_item := i.fzreexp_item_no;
            g_comm_rec.r_prev_amt := NULL;
            g_comm_rec.r_desc_chge_ind := NULL;
            g_comm_rec.r_last_rcvr_ind := NULL;
            g_comm_rec.r_override_tax_amt := NULL;
            g_comm_rec.r_data_origin := g_orig;
            g_comm_rec.r_create_user := g_inv_user;
            g_comm_rec.r_create_date := TRUNC (SYSDATE);

            --  Select error msg info
            BEGIN
                SELECT    fzreexp_trans_date
                       || CHR (9)
                       || fzreexp_comm_desc
                       || CHR (9)
                       || CHR (9)
                       || fzreexp_appr_amt
                       || CHR (9)
                       || fzreexp_index
                       || '-'
                       || fzreexp_acct_code
                  INTO l_msg
                  FROM fzreexp
                 WHERE fzreexp_inv_num = g_vend_inv_code
                   AND fzreexp_item_no = i.fzreexp_item_no
                   AND fzreexp_status = 'N';
            EXCEPTION
                WHEN OTHERS
                THEN
                    l_msg := 'Error selecting msg info ' || SQLERRM;
            END;

            --  Create the commodity lines
            fb_invoice_item.p_create_item (g_comm_rec);
        --
        END LOOP;

        FOR j IN inv_acctg_cursor
        LOOP
            acctg_seq := acctg_seq + 1;
            l_msg := NULL;
            l_fund_code :=
                SUBSTR (j.fzreexp_foapl
                       ,1
                       ,INSTR (j.fzreexp_foapl, '-', 1, 1) - 1);
            l_orgn_code :=
                SUBSTR
                (
                    j.fzreexp_foapl
                   ,INSTR (j.fzreexp_foapl, '-', 1, 1) + 1
                   ,(  INSTR (j.fzreexp_foapl, '-', 1, 2)
                     - (INSTR (j.fzreexp_foapl, '-', 1, 1) + 1))
                );
            l_prog_code :=
                SUBSTR (j.fzreexp_foapl
                       ,INSTR (j.fzreexp_foapl, '-', 1, 2) + 1
                       ,LENGTH (j.fzreexp_foapl));

            --dbms_output.put_line('Invoice '||g_inv_code);
            --dbms_output.put_line('Vend Inv '||g_vend_inv_code);
            --dbms_output.put_line('Item no '||j.fzreexp_item_no);

            --   Create the accounting record
            g_acctg_rec.r_invh_code := g_inv_code;
            g_acctg_rec.r_pohd_code := NULL;
            g_acctg_rec.r_item := 0; --<DGS 05292018> changed from j.fzreexp_item_no;
            g_acctg_rec.r_po_item := NULL;
            g_acctg_rec.r_seq_num := acctg_seq;
            g_acctg_rec.r_user_id := g_inv_user;
            g_acctg_rec.r_fsyr_code := g_fsyr_code;
            g_acctg_rec.r_period := g_fspd_code;
            g_acctg_rec.r_eoy_accr_status_ind := g_eoy_accr_status_ind;
            --VR 9/16/2019 update rucl based on g_r_rucl_code set in get_inv header if its a credit memo for INC0089249
            g_acctg_rec.r_rucl_code := g_r_rucl_code;
            g_acctg_rec.r_disc_rucl_code := NULL;
            g_acctg_rec.r_tax_rucl_code := NULL;
            g_acctg_rec.r_addl_rucl_code := NULL;
            g_acctg_rec.r_coas_code := '1';
            g_acctg_rec.r_acci_code := j.fzreexp_index;
            g_acctg_rec.r_fund_code := l_fund_code;
            g_acctg_rec.r_orgn_code := l_orgn_code;
            g_acctg_rec.r_acct_code := j.fzreexp_acct_code;
            g_acctg_rec.r_prog_code := l_prog_code;
            g_acctg_rec.r_actv_code := NULL;
            g_acctg_rec.r_locn_code := NULL;
            g_acctg_rec.r_bank_code := g_bank_code;
            g_acctg_rec.r_invd_amt := NULL;
            g_acctg_rec.r_disc_amt := 0;
            g_acctg_rec.r_tax_amt := 0;
            g_acctg_rec.r_addl_chrg_amt := 0;
            g_acctg_rec.r_appr_amt := j.appr_amt;
            g_acctg_rec.r_prev_paid_amt := j.appr_amt;
            g_acctg_rec.r_nsf_override_ind := 'N';
            g_acctg_rec.r_ityp_seq_code := NULL;
            g_acctg_rec.r_proj_code := NULL;
            g_acctg_rec.r_susp_ind := 'N';
            g_acctg_rec.r_nsf_susp_ind := 'N';
            g_acctg_rec.r_partial_liq_ind := NULL;
            g_acctg_rec.r_appr_ind := 'N';
            g_acctg_rec.r_open_paid_ind := 'O';
            g_acctg_rec.r_convert_amt := NULL;
            g_acctg_rec.r_convert_disc_amt := NULL;
            g_acctg_rec.r_convert_tax_amt := NULL;
            g_acctg_rec.r_convert_addl_chrg_amt := NULL;
            g_acctg_rec.r_appr_amt_pct := NULL;
            g_acctg_rec.r_disc_amt_pct := NULL;
            g_acctg_rec.r_addl_amt_pct := NULL;
            g_acctg_rec.r_tax_amt_pct := NULL;
            g_acctg_rec.r_cap_amt := NULL;
            g_acctg_rec.r_data_origin := g_orig;

            -- Create error message info
            -- Change to a cursor and string together
            BEGIN
                --       select fzreexp_trans_date||chr(9)||fzreexp_comm_desc||chr(9)||chr(9)||fzreexp_appr_amt||chr(9)||fzreexp_index||'-'||FZREEXP_ACCT_CODE
                /*        select fzreexp_appr_amt||chr(9)||fzreexp_index||'-'||FZREEXP_ACCT_CODE
                        into l_msg
                        from fzreexp
                      where fzreexp_inv_num = g_vend_inv_code
                        and fzreexp_foapl = j.fzreexp_foapl         -- <DGS 05292018>  constraint changed from item no to foapl and acct_code
                        and fzreexp_acct_code = j.fzreexp_acct_code
                        and fzreexp_status = 'N';
                */
                FOR k
                    IN acctg_error_cursor (j.fzreexp_foapl
                                          ,j.fzreexp_acct_code)
                LOOP
                    l_msg :=
                           l_msg
                        || k.fzreexp_trans_date
                        || CHR (9)
                        || k.fzreexp_comm_desc
                        || CHR (9)
                        || CHR (9)
                        || k.appr_amt
                        || CHR (9)
                        || k.foapl_code
                        || CHR (10);
                END LOOP;
            EXCEPTION
                WHEN OTHERS
                THEN
                    l_msg := 'Error selecting msg info ' || SQLERRM;
            END;

            --   Create the accounting lines
            fb_invoice_acctg.p_create_accounting (g_acctg_rec);
            --  if no error is thrown, wipe out l_msg;
            l_msg := NULL;
        END LOOP;
    --
    EXCEPTION
        WHEN OTHERS
        THEN
            p_msg :=
                   SUBSTR (SQLERRM, 1, 100)
                || ' '
                || g_vend_inv_code
                || CHR (10);
            p_msg :=
                   p_msg
                || 'Expense Date'
                || CHR (9)
                || 'Commodity'
                || CHR (9)
                || CHR (9)
                || 'Amount'
                || CHR (9)
                || 'Fund-Org-Prog-Acct'
                || CHR (10);
            --  p_msg := 'Amount'||chr(9)||'Index-Acct'||chr(10);
            p_msg := p_msg || l_msg;
            l_msg := NULL;
    END p_create_items;

    --
    --==============================================================================
    --
    PROCEDURE p_processinvoice (p_batch_id IN VARCHAR2)
    IS
        l_msg          VARCHAR2 (4000);
        l_str          VARCHAR2 (4000);
        p_msg          VARCHAR2 (4000);
        l_sum          NUMBER (12, 2);
        found_vend_pidm VARCHAR2 (1) := 'N';
        debug_stmt     VARCHAR2 (1000);

        --<DGS 05112018>  Start
        fye_test       VARCHAR2 (1) := 'N';
        g_enc_roll_ind VARCHAR2 (1) := 'N';

        --<DGS 05112018> End

        CURSOR inv_cursor IS
              SELECT fzreexp_inv_num                   vend_inv_code
                    ,MAX (UPPER (fzreexp_vend_id))     vend_id
                    ,MAX (fzreexp_exp_export_date)     exp_inv_date
                    ,MAX (fzreexp_amx_stmt_date)       amx_inv_date
                    ,MAX (fzreexp_amx_export_date)     amx_trans_date
                    ,MAX (fzreexp_amx_chid)            amx_chid
                    ,MAX (fzreexp_inv_type)            inv_type
                    ,MAX (fzreexp_inv_due_date)        inv_due_date
                    ,MAX (fzreexp_bank_code)           bank_code
                    ,SUM (fzreexp_appr_amt)            appr_amt
                FROM fzreexp
               WHERE fzreexp_status = 'N' AND fzreexp_batch_id = p_batch_id
            GROUP BY fzreexp_inv_num;

        --------------------------------------------------------------------------------
        PROCEDURE p_update_log
        IS
        BEGIN
            UPDATE fzreexp
               SET fzreexp_invh_code = g_inv_code
                  ,fzreexp_status = g_doc_status
                  ,fzreexp_err_msg = g_err_msg
                  ,fzreexp_load_date = SYSDATE
             WHERE fzreexp_inv_num = g_vend_inv_code
               AND fzreexp_batch_id = p_batch_id;

            COMMIT;
        END;

        --------------------------------------------------------------------------------
        PROCEDURE p_create_header (p_msg IN OUT VARCHAR2)
        IS
        BEGIN
            --
            g_prc_name := 'p_create_header';
            --
            --dbms_output.put_line('  Invoice Header');
            --dbms_output.put_line('    invoiceID           '||g_vend_inv_code);
            --dbms_output.put_line('    FABINVH_CODE        '||g_inv_code);
            --dbms_output.put_line('    FABINVH_USER_ID     '||g_header_rec.r_user_id);
            --dbms_output.put_line('    FABINVH_CREATE_USER '||g_header_rec.r_create_user);
            --dbms_output.put_line('    fabinvh_vend_pidm  '|| g_vend_pidm);
            --dbms_output.put_line('invoice trans date '||g_trans_date);
            --dbms_output.put_line('payment due date '||g_pmt_due_date);

            -- dbms_output.put_line('Vendor InvCode in '||g_header_rec.r_vend_inv_code);
            fb_invoice_header.p_create_header (g_header_rec);
            --
            -- Assign returned values to global variables
            g_inv_code := g_header_rec.r_code;
            g_vend_inv_code := g_header_rec.r_vend_inv_code;
        -- dbms_output.put_line('Vendor inv code: '||g_vend_inv_code);

        EXCEPTION
            WHEN OTHERS
            THEN
                p_msg :=
                       SUBSTR (SQLERRM, 1, 80)
                    || 'For '
                    || g_vend_inv_code
                    || 'TransDt: '
                    || g_trans_date
                    || ' T-ID: '
                    || tcapp.tc_argos_util.tc_get_t_from_uni (g_vend_id);
        --p_msg := substr(SQLERRM,1,100)||' '||g_vend_inv_code||' T-ID: '||g_vend_id;
        END;

        --------------------------------------------------------------------------------
        PROCEDURE p_complete (p_msg IN OUT VARCHAR2)
        IS
            l_succ_msg     gb_common_strings.err_type;
            l_err_msg      gb_common_strings.err_type;
            l_warn_msg     gb_common_strings.err_type;

            -----------------------------------------
            PROCEDURE p_set_post (p_set VARCHAR2)
            IS
            BEGIN
                gb_common.p_set_context ('FP_INVOICE.P_COMPLETE'
                                        ,'BATCH_TRAN_POST'
                                        ,p_set);
            END;

            -----------------------------------------
            FUNCTION f_cln_msg (p_msg VARCHAR2)
                RETURN VARCHAR2
            IS
            BEGIN
                RETURN REPLACE (RTRIM (LTRIM (p_msg, ':'), ':')
                               ,'::::'
                               ,CHR (10));
            END;

            -----------------------------------------
            PROCEDURE p_complete_invoice
            IS
            BEGIN
                DBMS_OUTPUT.put_line ('Call fp_invoice.p_complete');
                fp_invoice.p_complete (p_code      => g_inv_code
                                      ,p_user_id   => g_inv_user
                                      ,p_completion_ind => 'Y'
                                      ,p_success_msg_out => l_succ_msg
                                      ,p_error_msg_out => l_err_msg
                                      ,p_warn_msg_out => l_warn_msg);
            --dbms_output.put_line('l_succ_msg: '||l_succ_msg);
            --dbms_output.put_line('l_err_msg: '||l_err_msg);
            --dbms_output.put_line('l_warn_msg: '||l_warn_msg);

            END;
        -----------------------------------------
        BEGIN -- p_complete
            --
            p_set_post ('TRUE');
            --
            p_complete_invoice;

            --
            IF l_err_msg IS NOT NULL
            THEN
                --    Got error trying to post. Try now just to complete.
                p_set_post ('FALSE');
                p_complete_invoice;
            END IF;

            --
            g_nsf_appr_byp := 'N';

            IF UPPER (l_err_msg) LIKE
                   '%CANNOT COMPLETE A DOCUMENT HAVING TRANSACTIONS IN NSF STATUS WHEN APPROVALS ARE BYPASSED%'
            THEN
                l_succ_msg :=
                    'Cannot complete a document having transactions in NSF status when approvals are bypassed.';
                l_err_msg := NULL;
                g_nsf_appr_byp := 'Y';
            END IF;

            --
            p_msg := f_cln_msg (l_err_msg);
        --
        EXCEPTION
            WHEN OTHERS
            THEN
                p_msg :=
                       SUBSTR (SQLERRM, 1, 80)
                    || ' in p_complete '
                    || g_vend_inv_code;
        END;

        ---------------------------------------------------------------------------------------
        PROCEDURE p_done
        IS
        BEGIN
            --
            IF l_msg IS NULL
            THEN
                p_check_nsf_invoice (g_inv_code);

                g_doc_status := 'C';
                g_err_msg := NULL;
            ELSE
                IF l_msg LIKE 'Vendor Invoice%already exists%'
                THEN
                    g_doc_status := 'D';
                ELSE
                    g_doc_status := 'E';
                END IF;

                g_err_msg := SUBSTR (l_msg, 1, 400); --<DGS 08202018>
                l_msg := NULL;
            END IF;
        --
        END;

        ---------------------------------------------------------------------------------------
        PROCEDURE p_insrt_vend_record
        IS
        /* Will only create for employees.  Students will raise an error */
        BEGIN
            INSERT INTO ftvvend (ftvvend_pidm
                                ,ftvvend_eff_date
                                ,ftvvend_term_date
                                ,ftvvend_activity_date
                                ,ftvvend_user_id
                                ,ftvvend_ityp_seq_code
                                ,ftvvend_disc_code
                                ,ftvvend_1099_rpt_id
                                ,ftvvend_trat_code
                                ,ftvvend_in_st_ind
                                ,ftvvend_grouping_ind
                                ,ftvvend_carrier_ind
                                ,ftvvend_contact
                                ,ftvvend_phone_area
                                ,ftvvend_phone_number
                                ,ftvvend_phone_ext
                                ,ftvvend_atyp_code
                                ,ftvvend_addr_seqno
                                ,ftvvend_fed_whold_pct
                                ,ftvvend_st_whold_pct
                                ,ftvvend_combined_fed_st_filer
                                ,ftvvend_collect_tax
                                ,ftvvend_vend_check_pidm
                                ,ftvvend_vend_check_atyp_code
                                ,ftvvend_vend_check_addr_seqno
                                ,ftvvend_curr_code
                                ,ftvvend_entity_ind
                                ,ftvvend_pidm_owner
                                ,ftvvend_eproc_ind
                                ,ftvvend_data_origin
                                ,ftvvend_ctry_code_phone
                                ,ftvvend_po_hold_rsn_code
                                ,ftvvend_pmt_hold_rsn_code
                                ,ftvvend_1099_atyp_code
                                ,ftvvend_1099_addr_seqno
                                ,ftvvend_tax_form_status
                                ,ftvvend_tax_form_date
                                ,ftvvend_surrogate_id
                                ,ftvvend_version
                                ,ftvvend_vpdi_code)
                SELECT a.spriden_pidm
                      , --ftvvend_pidm
                       pebempl_first_hire_date
                      , --ftvvend_eff_date
                       NULL
                      , --ftvvend_term_date
                       SYSDATE
                      , --ftvvend_activity_date
                       USER
                      , --ftvvend_user_id
                       NULL
                      , --ftvvend_ityp_seq_code
                       NULL
                      , --ftvvend_disc_code
                       NULL
                      , --ftvvend_1099_rpt_id
                       NULL
                      , --ftvvend_trat_code
                       DECODE (spraddr_stat_code, 'NY', 'I', 'O')
                      , --ftvvend_in_st_ind    can be I, O or null
                       'M'
                      , --ftvvend_grouping_ind
                       NULL
                      , --ftvvend_carrier_ind
                       NULL
                      , -- ftvvend_contact
                       NULL
                      , --ftvvend_phone_area
                       NULL
                      , --ftvvend_phone_number
                       NULL
                      , --ftvvend_phone_ext
                       spraddr_atyp_code
                      , --ftvvend_atyp_code
                       spraddr_seqno
                      , --ftvvend_addr_seqno
                       NULL
                      , --ftvvend_fed_whold_pct
                       NULL
                      , --ftvvend_st_whold_pct
                       NULL
                      , --ftvvend_combined_fed_st_filer
                       'N'
                      , -- ftvvend_collect_tax
                       NULL
                      , -- ftvvend_vend_check_pidm
                       NULL
                      , --ftvvend_vend_check_atyp_code,
                       NULL
                      , --ftvvend_vend_check_addr_seqno,
                       NULL
                      , --ftvvend_curr_code,
                       'P'
                      , --ftvvend_entity_ind,
                       NULL
                      , --ftvvend_pidm_owner
                       'N'
                      , --ftvvend_eproc_ind
                       'Banner'
                      , --ftvvend_data_origin
                       NULL
                      , --ftvvend_ctry_code_phone
                       NULL
                      , --ftvvend_po_hold_rsn_code
                       NULL
                      , --ftvvend_pmt_hole_rsn_code
                       NULL
                      , --ftvvend_1099_atyp_code
                       NULL
                      , --ftvvend_1099_addr_seqno
                       NULL
                      , --ftvvend_tax_form_status
                       NULL
                      , --ftvvend_tax_form_date
                       NULL
                      , --ftvvend_surrogate_id
                       NULL
                      , --ftvvend_version
                       NULL --ftvvend_vpdi_code
                  FROM spriden  a
                      ,nbrbjob  n
                      ,nbbposn
                      ,nbrjobs
                      ,pebempl
                      ,spraddr
                 WHERE a.spriden_change_ind IS NULL
                   AND a.spriden_entity_ind = 'P'
                   AND a.spriden_pidm = pebempl_pidm
                   AND a.spriden_pidm = n.nbrbjob_pidm
                   AND n.nbrbjob_contract_type = 'P'
                   AND TRUNC (SYSDATE) BETWEEN TRUNC (n.nbrbjob_begin_date)
                                           AND TRUNC
                                               (
                                                   NVL (n.nbrbjob_end_date
                                                       ,SYSDATE)
                                               )
                   AND n.nbrbjob_posn = nbbposn_posn
                   AND a.spriden_pidm = nbrjobs_pidm
                   AND nbrjobs_status <> 'T'
                   AND nbrjobs_pidm = nbrbjob_pidm
                   AND nbrjobs_posn = nbrbjob_posn
                   AND nbrjobs_suff = nbrbjob_suff
                   AND nbrjobs_effective_date =
                       (SELECT MAX (n.nbrjobs_effective_date)
                          FROM nbrjobs n
                         WHERE n.nbrjobs_suff = nbrbjob_suff
                           AND n.nbrjobs_posn = nbrbjob_posn
                           AND n.nbrjobs_pidm = nbrbjob_pidm
                           AND TRUNC (n.nbrjobs_effective_date) <=
                               TRUNC (SYSDATE))
                   AND EXISTS
                           (SELECT 'y'
                              FROM spriden b
                             WHERE b.spriden_pidm = a.spriden_pidm
                               AND b.spriden_ntyp_code = 'UNI'
                               AND b.spriden_entity_ind = 'P')
                   AND NOT EXISTS
                           (SELECT 'y'
                              FROM ftvvend
                             WHERE ftvvend_pidm = a.spriden_pidm)
                   AND a.spriden_pidm = spraddr_pidm
                   AND spraddr.ROWID = f_get_address_fnc (a.spriden_pidm
                                                         ,'CRADDR'
                                                         ,'A'
                                                         ,SYSDATE
                                                         ,1
                                                         ,'P'
                                                         ,'')
                   AND a.spriden_pidm = g_vend_pidm;
        EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
                l_msg := 'Unable to create vendor: ' || SQLERRM;
            WHEN OTHERS
            THEN
                l_msg := 'Error creating vendor: ' || SQLERRM;
        END;
    ---------------------------------------------------------------------------------------
    BEGIN -- p_ProcessInvoice  The real start for processing.
        --
        g_prc_name := 'p_ProcessInvoice';
        g_user_coas := '1';

        --
        --<DGS 05112018> Start
        /* selects for fiscal year end adjustment */
        debug_stmt := 'stmt 1';

        BEGIN
            SELECT 'Y'
              INTO fye_test
              FROM ftvfspd
             WHERE TRUNC (SYSDATE) BETWEEN TRUNC (ftvfspd_prd_start_date)
                                       AND TRUNC (ftvfspd_prd_end_date)
               AND ftvfspd_fspd_code = '01';
        EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
                fye_test := 'N';
            WHEN OTHERS
            THEN
                l_msg := 'Unable to determine FSPD';
        END;

        IF fye_test = 'Y'
        THEN
            debug_stmt := 'stmt 2';

            BEGIN
                SELECT DECODE (fgbyrlm_encb_per_date, NULL, 'N', 'Y')
                  INTO g_enc_roll_ind
                  FROM ftvfsyr, fgbyrlm
                 WHERE ftvfsyr_fsyr_code =
                       (SELECT ftvfsyr_fsyr_code - 1
                          FROM ftvfsyr
                         WHERE TRUNC (SYSDATE) BETWEEN TRUNC
                                                       (
                                                           ftvfsyr_start_date
                                                       )
                                                   AND TRUNC
                                                       (
                                                           ftvfsyr_end_date
                                                       )
                           AND ftvfsyr_coas_code = '1')
                   AND ftvfsyr_coas_code = '1'
                   AND fgbyrlm_fsyr_code(+) = ftvfsyr_fsyr_code
                   AND fgbyrlm_coas_code(+) = '1';
            EXCEPTION
                WHEN NO_DATA_FOUND
                THEN
                    g_enc_roll_ind := 'N';
                WHEN OTHERS
                THEN
                    l_msg := 'Unable to determine enc roll status';
            END;
        END IF;

        --dbms_output.put_line('fye_test completed');

        -- <DGS 05112018> end

        FOR i IN inv_cursor
        LOOP
            debug_stmt := 'stmt 3';
            g_vend_id := i.vend_id;
            g_vend_inv_code := i.vend_inv_code;
            g_invoice_date :=
                CASE i.inv_type
                    WHEN 'AMX'
                    THEN
                        i.amx_inv_date
                    ELSE
                        i.exp_inv_date
                END;
            g_trans_date :=
                CASE i.inv_type
                    WHEN 'AMX'
                    THEN
                        i.amx_trans_date
                    ELSE
                        i.exp_inv_date
                END;

            g_pmt_due_date := i.inv_due_date;

            -- <DGS 05112018> start
            /* adjust the transaction date during fiscal year end */
            IF fye_test = 'Y' AND g_enc_roll_ind = 'N'
            THEN
                debug_stmt := 'stmt 4';

                SELECT TRUNC
                       (
                           TO_DATE ('31-AUG-' || TO_CHAR (SYSDATE, 'yyyy')
                                   ,'dd-mon-yyyy')
                       )
                  -- select trunc(to_date('10-JUL-'||to_char(sysdate,'yyyy'),'dd-mon-yyyy'))
                  INTO g_trans_date
                  FROM DUAL;

                SELECT GREATEST (i.inv_due_date, TRUNC (SYSDATE + 1))
                  INTO g_pmt_due_date
                  FROM DUAL;
            END IF;


            -- <DGS 05112018> end

            g_bank_code := i.bank_code;
            l_sum := i.appr_amt;
            g_inv_type := i.inv_type;
            l_msg := NULL;
            g_doc_status := NULL;
            g_inv_code := NULL;
            g_amx_chid := i.amx_chid; -- VR
            fokutil.p_gen_doc_code (3, g_inv_code);

            -- Get payee pidm
            IF i.inv_type = 'AMX'
            THEN
                debug_stmt := 'stmt 5';

                BEGIN
                    SELECT spraddr_pidm
                      INTO g_vend_pidm
                      FROM spraddr
                     WHERE spraddr_atyp_code = 'AM'
                       AND spraddr_to_date IS NULL
                       AND TRIM (spraddr_street_line1) =
                           (SELECT a.spriden_id
                              FROM spriden a
                             WHERE a.spriden_change_ind IS NULL
                               AND a.spriden_pidm =
                                   (SELECT b.spriden_pidm
                                      FROM spriden b
                                     WHERE b.spriden_ntyp_code = 'UNI'
                                       AND b.spriden_id = i.amx_chid));
                EXCEPTION
                    WHEN NO_DATA_FOUND
                    THEN
                        l_msg :=
                            'Amex Vendor for' || i.amx_chid || ' not found.';
                    WHEN OTHERS
                    THEN
                        l_msg := SUBSTR (SQLERRM, 1, 80);
                END;
            ELSE
                BEGIN
                    debug_stmt := 'stmt 6';

                    SELECT DISTINCT spriden_pidm
                      INTO g_vend_pidm
                      FROM spriden
                     WHERE spriden_id = g_vend_id;
                EXCEPTION
                    WHEN NO_DATA_FOUND
                    THEN
                        l_msg := 'Vendor ' || g_vend_id || ' not found.';
                    WHEN OTHERS
                    THEN
                        l_msg := SUBSTR (SQLERRM, 1, 80);
                END;
            END IF;

            -- <DGS 041318> check for employee vendor record here and create if it doesn't exist

            BEGIN
                debug_stmt := 'stmt 7';

                SELECT 'y'
                  INTO found_vend_pidm
                  FROM ftvvend
                 WHERE ftvvend_pidm = g_vend_pidm;
            EXCEPTION
                WHEN NO_DATA_FOUND
                THEN
                    p_insrt_vend_record;
                WHEN OTHERS
                THEN
                    l_msg := 'Error checking vendor: ' || g_vend_pidm;
            END;

            --If this is an Amex invoice adjust the invoice due date if necessary
            IF g_inv_type = 'AMX'
            THEN
                IF g_pmt_due_date <= g_trans_date
                THEN
                    debug_stmt := 'stmt 8';
                    g_pmt_due_date :=
                        CASE
                            WHEN TO_NUMBER (TO_CHAR (g_trans_date, 'dd')) <
                                 17
                            THEN
                                  g_trans_date
                                + 18
                                - TO_NUMBER (TO_CHAR (g_trans_date, 'dd'))
                            WHEN TO_NUMBER (TO_CHAR (g_trans_date, 'dd')) >=
                                 17
                            THEN
                                  ADD_MONTHS (g_trans_date, 1)
                                + 18
                                - TO_NUMBER (TO_CHAR (g_trans_date, 'dd'))
                        END;
                END IF;
            END IF;

            -- Create the invoice
            IF l_msg IS NULL
            THEN
                debug_stmt := 'stmt 9';
                -- VR 9/30/19 INC0089249 added 'i.appr.amt' parameter to pass amount and set CR memo indicator, and rucl code conditionally
                p_get_inv_header (l_msg, i.appr_amt);

                --  dbms_output.put_line('Get inv header msg ');
                --  dbms_output.put_line(l_msg);

                IF l_msg IS NULL
                THEN
                    --
                    --  dbms_output.put_line('Create inv header');
                    debug_stmt := 'stmt 10';
                    p_create_header (l_msg);

                    --
                    --   dbms_output.put_line(l_msg);

                    IF l_msg IS NULL
                    THEN
                        debug_stmt := 'stmt 11';
                        p_create_items (l_msg);
                    -- dbms_output.put_line('Create lines msg ');
                    -- dbms_output.put_line(l_msg);

                    END IF;

                    --
                    IF l_msg IS NULL
                    THEN
                        --      dbms_output.put_line(' calling p_complete '||l_msg);
                        debug_stmt := 'stmt 12';
                        p_complete (l_msg);
                    --      dbms_output.put_line('p_complete '||l_msg);
                    END IF;
                --
                END IF;
            END IF;

            --  dbms_output.put_line('just before update status');
            -- Update status
            debug_stmt := 'stmt 13';
            p_done;
            debug_stmt := 'stmt 13a';

            --   dbms_output.put_line('just after p_done');
            debug_stmt := 'stmt 14';
            p_update_log;

            --   dbms_output.put_line('just after p_update_log');

            -- Send Error email if needed
            BEGIN
                debug_stmt := 'stmt 15';

                SELECT DISTINCT fzreexp_err_msg
                  INTO l_msg
                  FROM fzreexp
                 WHERE fzreexp_err_msg IS NOT NULL
                   AND fzreexp_inv_num = i.vend_inv_code
                   AND fzreexp_batch_id = p_batch_id;

                debug_stmt := 'stmt 16';
                l_msg := i.vend_inv_code || ': ' || l_msg;
                p_send_error_email (l_msg);
            EXCEPTION
                WHEN NO_DATA_FOUND
                THEN
                    NULL;
                WHEN OTHERS
                THEN
                    p_send_error_email (SQLERRM);
            END;
        --
        END LOOP;
    EXCEPTION
        WHEN OTHERS
        THEN
            p_msg := SUBSTR (SQLERRM, 1, 80) || ' ' || debug_stmt; -- <DGS 08202018>

            UPDATE fzreexp
               SET fzreexp_invh_code = g_inv_code
                  ,fzreexp_status = 'E'
                  ,fzreexp_err_msg = p_msg || ' ' || g_vend_inv_code
                  ,fzreexp_load_date = TRUNC (SYSDATE)
             WHERE fzreexp_inv_num = g_vend_inv_code
               AND fzreexp_batch_id = p_batch_id;

            p_send_error_email (g_vend_inv_code || ' ' || p_msg); --  <DGS 08202018>
    END p_processinvoice;

    -----------------------------------------------------------------------
    PROCEDURE p_reset_error (p_msg IN OUT VARCHAR2, p_err_type IN VARCHAR2)
    IS
        batch_count    NUMBER;
        batch_total    NUMBER (12, 2);
        next_batch     NUMBER;
        inv_posted     VARCHAR2 (1) := 'N';
        item_num       NUMBER := 0;
        l_msg          VARCHAR2 (100) := NULL;

        CURSOR reset_batch IS
            SELECT DISTINCT fzreexp_invh_code invh_code, fzreexp_batch_id
              FROM fzreexp
             WHERE fzreexp_status = 'E'
               AND fzreexp_err_msg LIKE '%NSF%'
               AND p_err_type IN ('N', 'A')
            UNION
            SELECT DISTINCT fzreexp_invh_code invh_code, fzreexp_batch_id
              FROM fzreexp
             WHERE fzreexp_status = 'E'
               AND fzreexp_err_msg NOT LIKE '%NSF%'
               AND p_err_type IN ('O', 'A');
    BEGIN
        /* select the next batch number */

        IF l_msg IS NULL
        THEN
            SELECT fimsmgr.cr_batch_seq.NEXTVAL INTO next_batch FROM DUAL;
        END IF;

        FOR i IN reset_batch
        LOOP
            l_msg := NULL;

            /* check that the invoice didn't post */
            BEGIN
                SELECT 'Y'
                  INTO inv_posted
                  FROM fgbtrnd
                 WHERE fgbtrnd_doc_code = i.invh_code;
            EXCEPTION
                WHEN TOO_MANY_ROWS
                THEN
                    inv_posted := 'Y'; -- VR 9/30/2019 INC0089249 Hotfix - added "when too_many_rows then inv_posted := 'Y';"
                WHEN NO_DATA_FOUND
                THEN
                    inv_posted := 'N';
                WHEN OTHERS
                THEN
                    l_msg := SUBSTR (SQLERRM, 1, 100);
            END;

            IF inv_posted <> 'Y'
            THEN
                /* clean out the records from the Banner tables */
                /* Note - invoices that have already posted will fail when the new batch is processed.
                This is intentional so that we have the processing history */

                BEGIN
                    DELETE FROM farinva
                          WHERE farinva_invh_code = i.invh_code;
                EXCEPTION
                    WHEN NO_DATA_FOUND
                    THEN
                        NULL;
                    WHEN OTHERS
                    THEN
                        l_msg := SUBSTR (SQLERRM, 1, 100);
                END;

                BEGIN
                    DELETE FROM farinvc
                          WHERE farinvc_invh_code = i.invh_code;
                EXCEPTION
                    WHEN NO_DATA_FOUND
                    THEN
                        NULL;
                    WHEN OTHERS
                    THEN
                        l_msg := SUBSTR (SQLERRM, 1, 100);
                END;

                BEGIN
                    DELETE FROM fabinvh
                          WHERE fabinvh_code = i.invh_code;
                EXCEPTION
                    WHEN NO_DATA_FOUND
                    THEN
                        NULL;
                    WHEN OTHERS
                    THEN
                        l_msg := SUBSTR (SQLERRM, 1, 100);
                END;

                COMMIT;

                --end if;

                --VR 9/30/2019 INC0089249 Hotfix - Commented out if statement
                --IF L_MSG is not null (in the case that inv_posted query return too many records , or if deletion fails) it may get overwritten in the loop and never reported. VR 9/30

                -- for j in (select fzreexp_item_no item_no from fzreexp where fzreexp_invh_code = i.invh_code and fzreexp_status = 'E') loop
                --   item_num := item_num + 1;
                INSERT INTO fzreexp
                    SELECT next_batch
                          ,NULL
                          ,fzreexp_inv_num
                          ,fzreexp_external_doc_id
                          ,fzreexp_inv_type
                          ,fzreexp_exp_export_date
                          ,fzreexp_amx_stmt_date
                          ,fzreexp_amx_export_date
                          ,fzreexp_amx_chid
                          ,fzreexp_inv_due_date
                          ,fzreexp_inv_desc
                          ,fzreexp_foapl
                          ,fzreexp_trans_date
                          ,fzreexp_item_no
                          ,fzreexp_comm_desc
                          ,fzreexp_index
                          ,fzreexp_acct_code
                          ,fzreexp_appr_amt
                          ,fzreexp_bank_code
                          ,DECODE (l_msg, NULL, 'N', 'E')
                          ,l_msg
                          ,TRUNC (SYSDATE)
                          ,NULL
                          ,fzreexp_vend_id
                          ,NULL
                          ,NULL
                      FROM fzreexp
                     WHERE fzreexp_invh_code = i.invh_code
                       --    and fzreexp_item_no = j.item_no
                       AND fzreexp_status = 'E';

                --  end loop;
                /* Update the prior batch record status */
                UPDATE fzreexp
                   SET fzreexp_status = 'R'
                 WHERE fzreexp_invh_code = i.invh_code
                   AND fzreexp_batch_id = i.fzreexp_batch_id
                   AND fzreexp_status = 'E';
            --else
            --end if; --VR 9/30/2019 INC0089249 Hotfix - Commented out if statement
            ELSE /*Posted, no further processing */
                /* Update the prior batch record status */
                /* VR 9/30/2019 INC0089249 Hotfix added else statement*/
                UPDATE fzreexp
                   SET fzreexp_status = 'R'
                 WHERE fzreexp_invh_code = i.invh_code
                   AND fzreexp_batch_id = i.fzreexp_batch_id
                   AND fzreexp_status = 'E';
            END IF;
        --
        END LOOP;

        BEGIN
            SELECT COUNT (*), SUM (fzreexp_appr_amt)
              INTO batch_count, batch_total
              FROM fzreexp
             WHERE fzreexp_status = 'N';
        END;

        BEGIN
            INSERT INTO fzbeexp
                 VALUES (next_batch
                        ,NULL
                        ,'FZKEEXP ' || TO_CHAR (SYSDATE, 'yyyymmdd')
                        ,batch_count
                        ,batch_total
                        ,'N'
                        ,NULL);
        END;

        p_processinvoice (next_batch);

        UPDATE fzbeexp
           SET fzbeexp_status = 'C'
         WHERE fzbeexp_batch_id = next_batch;

        p_msg :=
               'Batch Id '
            || TO_CHAR (next_batch)
            || NVL (l_msg, ': Error Processing Completed Successfully');
    EXCEPTION
        WHEN OTHERS
        THEN
            p_msg := SUBSTR (SQLERRM, 1, 100);
    END p_reset_error;

    --------------------------------------------------------------------------------
    -- <DGS 06042018> Start
    PROCEDURE p_reprocess_dupebatch (p_batch_id IN VARCHAR2
                                    ,p_external_batch_id IN VARCHAR2
                                    ,p_msg IN OUT VARCHAR2)
    IS
        batch_count    NUMBER;
        batch_total    NUMBER (12, 2);
    BEGIN
        SELECT COUNT (*), SUM (fzreexp_appr_amt)
          INTO batch_count, batch_total
          FROM fzreexp
         WHERE fzreexp_status = 'X'
           AND fzreexp_external_batch_id = p_external_batch_id
           AND fzreexp_batch_id = p_batch_id;

        INSERT INTO fzbeexp
             VALUES (NULL
                    ,NULL
                    ,'FZKEEXP Dupe ' || TO_CHAR (SYSDATE, 'yyyymmdd')
                    ,batch_count
                    ,batch_total
                    ,'N'
                    ,NULL);

        INSERT INTO fzreexp
            SELECT NULL
                  ,NULL
                  ,fzreexp_inv_num
                  ,fzreexp_external_doc_id
                  ,fzreexp_inv_type
                  ,fzreexp_exp_export_date
                  ,fzreexp_amx_stmt_date
                  ,fzreexp_amx_export_date
                  ,fzreexp_amx_chid
                  ,fzreexp_inv_due_date
                  ,fzreexp_inv_desc
                  ,fzreexp_foapl
                  ,fzreexp_trans_date
                  ,fzreexp_item_no
                  ,fzreexp_comm_desc
                  ,fzreexp_index
                  ,fzreexp_acct_code
                  ,fzreexp_appr_amt
                  ,fzreexp_bank_code
                  ,'N'
                  ,NULL
                  ,TRUNC (SYSDATE)
                  ,NULL
                  ,fzreexp_vend_id
                  ,NULL
                  ,NULL
              FROM fzreexp
             WHERE fzreexp_batch_id = p_batch_id
               AND fzreexp_external_batch_id = p_external_batch_id
               AND fzreexp_status = 'X';

        p_processbatch;

        p_msg :=
               'Batch Id '
            || TO_CHAR (p_batch_id)
            || ' for '
            || p_external_batch_id
            || ' created successfully';
    EXCEPTION
        WHEN OTHERS
        THEN
            p_msg := SUBSTR (SQLERRM, 1, 100);
    END p_reprocess_dupebatch;

    -- <DGS 06042018> End
    --------------------------------------------------------------------------------
    PROCEDURE p_renumber_batch (p_batch_id IN VARCHAR2
                               ,p_msg IN OUT VARCHAR2)
    IS
        CURSOR distinct_invoices (p_batch_id IN VARCHAR2)
        IS
            SELECT DISTINCT fzreexp_inv_num     inv_num
              FROM fzreexp
             WHERE fzreexp_batch_id = p_batch_id;

        CURSOR inv_lines (p_batch_id IN VARCHAR2, p_inv_num IN VARCHAR2)
        IS
              SELECT fzreexp_item_no
                FROM fzreexp
               WHERE fzreexp_batch_id = p_batch_id
                 AND fzreexp_inv_num = p_inv_num
            ORDER BY fzreexp_item_no;

        cntr           NUMBER := 0;
    BEGIN
        FOR i IN distinct_invoices (p_batch_id)
        LOOP
            cntr := 0;

            FOR j IN inv_lines (p_batch_id, i.inv_num)
            LOOP
                BEGIN
                    cntr := cntr + 1;

                    UPDATE fzreexp
                       SET fzreexp_item_no = cntr
                     WHERE fzreexp_batch_id = p_batch_id
                       AND fzreexp_inv_num = i.inv_num
                       AND fzreexp_item_no = j.fzreexp_item_no;
                EXCEPTION
                    WHEN OTHERS
                    THEN
                        p_msg := SUBSTR (SQLERRM, 1, 80);
                END;
            END LOOP;
        END LOOP;
    EXCEPTION
        WHEN OTHERS
        THEN
            p_msg := SUBSTR (SQLERRM, 1, 80);
    END;

    ---------------------------------------------------------------------
    PROCEDURE get_paid_expenses
    IS
        CURSOR paid_expense_list IS
            SELECT DISTINCT fabinck_invh_code
                           ,fabinck_check_num
                           ,fabinck_bank_code
                           ,fzreexp_inv_num
                           ,fzreexp_external_doc_id
                           ,fabinck_net_amt
                           ,fzreexp_inv_type
                           ,fabinck_check_date
              FROM fabinck, fabinvh, fzreexp
             WHERE fabinck_invh_code = fabinvh_code
               AND fabinck_invh_code = fzreexp_invh_code
               AND fzreexp_status = 'C'
               AND fzreexp_feed_date IS NULL
               AND fabinvh_open_paid_ind = 'P'
               AND fabinck_cancel_date IS NULL
               AND fabinvh_cancel_date IS NULL
            UNION /*INC0102709 VER2113 Added union below for marking CARETURNs paid in CR*/
              SELECT MAX (fzreexp_invh_code)          AS fabinck_invh_code
                    ,'CARETURN'                       AS fabinck_check_num
                    ,'14'                             AS fabinck_bank_code
                    ,MAX (fzreexp_inv_num)            AS fzreexp_inv_num
                    ,MAX (fzreexp_external_doc_id)    AS fzreexp_external_doc_id
                    ,SUM (fzreexp_appr_amt)           AS fabinck_net_amt
                    ,MAX (fzreexp_inv_type)           AS fzreexp_inv_type
                    ,TRUNC (SYSDATE)                  AS fabinck_check_date
                FROM fzreexp
               WHERE fzreexp_feed_date IS NULL AND fzreexp_status = 'C'
            GROUP BY fzreexp_external_doc_id
              HAVING SUM (fzreexp_appr_amt) = 0;

        CURSOR get_voids IS
              SELECT MAX (fabinck_cancel_date) cancel_date, fabinck_invh_code
                FROM fabinck, fzreexp
               WHERE fabinck_invh_code = fzreexp_invh_code
                 AND fabinck_cancel_date IS NOT NULL
                 AND fzreexp_status = 'C'
                 AND NOT EXISTS
                         (SELECT 'y'
                            FROM fzrexpd
                           WHERE fzrexpd_invh_code = fabinck_invh_code
                             AND fzrexpd_check_num =
                                 'VOID ' || fabinck_check_num)
            GROUP BY fabinck_invh_code;

        CURSOR get_reissues IS
              SELECT MAX (fabinck_check_date) check_date, fabinck_invh_code
                FROM fabinck, fzreexp
               WHERE fabinck_invh_code = fzreexp_invh_code
                 AND fabinck_cancel_date IS NULL
                 AND fzreexp_status = 'C'
                 AND NOT EXISTS
                         (SELECT 'y'
                            FROM fzrexpd
                           WHERE fzrexpd_invh_code = fabinck_invh_code
                             AND fzrexpd_check_num = fabinck_check_num)
            GROUP BY fabinck_invh_code;

        CURSOR get_cash_adv_returns IS
            SELECT tbrmisd_receipt_number
                  ,tbrmisd_pidm
                  ,tbrmisd_amount
                  ,tbrmisd_feed_date
                  ,tbrmisd_payment_id
              FROM taismgr.tbrmisd, fimsmgr.cr_inv_hdr_vw
             WHERE 'CR' || tbrmisd_payment_id = fzreexp_inv_num
               AND fzreexp_inv_type = 'ADV'
               AND fzreexp_status = 'C'
               AND tbrmisd_charge_detail_code = 'CRAR'
               AND NOT EXISTS
                       (SELECT 'y'
                          FROM fzrexpd
                         WHERE fzrexpd_inv_num = 'CR' || tbrmisd_payment_id
                           AND fzrexpd_check_num =
                               TO_CHAR (tbrmisd_receipt_number));

        g_line_cntr    NUMBER := 0;
        g_ck_num       fzrexpd.fzrexpd_check_num%TYPE;
        g_net_amt      fzrexpd.fzrexpd_check_amt%TYPE;
        g_bank_code    fzrexpd.fzrexpd_bank_id%TYPE;
        g_inv_num      fzrexpd.fzrexpd_inv_num%TYPE;
        g_external_doc_id fzrexpd.fzrexpd_external_doc_id%TYPE;
        adv_external_doc_id fzrexpd.fzrexpd_external_doc_id%TYPE;
        l_msg          VARCHAR2 (100);
    BEGIN
        g_line_cntr := 0;

        FOR i IN paid_expense_list
        LOOP
            g_line_cntr := g_line_cntr + 1;
            --insert to fzrexpd table
            p_insrt_fzrexpd (p_invh_code => i.fabinck_invh_code
                            ,p_check_num => i.fabinck_check_num
                            ,p_check_amt => i.fabinck_net_amt
                            ,p_bank_id   => i.fabinck_bank_code
                            ,p_inv_num   => i.fzreexp_inv_num
                            ,p_check_date => i.fabinck_check_date
                            ,p_external_doc_id => i.fzreexp_external_doc_id
                            ,p_feed_lineno => g_line_cntr
                            ,p_msg       => l_msg);
        END LOOP;

        /* INC0102709 VER2113 Change made on 2/4/2020 For Cash Advance Return
           Two Loops below cause errors and are not currently in use so they're being commented out temporarily
        for j in get_voids loop

           g_line_cntr := g_line_cntr + 1;

           select 'VOID '||fabinck_check_num, fabinck_net_amt, fabinck_bank_code,
                  fzreexp_inv_num, fzreexp_external_doc_id
             into g_ck_num, g_net_amt, g_bank_code, g_inv_num, g_external_doc_id
             from fabinck, fzreexp
            where fabinck_invh_code = fzreexp_invh_code
              and fzreexp_status = 'C'
              and fabinck_invh_code = j.fabinck_invh_code
              and fabinck_cancel_date = j.cancel_date;

          -- insert to fzrexpd table
            p_insrt_fzrexpd(p_invh_code => j.fabinck_invh_code,
                            p_check_num => g_ck_num,
                            p_check_amt => g_net_amt,
                            p_bank_id  => g_bank_code,
                            p_inv_num => g_inv_num,
                            p_check_date => j.cancel_date,
                            p_external_doc_id => g_external_doc_id,
                            p_feed_lineno => g_line_cntr,
                            p_msg => l_msg);
        end loop;

        for k in get_reissues loop

        select fabinck_check_num, fabinck_net_amt, fabinck_bank_code,
                  fzreexp_inv_num, fzreexp_external_doc_id
             into g_ck_num, g_net_amt, g_bank_code, g_inv_num, g_external_doc_id
             from fabinck, fzreexp
            where fabinck_invh_code = fzreexp_invh_code
              and fzreexp_status = 'C'
              and fabinck_invh_code = k.fabinck_invh_code
              and fabinck_check_date = k.check_date;

           g_line_cntr := g_line_cntr + 1;
          -- insert to fzrexpd table
            p_insrt_fzrexpd(p_invh_code => k.fabinck_invh_code,
                            p_check_num => g_ck_num,
                            p_check_amt => g_net_amt,
                            p_bank_id  => g_bank_code,
                            p_inv_num => g_inv_num,
                            p_check_date => k.check_date,
                            p_external_doc_id => g_external_doc_id,
                            p_feed_lineno => g_line_cntr,
                            p_msg => l_msg);
        end loop;
        */
        FOR l IN get_cash_adv_returns
        LOOP
            /* INC0102709 VER2113 Change made on 2/4/2020 For Cash Advance Return. Added 'distinct' below as a precaution*/
            SELECT DISTINCT fzreexp_external_doc_id
              INTO adv_external_doc_id
              FROM fzreexp
             WHERE fzreexp_inv_num = 'CR' || l.tbrmisd_payment_id;

            g_line_cntr := g_line_cntr + 1;
            /* INC0102709 VER2113 Change made on 2/4/2020 For Cash Advance Return. Added 'CR'|| below*/
            p_insrt_fzrexpd (p_invh_code => NULL
                            ,p_check_num => l.tbrmisd_receipt_number
                            ,p_check_amt => l.tbrmisd_amount
                            ,p_bank_id   => '14'
                            ,p_inv_num   => 'CR' || l.tbrmisd_payment_id
                            ,p_check_date => l.tbrmisd_feed_date
                            ,p_external_doc_id => adv_external_doc_id
                            ,p_feed_lineno => g_line_cntr
                            ,p_msg       => l_msg);

            IF l_msg IS NOT NULL
            THEN
                tcapp.util_msg.logentry ('CashAdvReturn error ' || l_msg);
                DBMS_OUTPUT.put_line (l_msg);
            END IF;
        END LOOP;
    EXCEPTION
        WHEN OTHERS
        THEN
            DBMS_OUTPUT.put_line (SQLERRM);
    END;

    PROCEDURE p_insrt_fzrexpd (p_invh_code IN VARCHAR2
                              ,p_check_num IN VARCHAR2
                              ,p_check_amt IN VARCHAR2
                              ,p_bank_id IN VARCHAR2
                              ,p_inv_num IN VARCHAR2
                              ,p_check_date IN DATE
                              ,p_external_doc_id IN VARCHAR2
                              ,p_feed_lineno IN NUMBER
                              ,p_msg IN OUT VARCHAR2)
    IS
    BEGIN
        p_msg := NULL;

        INSERT INTO fzrexpd p_insrt_fzrexpd (fzrexpd_invh_code
                                            ,fzrexpd_check_num
                                            ,fzrexpd_check_amt
                                            ,fzrexpd_bank_id
                                            ,fzrexpd_inv_num
                                            ,fzrexpd_check_date
                                            ,fzrexpd_external_doc_id
                                            ,fzrexpd_feed_date
                                            ,fzrexpd_feed_doc_id
                                            ,fzrexpd_feed_lineno)
             VALUES (p_invh_code
                    ,p_check_num
                    ,p_check_amt
                    ,p_bank_id
                    ,p_inv_num
                    ,p_check_date
                    ,p_external_doc_id
                    ,NULL
                    ,NULL
                    ,p_feed_lineno);
    EXCEPTION
        WHEN OTHERS
        THEN
            p_msg := SUBSTR (SQLERRM, 1, 80);
            DBMS_OUTPUT.put_line (p_msg);
    END;

    FUNCTION f_chrome_multi_sup_code (p_pidm NUMBER, p_code VARCHAR2)
        RETURN VARCHAR2
    IS
        /*-------------------------------------------------------------------------
        -- Use to identify employees in the CR person file feed with multiple
        -- supervisors and supervisors of employees who have multiple supervisors
        --------------------------------------------------------------------------*/

        --count of supervisors and list of supervisors
        CURSOR c_sup (v_pidm NUMBER)
        IS
            SELECT COUNT (supervisor)                        cnt
                  ,LISTAGG (supervisor, ';')
                       WITHIN GROUP (ORDER BY supervisor)    sup
              FROM (SELECT DISTINCT DECODE
                                    (
                                        n.nbrbjob_posn
                                       ,'999999', ''
                                       , (SELECT c.spriden_id
                                            FROM spriden c
                                           WHERE c.spriden_pidm =
                                                 (NVL
                                                  (
                                                      tcapp.hr_report_util.f_hrfindjqueapprover
                                                      (
                                                          a.spriden_pidm
                                                         ,n.nbrbjob_posn
                                                         ,n.nbrbjob_suff
                                                         ,'WTE'
                                                      )
                                                     ,tcapp.hr_report_util.f_hrfindposnsupervisor
                                                      (
                                                          n.nbrbjob_posn
                                                         ,TRUNC (SYSDATE)
                                                      )
                                                  ))
                                             AND c.spriden_ntyp_code = 'UNI'
                                             AND c.spriden_entity_ind = 'P'
                                             AND c.spriden_pidm <>
                                                 a.spriden_pidm)
                                    )    supervisor
                      FROM spriden a, nbrbjob n
                     WHERE a.spriden_change_ind IS NULL
                       AND a.spriden_entity_ind = 'P'
                       AND a.spriden_pidm = n.nbrbjob_pidm
                       AND TRUNC (SYSDATE) BETWEEN TRUNC
                                                   (
                                                       n.nbrbjob_begin_date
                                                   )
                                               AND TRUNC
                                                   (
                                                       NVL
                                                       (
                                                           n.nbrbjob_end_date
                                                          ,SYSDATE
                                                       )
                                                   )
                       AND spriden_pidm = v_pidm
                    UNION
                    SELECT fzkeexp.f_daa_lkup (nbrjobs_orgn_code_ts
                                              ,a.spriden_pidm)
                      FROM spriden a, nbrbjob n, nbbposn, nbrjobs
                     WHERE a.spriden_change_ind IS NULL
                       AND a.spriden_entity_ind = 'P'
                       AND a.spriden_pidm = n.nbrbjob_pidm
                       AND n.nbrbjob_contract_type = 'P'
                       AND TRUNC (SYSDATE) BETWEEN TRUNC
                                                   (
                                                       n.nbrbjob_begin_date
                                                   )
                                               AND TRUNC
                                                   (
                                                       NVL
                                                       (
                                                           n.nbrbjob_end_date
                                                          ,SYSDATE
                                                       )
                                                   )
                       AND n.nbrbjob_posn = nbbposn_posn
                       AND a.spriden_pidm = nbrjobs_pidm
                       AND nbrjobs_status <> 'T'
                       AND nbrbjob_contract_type = 'P'
                       AND nbrjobs_pidm = nbrbjob_pidm
                       AND nbrjobs_posn = nbrbjob_posn
                       AND nbrjobs_suff = nbrbjob_suff
                       AND nbrjobs_effective_date =
                           (SELECT MAX (n.nbrjobs_effective_date)
                              FROM nbrjobs n
                             WHERE n.nbrjobs_suff = nbrbjob_suff
                               AND n.nbrjobs_posn = nbrbjob_posn
                               AND n.nbrjobs_pidm = nbrbjob_pidm
                               AND n.nbrjobs_status <> 'T'
                               AND TRUNC (n.nbrjobs_effective_date) <=
                                   TRUNC (SYSDATE))
                       AND nbbposn_ecls_code IN ('11'
                                                ,'20'
                                                ,'24'
                                                ,'25'
                                                ,'30'
                                                ,'31'
                                                ,'35'
                                                ,'39'
                                                ,'81')
                       AND EXISTS
                               (SELECT 'y'
                                  FROM spriden b
                                 WHERE b.spriden_pidm = a.spriden_pidm
                                   AND b.spriden_ntyp_code = 'UNI'
                                   AND b.spriden_entity_ind = 'P')
                       AND a.spriden_pidm = v_pidm);

        --get employees for position of supervisor
        CURSOR c_emp (posn VARCHAR2)
        IS
            SELECT a.nbrjobs_pidm     pidm
              FROM nbrjobs  a
                   JOIN
                   (SELECT *
                      FROM nbbposn a1
                     WHERE a1.nbbposn_posn_reports = posn
                       AND TRUNC (SYSDATE) BETWEEN TRUNC
                                                   (
                                                       a1.nbbposn_begin_date
                                                   )
                                               AND TRUNC
                                                   (
                                                       NVL
                                                       (
                                                           a1.nbbposn_end_date
                                                          ,SYSDATE
                                                       )
                                                   )
                       AND a1.nbbposn_status = 'A') b
                       ON a.nbrjobs_posn = b.nbbposn_posn
             WHERE a.nbrjobs_effective_date =
                   (SELECT MAX (nbrjobs_effective_date)
                      FROM nbrjobs
                     WHERE nbrjobs_pidm = a.nbrjobs_pidm
                       AND nbrjobs_posn = a.nbrjobs_posn
                       AND nbrjobs_suff = a.nbrjobs_suff
                       AND nbrjobs_effective_date <= SYSDATE)
               AND nbrjobs_status = 'A'
            UNION
            SELECT b.nbrrjqe_pidm     pidm
              FROM nbrjobs  a
                   LEFT OUTER JOIN nbrrjqe b
                       ON nbrjobs_pidm = nbrrjqe_appr_pidm
             WHERE a.nbrjobs_pidm IN (p_pidm)
               AND a.nbrjobs_effective_date =
                   (SELECT MAX (nbrjobs_effective_date)
                      FROM nbrjobs
                     WHERE nbrjobs_pidm = a.nbrjobs_pidm
                       AND nbrjobs_posn = a.nbrjobs_posn
                       AND nbrjobs_suff = a.nbrjobs_suff
                       AND nbrjobs_effective_date <= SYSDATE)
               AND nbrjobs_status = 'A';

        --get positions for emp passed in
        CURSOR c_posn IS
            SELECT a.nbrjobs_posn
              FROM nbrjobs a
             WHERE a.nbrjobs_pidm = p_pidm
               AND a.nbrjobs_effective_date =
                   (SELECT MAX (nbrjobs_effective_date)
                      FROM nbrjobs
                     WHERE nbrjobs_pidm = a.nbrjobs_pidm
                       AND nbrjobs_posn = a.nbrjobs_posn
                       AND nbrjobs_suff = a.nbrjobs_suff
                       AND nbrjobs_effective_date <= SYSDATE)
               AND nbrjobs_status = 'A';

        CURSOR c_sub_with_mult_sups (p_posn VARCHAR2)
        IS
            SELECT COUNT (emp_pidm)     cnt
              FROM (  SELECT a.spriden_pidm    emp_pidm
                            ,COUNT
                             (
                                 DECODE
                                 (
                                     n.nbrbjob_posn
                                    ,'999999', ''
                                    , (SELECT c.spriden_id
                                         FROM spriden c
                                        WHERE c.spriden_pidm =
                                              (NVL
                                               (
                                                   tcapp.hr_report_util.f_hrfindjqueapprover
                                                   (
                                                       a.spriden_pidm
                                                      ,n.nbrbjob_posn
                                                      ,n.nbrbjob_suff
                                                      ,'WTE'
                                                   )
                                                  ,tcapp.hr_report_util.f_hrfindposnsupervisor
                                                   (
                                                       n.nbrbjob_posn
                                                      ,TRUNC (SYSDATE)
                                                   )
                                               ))
                                          AND c.spriden_ntyp_code = 'UNI'
                                          AND c.spriden_entity_ind = 'P'
                                          AND c.spriden_pidm <> a.spriden_pidm)
                                 )
                             )                 supervisor
                        FROM spriden a, nbrbjob n
                       WHERE a.spriden_change_ind IS NULL
                         AND a.spriden_entity_ind = 'P'
                         AND a.spriden_pidm = n.nbrbjob_pidm
                         AND TRUNC (SYSDATE) BETWEEN TRUNC
                                                     (
                                                         n.nbrbjob_begin_date
                                                     )
                                                 AND TRUNC
                                                     (
                                                         NVL
                                                         (
                                                             n.nbrbjob_end_date
                                                            ,SYSDATE
                                                         )
                                                     )
                         AND a.spriden_pidm IN
                                 -------------------------------------------------------------
                                 (SELECT a.nbrjobs_pidm
                                    FROM nbrjobs a
                                         JOIN
                                         (SELECT *
                                            FROM nbbposn a1
                                           WHERE a1.nbbposn_posn_reports =
                                                 p_posn
                                             AND TRUNC (SYSDATE) BETWEEN TRUNC
                                                                         (
                                                                             a1.nbbposn_begin_date
                                                                         )
                                                                     AND TRUNC
                                                                         (
                                                                             NVL
                                                                             (
                                                                                 a1.nbbposn_end_date
                                                                                ,SYSDATE
                                                                             )
                                                                         )
                                             AND a1.nbbposn_status =
                                                 'A') b
                                             ON a.nbrjobs_posn =
                                                b.nbbposn_posn
                                   WHERE a.nbrjobs_effective_date =
                                         (SELECT MAX (nbrjobs_effective_date)
                                            FROM nbrjobs
                                           WHERE nbrjobs_pidm = a.nbrjobs_pidm
                                             AND nbrjobs_posn = a.nbrjobs_posn
                                             AND nbrjobs_suff = a.nbrjobs_suff
                                             AND nbrjobs_effective_date <=
                                                 SYSDATE)
                                     AND nbrjobs_status = 'A'
                                  UNION
                                  SELECT b.nbrrjqe_pidm
                                    FROM nbrjobs a
                                         LEFT OUTER JOIN nbrrjqe b
                                             ON nbrjobs_pidm =
                                                nbrrjqe_appr_pidm
                                   WHERE a.nbrjobs_pidm = p_pidm
                                     AND a.nbrjobs_effective_date =
                                         (SELECT MAX (nbrjobs_effective_date)
                                            FROM nbrjobs
                                           WHERE nbrjobs_pidm = a.nbrjobs_pidm
                                             AND nbrjobs_posn = a.nbrjobs_posn
                                             AND nbrjobs_suff = a.nbrjobs_suff
                                             AND nbrjobs_effective_date <=
                                                 SYSDATE)
                                     AND nbrjobs_status = 'A')
                    GROUP BY a.spriden_pidm
                      HAVING COUNT
                             (
                                 DECODE
                                 (
                                     n.nbrbjob_posn
                                    ,'999999', ''
                                    , (SELECT c.spriden_id
                                         FROM spriden c
                                        WHERE c.spriden_pidm =
                                              (NVL
                                               (
                                                   tcapp.hr_report_util.f_hrfindjqueapprover
                                                   (
                                                       a.spriden_pidm
                                                      ,n.nbrbjob_posn
                                                      ,n.nbrbjob_suff
                                                      ,'WTE'
                                                   )
                                                  ,tcapp.hr_report_util.f_hrfindposnsupervisor
                                                   (
                                                       n.nbrbjob_posn
                                                      ,TRUNC (SYSDATE)
                                                   )
                                               ))
                                          AND c.spriden_ntyp_code = 'UNI'
                                          AND c.spriden_entity_ind = 'P'
                                          AND c.spriden_pidm <>
                                              a.spriden_pidm)
                                 )
                             ) >
                             1
                    ------------------------------------------------------------
                    UNION
                      SELECT a.spriden_pidm
                            ,COUNT
                             (
                                 fzkeexp.f_daa_lkup (nbrjobs_orgn_code_ts
                                                    ,a.spriden_pidm)
                             )
                        FROM spriden a, nbrbjob n, nbbposn, nbrjobs
                       WHERE a.spriden_change_ind IS NULL
                         AND a.spriden_entity_ind = 'P'
                         AND a.spriden_pidm = n.nbrbjob_pidm
                         AND n.nbrbjob_contract_type = 'P'
                         AND TRUNC (SYSDATE) BETWEEN TRUNC
                                                     (
                                                         n.nbrbjob_begin_date
                                                     )
                                                 AND TRUNC
                                                     (
                                                         NVL
                                                         (
                                                             n.nbrbjob_end_date
                                                            ,SYSDATE
                                                         )
                                                     )
                         AND n.nbrbjob_posn = nbbposn_posn
                         AND a.spriden_pidm = nbrjobs_pidm
                         AND nbrjobs_status <> 'T'
                         AND nbrbjob_contract_type = 'P'
                         AND nbrjobs_pidm = nbrbjob_pidm
                         AND nbrjobs_posn = nbrbjob_posn
                         AND nbrjobs_suff = nbrbjob_suff
                         AND nbrjobs_effective_date =
                             (SELECT MAX (n.nbrjobs_effective_date)
                                FROM nbrjobs n
                               WHERE n.nbrjobs_suff = nbrbjob_suff
                                 AND n.nbrjobs_posn = nbrbjob_posn
                                 AND n.nbrjobs_pidm = nbrbjob_pidm
                                 AND n.nbrjobs_status <> 'T'
                                 AND TRUNC (n.nbrjobs_effective_date) <=
                                     TRUNC (SYSDATE))
                         AND nbbposn_ecls_code IN ('11'
                                                  ,'20'
                                                  ,'24'
                                                  ,'25'
                                                  ,'30'
                                                  ,'31'
                                                  ,'35'
                                                  ,'39'
                                                  ,'81')
                         AND EXISTS
                                 (SELECT 'y'
                                    FROM spriden b
                                   WHERE b.spriden_pidm = a.spriden_pidm
                                     AND b.spriden_ntyp_code = 'UNI'
                                     AND b.spriden_entity_ind = 'P')
                         AND a.spriden_pidm IN
                                 -------------------------------------------------------------
                                 (SELECT a.nbrjobs_pidm
                                    FROM nbrjobs a
                                         JOIN
                                         (SELECT *
                                            FROM nbbposn a1
                                           WHERE a1.nbbposn_posn_reports =
                                                 p_posn
                                             AND TRUNC (SYSDATE) BETWEEN TRUNC
                                                                         (
                                                                             a1.nbbposn_begin_date
                                                                         )
                                                                     AND TRUNC
                                                                         (
                                                                             NVL
                                                                             (
                                                                                 a1.nbbposn_end_date
                                                                                ,SYSDATE
                                                                             )
                                                                         )
                                             AND a1.nbbposn_status =
                                                 'A') b
                                             ON a.nbrjobs_posn =
                                                b.nbbposn_posn
                                   WHERE a.nbrjobs_effective_date =
                                         (SELECT MAX (nbrjobs_effective_date)
                                            FROM nbrjobs
                                           WHERE nbrjobs_pidm = a.nbrjobs_pidm
                                             AND nbrjobs_posn = a.nbrjobs_posn
                                             AND nbrjobs_suff = a.nbrjobs_suff
                                             AND nbrjobs_effective_date <=
                                                 SYSDATE)
                                     AND nbrjobs_status = 'A'
                                  UNION
                                  SELECT b.nbrrjqe_pidm
                                    FROM nbrjobs a
                                         LEFT OUTER JOIN nbrrjqe b
                                             ON nbrjobs_pidm =
                                                nbrrjqe_appr_pidm
                                   WHERE a.nbrjobs_pidm = p_pidm
                                     AND a.nbrjobs_effective_date =
                                         (SELECT MAX (nbrjobs_effective_date)
                                            FROM nbrjobs
                                           WHERE nbrjobs_pidm = a.nbrjobs_pidm
                                             AND nbrjobs_posn = a.nbrjobs_posn
                                             AND nbrjobs_suff = a.nbrjobs_suff
                                             AND nbrjobs_effective_date <=
                                                 SYSDATE)
                                     AND nbrjobs_status = 'A')
                    ------------------------------------------------------------
                    GROUP BY a.spriden_pidm
                      HAVING COUNT
                             (
                                 fzkeexp.f_daa_lkup (nbrjobs_orgn_code_ts
                                                    ,a.spriden_pidm)
                             ) >
                             1);

        v_posn         VARCHAR2 (20);
        v_return       VARCHAR2 (100);
        v_chk_sub_sup  VARCHAR2 (1);
    BEGIN
        IF p_pidm IN (237145, 1150, 9027, 79745) AND p_code = 'CODE'
        THEN
            v_return := 'SP';
        ELSE
            v_chk_sub_sup := 'N';

            FOR r_posn IN c_posn
            LOOP
                FOR r_sub_with_mult_sups
                    IN c_sub_with_mult_sups (r_posn.nbrjobs_posn)
                LOOP
                    IF r_sub_with_mult_sups.cnt > 0
                    THEN
                        v_chk_sub_sup := 'Y';

                        EXIT;
                    END IF;
                END LOOP;

                IF v_chk_sub_sup = 'Y'
                THEN
                    EXIT;
                END IF;
            END LOOP;

            FOR r_sup IN c_sup (p_pidm)
            LOOP
                BEGIN
                    SELECT CASE
                               --More than 1 supervisor and no subordinates with multiple
                               --supervisors
                               WHEN p_code = 'CODE'
                                AND r_sup.cnt > 1
                                AND v_chk_sub_sup = 'N'
                               THEN
                                   'EE'
                               --1 or less supervisor and at least one subordinate with more
                               --than 1 supervisor
                               WHEN p_code = 'CODE'
                                AND r_sup.cnt <= 1
                                AND v_chk_sub_sup = 'Y'
                               THEN
                                   'SP'
                               --more than 1 supervisor and at least one subordinate with more
                               --than 1 supervisor
                               WHEN p_code = 'CODE'
                                AND r_sup.cnt > 1
                                AND v_chk_sub_sup = 'Y'
                               THEN
                                   'ES'
                               --only return list of supervisors if emp has more
                               --than 1
                               WHEN p_code = 'LIST' AND r_sup.cnt > 1
                               THEN
                                   r_sup.sup
                               ELSE
                                   NULL
                           END
                      INTO v_return
                      FROM DUAL;
                EXCEPTION
                    WHEN NO_DATA_FOUND
                    THEN
                        v_return := NULL;
                        GOTO end_loop;
                END;

               <<end_loop>>
                NULL;
            END LOOP;
        END IF;

        RETURN v_return;
    END;
--==============================================================================
--
END fzkeexp;
/
