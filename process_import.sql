set serveroutput on;

declare
p_msg varchar2(300);

begin
  fzkeexp.p_ProcessBatch;
exception
  when others then dbms_output.put_line(sqlerrm);
end;
/
show errors;
exit;

