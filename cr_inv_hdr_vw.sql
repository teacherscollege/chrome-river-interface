CREATE OR REPLACE VIEW "FIMSMGR"."CR_INV_HDR_VW" ("FZREEXP_BATCH_ID", "FZREEXP_EXTERNAL_BATCH_ID", "FZREEXP_INV_NUM", "FZREEXP_INV_TYPE", "FZREEXP_STATUS", "FZREEXP_EXP_EXPORT_DATE", "FZREEXP_AMX_STMT_DATE", "FZREEXP_AMX_EXPORT_DATE", "FZREEXP_AMX_CHID", "FZREEXP_INV_DUE_DATE", "FZREEXP_INV_DESC", "FZREEXP_ERR_MSG", "FZREEXP_LOAD_DATE", "FZREEXP_INVH_CODE", "FZREEXP_VEND_ID", "FZREEXP_FEED_DOC_ID", "FZREEXP_FEED_DATE") AS 
  select distinct fzreexp_batch_id,fzreexp_external_batch_id,fzreexp_inv_num, fzreexp_inv_type, fzreexp_status,
fzreexp_exp_export_date, 
fzreexp_amx_stmt_date, fzreexp_amx_export_date, fzreexp_amx_chid, 
fzreexp_inv_due_date, fzreexp_inv_desc,  fzreexp_err_msg, 
fzreexp_load_date, fzreexp_invh_code, fzreexp_vend_id, fzreexp_feed_doc_id, 
fzreexp_feed_date
from fzreexp
where fzreexp_status <> 'R'
order by fzreexp_inv_num, fzreexp_batch_id;