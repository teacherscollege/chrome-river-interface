SET DEFINE OFF;
SET ECHO OFF;
SET HEAD OFF;
SET FEEDBACK OFF;
SET TRIMSPOOL ON;
SET PAGESIZE 0;
SET LINESIZE 600;
SPOOL PersonExtract.lis;

SELECT the_data.dataline
  FROM (SELECT 1,
                  'FirstName'
               || '|'
               || 'LastName'
               || '|'
               || 'LoginID'
               || '|'
               || 'UniqueID'
               || '|'
               || 'ReportsTo'
               || '|'
               || 'Email'
               || '|'
               || 'Status'
               || '|'
               || 'Title'
               || '|'
               || 'Entity1'
               || '|'
               || 'Entity2'
               || '|'
               || 'Entity3'
               || '|'
               || 'CurrencyCode1'
               || '|'
               || 'Vendor1Code'
               || '|'
               || 'CurrencyCode2'
               || '|'
               || 'Vendor2Code'
               || '|'
               || 'UDF1'
               || '|'
               || 'UDF2'
               || '|'
               || 'UDF3'
               || '|'
               || 'DateMask'
               || '|'
               || 'CurrencyMask'
               || '|'
               || 'ExpenseBar'
               || '|'
               || 'VATLocation'
               || '|'
               || 'InteractionUserID'
               || '|'
               || 'InteractionAccountName'
               || '|'
               || 'IsAdmin'
               || '|'
               || 'IsCopyingItems'
               || '|'
               || 'IsFirmReporting'
               || '|'
               || 'IsSuperDelegate'
               || '|'
               || 'UseOnline'
               || '|'
               || 'LanguageID'
               || '|'
               || 'Password'
               || '|'
               || 'AnalyticsUserType'
               || '|'
               || 'AnalyticsView'
               || '|'
               || 'AnalyticsModules'
               || '|'
               || 'Command'
               || '|'
               || 'CommandOnPersonChange'
               || '|'
               || 'AlternateCurrencyCodes'
               || '|'
               || 'AlternateEmailAddress1'
               || '|'
               || 'AlternateEmailAddress2'
               || '|'
               || 'AlternateEmailAddress3'
               || '|'
               || 'DefaultApplication'    AS dataline
          FROM DUAL
        UNION
        -----------------------------------------------------------
        --Population # 1
        --Active employees with an active job
        -----------------------------------------------------------
        SELECT 2,
                  first_name
               || '|'
               || last_name
               || ' - '
               || uni
               || '|'
               || uni
               || '|'
               || uni
               || '|'
               -----------------------------------------------------------------
               --This case statement determines supervisor sent to CR--the
               --"Reports To" field
               -----------------------------------------------------------------
               || CASE
                      ----------------------------------------------------------
                      --Active primary job is single position
                      --and POSN ECLS not in exclusion list
                      --Get POSN supervisor before JQUE WTE approver
                      ----------------------------------------------------------
                      WHEN     nbbposn_type <> 'P'
                           AND nbbposn_ecls_code NOT IN ('11',
                                                         '20',
                                                         '24',
                                                         '25',
                                                         '30',
                                                         '31',
                                                         '35',
                                                         '39',
                                                         '81')
                      THEN
                          DECODE (
                              current_primary_job_posn,
                              '999999', '',
                              (SELECT c.spriden_id
                                 FROM spriden c
                                WHERE     c.spriden_pidm =
                                          ( ------------------------------------
                                           --Assign Katie Embree (PIDM: 200531
                                               --UNI: CME11) as supervisor for
                                                         --following employees
                                          ------------------------------------
                                           CASE
                                               WHEN pidm IN (392501, -- TC3130 Castaneda
                                                             305024, -- HP2125 Perkowski Henry
                                                             355606, -- MF192 Feierman Michael
                                                             127670, -- JSR167 Robinson Janice
                                                             392332 -- CPL2143 Cynthia Langin
                                                                   )
                                               THEN
                                                   200531
                                               ELSE
                                                   NVL (
                                                       tcapp.hr_report_util.f_hrfindposnsupervisor (
                                                           current_primary_job_posn,
                                                           TRUNC (SYSDATE)),
                                                       tcapp.hr_report_util.f_hrfindjqueapprover (
                                                           pidm,
                                                           current_primary_job_posn,
                                                           current_primary_job_suff,
                                                           'WTE'))
                                           END)
                                      AND c.spriden_ntyp_code = 'UNI'
                                      AND c.spriden_entity_ind = 'P'))
                      ----------------------------------------------------------
                      --Active primary job is a pooled position
                      --and POSN ECLS not in exclusion list then
                      --get the JQUE WTE approver before POSN supervisor
                      ----------------------------------------------------------
                      WHEN     nbbposn_type = 'P'
                           AND nbbposn_ecls_code NOT IN ('11',
                                                         '20',
                                                         '24',
                                                         '25',
                                                         '30',
                                                         '31',
                                                         '35',
                                                         '39',
                                                         '81')
                      THEN
                          DECODE (
                              current_primary_job_posn,
                              '999999', '',
                              (SELECT c.spriden_id
                                 FROM spriden c
                                WHERE     c.spriden_pidm =
                                          (NVL (
                                               tcapp.hr_report_util.f_hrfindjqueapprover (
                                                   pidm,
                                                   current_primary_job_posn,
                                                   current_primary_job_suff,
                                                   'WTE'),
                                               tcapp.hr_report_util.f_hrfindposnsupervisor (
                                                   current_primary_job_posn,
                                                   TRUNC (SYSDATE))))
                                      AND c.spriden_ntyp_code = 'UNI'
                                      AND c.spriden_entity_ind = 'P'))
                      ----------------------------------------------------------
                      --POSN ECLS is in inclusion list
                      --Get supervisor from override function
                      ----------------------------------------------------------
                      WHEN nbbposn_ecls_code IN ('11',
                                                 '20',
                                                 '24',
                                                 '25',
                                                 '30',
                                                 '31',
                                                 '35',
                                                 '39',
                                                 '81')
                      THEN
                          fzkeexp.f_daa_lkup (nbrjobs_orgn_code_ts, pidm)
                  END
               -----------------------------------------------------------------
               --End of case statement to get "Reports To" supervisor
               || '|'
               || LOWER (uni)
               || '@tc.columbia.edu'
               --|| tc_gmail_address
               || '|'
               || NULL
               || '|'
               -----------------------------------------------------------------
               --Job title field
               --When position e-class is one of following, get the job title
               --from job description.  Else get from SDE field on the job
               --(NBRJOBS)
               -----------------------------------------------------------------
               || CASE
                      WHEN nbbposn_ecls_code IN ('11',
                                                 '20',
                                                 '24',
                                                 '25',
                                                 '30',
                                                 '31',
                                                 '35',
                                                 '39',
                                                 '81')
                      THEN
                          nbrjobs_desc
                      ELSE
                          current_primary_job_title_1
                  END
               -----------------------------------------------------------------
               --End of case statement to get job title
               || '|'
               || --4/2/2019 by JA: adding 1 of 3 entity codes for the filed "Entity1"
                  --to allow for identification for those employees with multiple jobs or those
                  --supervisors with multiple subordinates
                  --EE = employee with multiple jobs
                  --SP = supervsor with multiple subordinates
                  --ES = employee with multiple jobs and also a supervisor of multiple suborindates
                baninst1.fzkeexp.f_chrome_multi_sup_code (pidm, 'CODE')
               || '|'
               || NULL
               || '|'
               || NULL
               || '|'
               || NULL
               || '|'
               || NULL
               || '|'
               || NULL
               || '|'
               || NULL
               || '|'
               || --4/5/19 by JA: changing reports to field to pipe-delimited value that
                  -- includes all supervisors for employee
                baninst1.fzkeexp.f_chrome_multi_sup_code (pidm, 'LIST')
               || '|'
               || NULL
               || '|'
               || NULL
               || '|'
               || NULL
               || '|'
               || NULL
               || '|'
               || NULL
               || '|'
               || NULL
               || '|'
               || NULL
               || '|'
               || NULL
               || '|'
               || NULL
               || '|'
               || NULL
               || '|'
               || NULL
               || '|'
               || NULL
               || '|'
               || NULL
               || '|'
               || NULL
               || '|'
               || NULL
               || '|'
               || NULL
               || '|'
               || NULL
               || '|'
               || NULL
               || '|'
               || NULL
               || '|'
               || NULL
               || '|'
               || NULL
               || '|'
               || NULL
               || '|'
               || NULL
               || '|'
               || NULL
               || '|'
               || NULL
          FROM gen_demographic, nbbposn, nbrjobs
         WHERE     current_primary_job_posn = nbbposn_posn
               AND nbbposn_status = 'A'
               AND TRUNC (SYSDATE) BETWEEN TRUNC (nbbposn_begin_date)
                                       AND TRUNC (
                                               NVL (nbbposn_end_date,
                                                    SYSDATE))
               AND nbrjobs_status <> 'T'
               --7/31/23 by JA: per INC0198556, removing limit of 00 suffix
               --AND nbrjobs_suff = '00'
               --Exclude retired faculty
               AND employee_class_code <> '30'
               AND nbrjobs_pidm = pidm
               AND nbrjobs_posn = current_primary_job_posn
               AND nbrjobs_suff = current_primary_job_suff
               AND nbrjobs_effective_date =
                   (SELECT MAX (n.nbrjobs_effective_date)
                      FROM nbrjobs n
                     WHERE     n.nbrjobs_suff = current_primary_job_suff
                           AND n.nbrjobs_posn = current_primary_job_posn
                           AND n.nbrjobs_pidm = pidm
                           --AND n.nbrjobs_suff = '00'
                           AND TRUNC (n.nbrjobs_effective_date) <=
                               TRUNC (SYSDATE))
               AND current_employee = 'Y'
               --7/26/23 by JA: per HR0020960 also need to include people
               --who are on sabbatical--the job is coded as on leave.  Because
               --leave or active are the only statuses other than null, changing
               --code to include all jobs where status is not null 
               --AND current_primary_job_status = 'Active'
               AND current_primary_job_status IS NOT NULL
               --AND uni_activated = 'Y'
               AND tc_gmail_address IS NOT NULL
        UNION
        ------------------------------------------------------------------------
        --Population # 2
        --Active employees without an active job (this would include SPA)
        ------------------------------------------------------------------------
        SELECT 2,
                  first_name
               || '|'
               || last_name
               || ' - '
               || uni
               || '|'
               || uni
               || '|'
               || uni
               || '|'
               || DECODE (
                      posn,
                      '999999', '',
                      (SELECT c.spriden_id
                         FROM spriden c
                        WHERE     c.spriden_pidm =
                                  (NVL (
                                       tcapp.hr_report_util.f_hrfindjqueapprover (
                                           pidm,
                                           posn,
                                           suff,
                                           'WTE'),
                                       tcapp.hr_report_util.f_hrfindposnsupervisor (
                                           posn,
                                           TRUNC (SYSDATE))))
                              AND c.spriden_ntyp_code = 'UNI'
                              AND c.spriden_entity_ind = 'P'))
               || '|'
               || LOWER (uni)
               || '@tc.columbia.edu'
               --|| tc_gmail_address
               || '|'
               || NULL
               || '|'
               -- || job_title_1_1
               || (SELECT NVL(job_title_1_1, nbrjobs_desc)
                     FROM nbrjobs_add title
                    WHERE     title.nbrjobs_pidm = pidm
                          AND title.nbrjobs_posn = posn
                          AND title.nbrjobs_suff = suff
                          AND title.nbrjobs_status = status
                          AND title.nbrjobs_effective_date = eff_date)
               || '|'
               || NULL
               --4/2/2019 by JA: adding 1 of 3 entity codes for the filed "Entity1"
               --to allow for identification for those employees with multiple jobs or those
               --supervisors with multiple subordinates
               --EE = employee with multiple jobs
               --SP = supervsor with multiple subordinates
               --ES = employee with multiple jobs and also a supervisor of multiple suborindates
               --baninst1.fzkeexp.f_chrome_multi_sup_code (a.spriden_pidm
               --                                         ,'CODE')
               || '|'
               || NULL
               || '|'
               || NULL
               || '|'
               || NULL
               || '|'
               || NULL
               || '|'
               || NULL
               || '|'
               || NULL
               || '|'
               || NULL
               --4/5/19 by JA: changing reports to field to pipe-delimited value that
               -- includes all supervisors for employee
               --baninst1.fzkeexp.f_chrome_multi_sup_code (a.spriden_pidm
               --                                       ,'LIST')
               || '|'
               || NULL
               || '|'
               || NULL
               || '|'
               || NULL
               || '|'
               || NULL
               || '|'
               || NULL
               || '|'
               || NULL
               || '|'
               || NULL
               || '|'
               || NULL
               || '|'
               || NULL
               || '|'
               || NULL
               || '|'
               || NULL
               || '|'
               || NULL
               || '|'
               || NULL
               || '|'
               || NULL
               || '|'
               || NULL
               || '|'
               || NULL
               || '|'
               || NULL
               || '|'
               || NULL
               || '|'
               || NULL
               || '|'
               || NULL
               || '|'
               || NULL
               || '|'
               || NULL
               || '|'
               || NULL
               || '|'
               || NULL
               || '|'
               || NULL
          FROM (SELECT uni,
                       first_name,
                       last_name,
                       pidm,
                       nbrjobs_posn                       posn,
                       nbrjobs_suff                       suff,
                       nbrjobs_effective_date             eff_date,
                       nbrjobs_status                     status,
                       RANK ()
                           OVER (
                               PARTITION BY nbrjobs.nbrjobs_pidm
                               ORDER BY
                                   nbrjobs.nbrjobs_activity_date DESC,
                                   nbrjobs.ROWID DESC)    job_rank
                  FROM gen_demographic,
                       nbbposn,
                       nbrjobs,
                       nbrbjob
                 WHERE     pidm = nbrjobs_pidm
                       AND nbrjobs_posn = nbbposn_posn
                       AND nbrjobs_pidm = nbrbjob_pidm
                       AND nbrjobs_posn = nbrbjob_posn
                       AND nbrjobs_suff = nbrbjob_suff
                       AND nbrjobs_status = 'A'
                       AND nbrjobs_suff = '00'
                       --Exclude retired faculty
                       AND employee_class_code <> '30'
                       AND nbrjobs_effective_date =
                           (SELECT MAX (n.nbrjobs_effective_date)
                              FROM nbrjobs n
                             WHERE     n.nbrjobs_pidm = pidm
                                   AND n.nbrjobs_status <> 'T'
                                   AND n.nbrjobs_suff = '00'
                                   AND TRUNC (n.nbrjobs_effective_date) <=
                                       TRUNC (SYSDATE))
                       AND TRUNC (nbrjobs_effective_date) BETWEEN TRUNC (
                                                                      nbbposn_begin_date)
                                                              AND TRUNC (
                                                                      NVL (
                                                                          nbbposn_end_date,
                                                                          SYSDATE))
                       --9/8/23 by JA: per INC0203740 removing P filter
                       --AND nbrbjob_contract_type = 'P'
                       AND current_employee = 'Y'
                       AND tc_gmail_address IS NOT NULL
                       AND currently_active_primary_job <> 'Y'
                       --AND uni_activated = 'Y'
                       )
         WHERE job_rank = 1
        UNION
        ----------------------------------------------------------------------
        --Population # 3
        --Active students who aren't employees (excludes graduates whose
        --grad date is older than 60 days; also excludes non-degree students)
        --Students who are employees are only identified in CR as employees
        --and not students
        ----------------------------------------------------------------------
        SELECT 2,
                  first_name
               || '|'
               || last_name
               || ' - '
               || uni
               || '|'
               || uni
               || '|'
               || uni
               || '|'
               || NULL
               || '|'
               || LOWER (uni)
               || '@tc.columbia.edu'
               || '|'
               || NULL
               || '|'
               || 'STUDENT'
               || '|'
               || NULL
               || '|'
               || NULL
               || '|'
               || NULL
               || '|'
               || NULL
               || '|'
               || NULL
               || '|'
               || NULL
               || '|'
               || NULL
               || '|'
               || NULL
               || '|'
               || NULL
               || '|'
               || NULL
               || '|'
               || NULL
               || '|'
               || NULL
               || '|'
               || NULL
               || '|'
               || NULL
               || '|'
               || NULL
               || '|'
               || NULL
               || '|'
               || NULL
               || '|'
               || NULL
               || '|'
               || NULL
               || '|'
               || NULL
               || '|'
               || NULL
               || '|'
               || NULL
               || '|'
               || NULL
               || '|'
               || NULL
               || '|'
               || NULL
               || '|'
               || NULL
               || '|'
               || NULL
               || '|'
               || NULL
               || '|'
               || NULL
               || '|'
               || NULL
               || '|'
               || NULL
               || '|'
               || NULL
               || '|'
               || NULL
          FROM gen_demographic
         WHERE     student_level_code_1 = 'GR'
               AND current_employee = 'N'
               --AND uni_activated = 'Y'
               AND tc_gmail_address IS NOT NULL
               AND (   EXISTS
                           (SELECT 'X'
                              FROM sfrstcr
                             WHERE     sfrstcr_pidm = pidm
                                   AND sfrstcr_rsts_code LIKE 'R%'
                                   AND sfrstcr_term_code >=
                                       (SELECT last_term FROM tcapp.selterm))
                    OR EXISTS
                           (SELECT 'RECENT_GRAD'
                              FROM sgbstdn c, shrdgmr
                             WHERE     c.sgbstdn_pidm = pidm
                                   AND c.sgbstdn_pidm = shrdgmr_pidm
                                   AND c.sgbstdn_degc_code_1 = 'AWD'
                                   AND shrdgmr_degs_code = 'DA'
                                   AND shrdgmr_grad_date > (SYSDATE - 60)
                                   AND c.sgbstdn_term_code_eff =
                                       (SELECT MAX (sgbstdn_term_code_eff)
                                          FROM sgbstdn
                                         WHERE sgbstdn_pidm = c.sgbstdn_pidm)
                                   AND c.sgbstdn_stst_code = 'AS'))
        ORDER BY 1) the_data;

SPOOL OFF;
EXIT;
