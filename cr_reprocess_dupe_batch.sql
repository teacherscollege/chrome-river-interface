declare
v_batch_id number := 1460;
v_external_batch_id varchar2(100) := '2018-05-08-230404';
l_msg varchar2(3000) := null;

begin
    FZKEEXP.P_REPROCESS_DUPEBATCH(v_batch_id, v_external_batch_id, l_msg);
    
    if l_msg is not null then
    dbms_output.put_line(l_msg);
    else
      dbms_output.put_line('Completed Successfully');
    end if;
exception
when others then dbms_output.put_line(sqlerrm);
end;