
  CREATE OR REPLACE VIEW "TCAPP"."FIN_CHROME_RIVER_ROLE_VW" ("CR_INDEX", "CR_3K_APPR_UNI", "CR_3K_APPR_NAME", "CR_25K_APPR_UNI", "CR_25K_APPR_NAME", "CR_100K_APPR_UNI", "CR_100K_APPR_NAME") AS 
  select distinct fzrappr_index, cr_3k_appr_uni, cr_3k_appr_name, cr_25k_appr_uni, cr_25k_appr_name, cr_100k_appr_uni, cr_100k_appr_name
from fzrappr, tcapp.cr_3k_appr_vw, tcapp.cr_25k_appr_vw, tcapp.cr_100k_appr_vw
where fzrappr_index = cr_3k_appr_index (+)
and fzrappr_index = cr_25k_appr_index (+)
and fzrappr_index = cr_100k_appr_index (+)
and fzrappr_system = 'CHROMERVR';