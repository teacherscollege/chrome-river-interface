CREATE OR REPLACE PACKAGE baninst1.fzkeexp
AS
    FUNCTION f_fspd_open (p_userid VARCHAR2, p_date DATE)
        RETURN BOOLEAN;

    FUNCTION f_daa_lkup (orgn_in IN VARCHAR2, pidm_in NUMBER)
        RETURN VARCHAR2;

    --function f_updt_crnum(p_batch_id in varchar2, p_crnum_in in varchar2, p_crnew_in in varchar2) return varchar2;
    FUNCTION f_updt_status (p_batch_id IN VARCHAR2
                           ,p_crnum_in IN VARCHAR2
                           ,p_status_in IN VARCHAR2)
        RETURN VARCHAR2;

    PROCEDURE p_check_nsf_invoice (p_invoice_code IN VARCHAR2);

    PROCEDURE p_get_inv_header (p_msg IN OUT VARCHAR2, appr_amt IN VARCHAR2);

    PROCEDURE p_create_items (p_msg IN OUT VARCHAR2);

    PROCEDURE p_processinvoice (p_batch_id IN VARCHAR2);

    PROCEDURE p_send_nsf_invoice_email (p_invoice_code IN VARCHAR2
                                       ,p_msg IN OUT VARCHAR2
                                       ,p_send_to_grants IN VARCHAR2
                                       ,p_send_to_budget IN VARCHAR2);

    PROCEDURE p_processbatch;

    PROCEDURE p_reset_error (p_msg IN OUT VARCHAR2, p_err_type IN VARCHAR2);

    PROCEDURE p_renumber_batch (p_batch_id IN VARCHAR2
                               ,p_msg IN OUT VARCHAR2);

    PROCEDURE get_paid_expenses;

    PROCEDURE p_insrt_fzrexpd (p_invh_code IN VARCHAR2
                              ,p_check_num IN VARCHAR2
                              ,p_check_amt IN VARCHAR2
                              ,p_bank_id IN VARCHAR2
                              ,p_inv_num IN VARCHAR2
                              ,p_check_date IN DATE
                              ,p_external_doc_id IN VARCHAR2
                              ,p_feed_lineno IN NUMBER
                              ,p_msg IN OUT VARCHAR2);

    PROCEDURE p_reprocess_dupebatch (p_batch_id IN VARCHAR2
                                    ,p_external_batch_id IN VARCHAR2
                                    ,p_msg IN OUT VARCHAR2);

    --5/8/19 by JA: added to allow for multiple supverisors
    FUNCTION f_chrome_multi_sup_code (p_pidm NUMBER, p_code VARCHAR2)
        RETURN VARCHAR2;
END fzkeexp;
/
