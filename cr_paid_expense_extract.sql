set define on;
set echo off;
set head off;
set feedback off;
set trimspool on;
set pagesize 0;
set linesize 500;

spool PaidExpenses.lis;

select the_data.dataline from
 (select 1, 'LineNumber'||'|'||   
'VoucherInvoice'||'|'||
'Amount'||'|'||
'AmountDecimals'||'|'||
'CurrencyCode1'||'|'||
'PaidDate'||'|'||
'PaidDateMask2'||'|'||
'CheckNumber'||'|'||
'BankID'||'|'||
'LastCreateDate'||'|'||
'IsAuthorized'||'|'||
'PaymentType' as dataline
from dual
union
select 2, fzrexpd_feed_lineno||'|'||   
fzrexpd_external_doc_id||'|'||
fzrexpd_check_amt*100||'|'||
'2'||'|'||
'USD'||'|'||
to_char(fzrexpd_check_date,'MM/dd/yyyy')||'|'||
'MM/dd/yyyy' ||'|'||
fzrexpd_check_num||'|'||
fzrexpd_bank_id||'|'||
null||'|'||
'0'||'|'||
'FIRM'
from fzrexpd
where fzrexpd_feed_date is null
order by 1) the_data;

spool off;
/* update the feed date and feed doc id on the interface tables */
update fzrexpd
set fzrexpd_feed_date = trunc(sysdate), fzrexpd_feed_doc_id = '&1'
where fzrexpd_feed_date is null;
update fzreexp
set fzreexp_feed_date = trunc(sysdate), fzreexp_feed_doc_id = '&1'
where fzreexp_status = 'C'
and fzreexp_external_doc_id in (select fzrexpd_external_doc_id
                                 from fzrexpd
                                where fzrexpd_feed_doc_id = '&1');
commit;
exit;

