
  CREATE OR REPLACE FORCE EDITIONABLE VIEW "TCAPP"."CR_25K_APPR_VW" ("CR_25K_APPR_PIDM", "CR_25K_APPR_UNI", "CR_25K_APPR_NAME", "CR_25K_APPR_INDEX", "CR_25K_APPR_ROLE") AS 
  select spriden_pidm,
       spriden_id,
       spriden_last_name||', '||spriden_first_name,
       a.fzrappr_index,
       a.fzrappr_sys_role
  from fzrappr a, spriden
  where a.fzrappr_pidm = spriden_pidm
  and spriden_ntyp_code = 'UNI'
  and a.fzrappr_system = 'CHROMERVR'
  and a.fzrappr_sys_role = '25KAPPR'
  and a.fzrappr_exp_date = (select min(fzrappr_exp_date)
                                    from fzrappr b
                                    where b.fzrappr_index = a.fzrappr_index
                                      and b.fzrappr_system = 'CHROMERVR'
                                      and b.fzrappr_sys_role = '25KAPPR'
                                      and trunc(b.fzrappr_exp_date) > trunc(sysdate));