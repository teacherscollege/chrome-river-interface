--6/6/24: ARCHIVING THIS VERSION AND COMMITTING PKB AND PKS AS 2 DIFFERENT FILES

CREATE OR REPLACE PACKAGE BANINST1.fzkeexp AS
  FUNCTION f_fspd_open(p_userid VARCHAR2, p_date DATE) RETURN BOOLEAN;
  FUNCTION f_daa_lkup(orgn_in IN VARCHAR2, pidm_in NUMBER) RETURN VARCHAR2;
  --function f_updt_crnum(p_batch_id in varchar2, p_crnum_in in varchar2, p_crnew_in in varchar2) return varchar2;
  FUNCTION f_updt_status(p_batch_id  IN VARCHAR2, p_crnum_in  IN VARCHAR2,p_status_in IN VARCHAR2) RETURN VARCHAR2;
  PROCEDURE p_check_nsf_invoice(p_invoice_code IN VARCHAR2);
  PROCEDURE p_get_inv_header(p_msg IN OUT VARCHAR2, appr_amt in VARCHAR2);
  PROCEDURE p_create_items(p_msg IN OUT VARCHAR2);
  PROCEDURE p_processinvoice(p_batch_id IN VARCHAR2);
  PROCEDURE p_send_nsf_invoice_email(p_invoice_code   IN VARCHAR2, p_msg IN OUT VARCHAR2, p_send_to_grants IN VARCHAR2,p_send_to_budget IN VARCHAR2);
  PROCEDURE p_processbatch;
  PROCEDURE p_reset_error(p_msg IN OUT VARCHAR2, p_err_type IN VARCHAR2);
  PROCEDURE p_renumber_batch(p_batch_id IN VARCHAR2, p_msg IN OUT VARCHAR2);
  PROCEDURE get_paid_expenses;
  PROCEDURE p_insrt_fzrexpd(p_invh_code       IN VARCHAR2,
                            p_check_num       IN VARCHAR2,
                            p_check_amt       IN VARCHAR2,
                            p_bank_id         IN VARCHAR2,
                            p_inv_num         IN VARCHAR2,
                            p_check_date      IN DATE,
                            p_external_doc_id IN VARCHAR2,
                            p_feed_lineno     IN NUMBER,
                            p_msg             IN OUT VARCHAR2);
  PROCEDURE p_reprocess_dupebatch(p_batch_id IN VARCHAR2, p_external_batch_id IN VARCHAR2, p_msg IN OUT VARCHAR2);
  --5/8/19 by JA: added to allow for multiple supverisors
  FUNCTION f_chrome_multi_sup_code(p_pidm NUMBER, p_code VARCHAR2)
    RETURN VARCHAR2;
END fzkeexp;
/



CREATE OR REPLACE PACKAGE BODY BANINST1.FZKEEXP AS
/*******************************************************************************

   NAME:       FZKEEXP

   PURPOSE:    This package is Chrome River Interface to Banner.

               Procedure p_ProcessInvoice and creates Invoice calling Banner Invoice API
                               fb_invoice_header.p_create_header
                               fb_invoice_item.p_create_item
                               fb_invoice_acctg.p_create_accounting

   
   REVISIONS:
   Ver  Date        Author        Description
   ---  ----------  ------------  ------------------------------------
   1.0  06/06/2017  D. Semar      Initial.
        02/27/2018  D. Semar      Production ready
        03/19/2018  D. Semar      Added calls to p_send_error_email 
        03/20/2018  D. Semar      Enhanced error email messages
        04/13/2018  D. Semar      Check if vendor record exists and create if not found <DGS 041318>
        05/11/2018  D. Semar      Back-date transaction date during the first close of year end <DGS 05112018>
        05/29/2018  D. Semar      Change accounting line creation to sum by foapl <DGS 05292018>
        06/07/2018  D. Semar      Added p_Reprocess_DupeBatch to create a new batch and reprocess a non-dupe batch which 
                                  errored out with the duplicate batch error message.  This occurs when two or more external batches
                                  are processed under the same batch id and one of the external batches has previously been 
                                  imported. This utility is run from Sql Developer. <DGS 06042018>
       08/20/2018   D. Semar      Tighten up error message handling for fatal error 'PL/SQL numeric or value error..' <DGS 08202018>
       06/06/2019   J. Asbury     Added new function f_chrome_multi_sup_code to 
                                  allow for multiple supervisors in CR.  Also
                                  modifications to function f_daa_lkup
*******************************************************************************/
--
   g_header_rec        fb_invoice_header.invoice_header_rec;
   g_comm_rec          fb_invoice_item.invoice_item_rec;
   g_acctg_rec         fb_invoice_acctg.invoice_acctg_rec;
--
   g_cr_memo_ind       fabinvh.fabinvh_cr_memo_ind%type;
   g_pmt_due_date      fabinvh.fabinvh_pmt_due_date%type;
--
   g_single_acctg_ind  fpbpohd.fpbpohd_single_acctg_ind%type;
--
   g_inv_user          fabinvh.fabinvh_user_id%type := 'APMASTER';
   g_inv_code          fabinvh.fabinvh_code%type;
   g_vend_inv_code     fabinvh.fabinvh_vend_inv_code%type;
   g_invoice_date      fabinvh.fabinvh_invoice_date%type;
   g_trans_date        fabinvh.fabinvh_trans_date%type;
   g_bank_code         fabinvh.fabinvh_bank_code%type;
--
   g_vend_pidm         fpbpohd.fpbpohd_vend_pidm%type;
   g_vend_id           spriden.spriden_id%type; 
   g_user_id           fobprof.fobprof_user_id%type;
   g_user_name         fobprof.fobprof_user_name%type;
   g_user_coas         fobprof.fobprof_coas_code%type;
   g_inv_type          fzreexp.fzreexp_inv_type%type;
   g_inv_prefix        varchar2(2);
   g_req_orgn          varchar2(2);
  
   g_nsf_appr_byp      char(1) := 'N';
   g_prc_name          varchar2(100);
   g_currency          varchar2(3) := 'USA';
   g_orig              varchar2(10) := 'CHROMERVR';
   g_doc_status varchar2(1) := null;
   g_err_msg varchar2(500) := null;
   g_atyp_seq_no      spraddr.spraddr_seqno%type;
   g_atyp_code        spraddr.spraddr_atyp_code%type;
   g_banner           varchar2(10);
   --VR 9/16/2019 INC0089249 g_r_rucl_code      VARCHAR(4);
   g_r_rucl_code      VARCHAR(4);
   
--==============================================================================
--
--==============================================================================
--
function f_fspd_open(p_userid varchar2, p_date date) return boolean is
   s varchar2(1);
begin
   select ftvfspd_prd_status_ind into s
     from ftvfspd, fobprof
    where ftvfspd_coas_code = fobprof_coas_code
      and fobprof_user_id = upper(p_userid)
      and p_date between trunc(ftvfspd_prd_start_date) and trunc(ftvfspd_prd_end_date);
   if s = 'O' then return true;
   else return false;
   end if;
exception when others then
   return false;
end;
----==============================================================================
function f_updt_status (p_batch_id in varchar2, p_crnum_in in varchar2, p_status_in in varchar2) return varchar2
is 
p_msg varchar2(100) := null;

begin
    update fzreexp
      set  fzreexp_status = p_status_in
     where fzreexp_batch_id = p_batch_id
       and fzreexp_inv_num = p_crnum_in;
      p_msg := 'Updated status for'||p_crnum_in||' to '||p_status_in;
       return p_msg;
exception
  when others then p_msg := substr(sqlerrm,1,80)||' for '||p_crnum_in;
                   return p_msg;
end;
----==============================================================================

  FUNCTION f_daa_lkup(orgn_in IN VARCHAR2, pidm_in NUMBER) RETURN VARCHAR2 IS
    daa_uni VARCHAR2(9);
    orgn_ts ftvorgn.ftvorgn_orgn_code%TYPE;
  
  BEGIN
    /* Override orgn_in for certain faculty */
    SELECT CASE
             WHEN pidm_in = 64472 THEN
             --Sharon Kagan
              '111883'
             WHEN pidm_in = 215031 THEN
             --Douglas Ready
              '117109'
             WHEN pidm_in = 270548 THEN
             --Peter Bergman
              '117121'
           --5/6/19 by JA: removed override for Dr. Han - INC0086206
           --her primary job org of 111429 will be passed in and set in override below 
           --to UNI 'PG2201'
           --   when pidm_in = 242186  then '117172'
             WHEN pidm_in = 82586 THEN
             --Jeanne Brooks-Gunn
              '111883'
             WHEN pidm_in = 942 THEN
             --Thomas Bailey
              '117121'
             WHEN pidm_in = 63028 THEN
             --Lening Liu
              '117174'
           --5/6/19 by JA: removed override org here for Dr. Han - INC0086206   
           --Her primary job org of 111429 is being passed in and set in override below 
           --for org to PG2201 who Dr. Han reports to, which is not correct. 
           --So there is another override at bottom of this function that will set her 
           --supervisor as UNI 'TJ2180' 
           --when pidm_in = 242186  then '117174'  
           --5/6/19 by JA: remove override for Gary Natriello because it was null
           --anyway; the supervisor on his primary job will be used
           --when pidm_in = 1138  then null
             ELSE
              orgn_in
           END
      INTO orgn_ts
      FROM dual;
  
    SELECT CASE
             WHEN orgn_ts IN ('111401',
                              '111411',
                              '111412',
                              '111413',
                              '111414',
                              '111415',
                              '111416',
                              '111417',
                              '111418',
                              '111419',
                              '111420',
                              '111421',
                              '111422',
                              '111423',
                              '111424',
                              '111425',
                              '111426',
                              '111427',
                              '111428',
                              '111429',
                              '111430',
                              '111431',
                              '111432',
                              '111433',
                              '111434',
                              '111435',
                              '111451',
                              '111452',
                              '111453',
                              '111454',
                              '111462',
                              '111463',
                              '111464',
                              '111466',
                              '111467',
                              '111468',
                              '111469',
                              '111470') THEN
              'PG2201'
             WHEN orgn_ts IN ('111701',
                              '111711',
                              '111712',
                              '111713',
                              '111714',
                              '111715',
                              '111716',
                              '111717',
                              '111718',
                              '111719',
                              '111720',
                              '111721',
                              '111722',
                              '111723',
                              '111724',
                              '111725',
                              '111726',
                              '111727') THEN
              'MEL31'
             WHEN orgn_ts IN ('111201',
                              '111211',
                              '111212',
                              '111213',
                              '111214',
                              '111215',
                              '111216',
                              '111217',
                              '111261',
                              '111262') THEN
              'EDD2001'
             WHEN orgn_ts IN ('111301',
                              '111311',
                              '111312',
                              '111313',
                              '111314',
                              '111315',
                              '111316',
                              '111317',
                              '111318',
                              '111319',
                              '111320',
                              '111351',
                              '111352',
                              '111353',
                              '111354',
                              '111355',
                              '111361') THEN
              'AMA2119'
             WHEN orgn_ts IN ('111001',
                              '111011',
                              '111012',
                              '111013',
                              '111014',
                              '111015',
                              '111016',
                              '111017') THEN
              'TSA2002'
             WHEN orgn_ts IN ('111501',
                              '111511',
                              '111512',
                              '111513',
                              '111514',
                              '111515',
                              '111516',
                              '111517',
                              '111518',
                              '111551',
                              '111561') THEN
              'AS5312'
             WHEN orgn_ts IN ('111801',
                              '111811',
                              '111812',
                              '111813',
                              '111814',
                              '111815',
                              '111816') THEN
              'CJG2164'
             WHEN orgn_ts IN ('111101', '111111', '111112', '111113') THEN
              'DM136'
             WHEN orgn_ts IN ('111601',
                              '111611',
                              '111612',
                              '111613',
                              '111614',
                              '111615',
                              '111616',
                              '111617',
                              '111618',
                              '111619',
                              '111620',
                              '111621',
                              '111651') THEN
              'KG2366'
             WHEN orgn_ts IN ('111901',
                              '111911',
                              '111912',
                              '111913',
                              '111914',
                              '111915',
                              '111916',
                              '111917',
                              '111918',
                              '111919',
                              '111920',
                              '111921',
                              '111922',
                              '111923',
                              '111924',
                              '111925',
                              '111926',
                              '111927',
                              '111928',
                              '111929',
                              '111930',
                              '111931',
                              '111932',
                              '111933',
                              '111934',
                              '111935',
                              '111936',
                              '111937',
                              '111938',
                              '111939',
                              '111940',
                              '111941',
                              '111942',
                              '111943',
                              '111944',
                              '111945',
                              '111946',
                              '111947',
                              '111948') THEN
              'DW2237'
             ELSE
              NULL
           END
      INTO daa_uni
      FROM dual;
  
    -- override the overide
    IF pidm_in = 976 THEN
    --Lucy Calkins
      daa_uni := 'BLN6';
    --Beth Neville  
    END IF;
    IF pidm_in = 942 THEN
    --Thomas Bailey
      daa_uni := 'KGM11';
    --Katharine Conway  
    END IF; --from 'LTR2'
    IF pidm_in = 64472 THEN
    --Sharon Kagan
      daa_uni := 'AMP155';
    --Aaron Pallas  
    END IF;
    IF pidm_in = 270548 THEN
    --Peter Bergman
      daa_uni := 'LTR2';
    --Lisa Rothman  
    END IF;
    --5/7/19 by JA: updated Dr. Han override per INC0086206; she is a chair and
    --the org on her primary job would mean that she was being assigned as supervisor
    --to herself and the org in override above is not correct because PG2201 reports
    --to Dr. Han 
    --8/15/19 by JA: override for Dr. Han changed to Stephanie Rowley per INC0092368 
    IF pidm_in = 242186 THEN
      daa_uni := 'SJR2192';
      --Stephanie Rowley 
      --daa_uni := 'TJ2180';
      --Thomas James
    END IF;
    --if pidm_in = 242186 then daa_uni := 'DS3010'; end if;
    IF pidm_in = 82586 THEN
    --Jeanne Brooks-Gunn
      daa_uni := 'JEC34';
      --James Corter
    END IF;
    IF pidm_in = 63028 THEN
    --Lening Liu
      daa_uni := 'DS3010';
      --Debi Slatkin
    END IF;
    
     --8/7/19 by JA: Added entry for Douglas Ready per Jennie Wilson  
    IF pidm_in = 215031 THEN
      --Douglas Ready
      daa_uni := 'AMP155';
      --Aaron Pallas    
    END IF;
  
    RETURN daa_uni;
  EXCEPTION
    WHEN OTHERS THEN
      RETURN NULL;
  END;
----==============================================================================

procedure p_get_email_route (p_msg_in  in varchar2,
                               p_route_to out varchar2) is

begin
      p_route_to := case when p_msg_in like '%Fund code%' then 
                                  case when substr(p_msg_in ,instr(p_msg_in,'Fund code')+10,1) = '5' then 'CR_GRANTS_ERROR'
                                  else 'CR_FINANCE_ERROR'
                                  end
                         when p_msg_in like '%Organization code%not valid%' then
                                  case when substr(p_msg_in ,instr(p_msg_in,'Fund code')+18,1) = '5' then 'CR_GRANTS_ERROR'
                                  else 'CR_FINANCE_ERROR'
                                  end
                         when p_msg_in like '%Index%invalid%' then 
                                  case when substr(p_msg_in ,instr(p_msg_in,'Index')+6,1) = '5' then 'CR_GRANTS_ERROR'
                                  else 'CR_FINANCE_ERROR'
                                  end
                        when p_msg_in like '%Invalid value for vendor%' then 'CR_PO_OFFICE'
                        when p_msg_in like '%Vendor address%' then 'CR_PO_OFFICE'
                        else
                        'CR ERROR EMAIL'
                        end;
exception 
when others then p_route_to := '';
end;
                               
procedure p_send_error_email(p_msg varchar2) is
   l_from varchar2(100);
   l_subj varchar2(500);
   l_to varchar2(32000);
   l_cc varchar2(32000);
   v_mail_successful VARCHAR2(300) := 'T' ; 
   v_mail_errmsg     VARCHAR2(300) := 'F' ; 
   v_error_type varchar2(100);
   v_error_code varchar2(100);
   v_error_message varchar2(3000);
   v_to_list varchar2(100);
   p_route_to varchar2(100); 
begin
   begin
     select sys_context('USERENV','DB_NAME')
       into g_banner
       from dual;
  exception
     when others then null;
  end;
  
   l_subj := 'CHROME RIVER: '||g_banner||' Error in request (FZBEPRC_ID) ';
--
/* determine where to route the email to */

    p_get_email_route(p_msg,p_route_to);
    v_to_list := nvl(p_route_to,'CR ERROR EMAIL');
--    tcapp.util_msg.LogEntry('Send Mail To: '||v_to_list);

   for e in(select ftvsdat_title email, ftvsdat_code_level code_level  from ftvsdat
             where ftvsdat_sdat_code_entity = 'FZKEEXP'
               and upper(ftvsdat_sdat_code_attr) =  v_to_list    
               and upper(ftvsdat_sdat_code_opt_1) = 'FROM'
               and ftvsdat_status_ind = 'A'
               and ftvsdat_eff_date <= sysdate
               and nvl(ftvsdat_term_date, sysdate+1) > sysdate
               and nvl(ftvsdat_nchg_date, sysdate+1) > sysdate
             order by ftvsdat_code_level) 
      loop
         l_from := e.email;
         for t in(select ftvsdat_title email from ftvsdat
                where ftvsdat_sdat_code_entity = 'FZKEEXP'
                  and upper(ftvsdat_sdat_code_attr) =  v_to_list 
                  and upper(ftvsdat_sdat_code_opt_1) = 'TO'
                  and ftvsdat_status_ind = 'A'
                  and ftvsdat_eff_date <= sysdate
                  and nvl(ftvsdat_term_date, sysdate+1) > sysdate
                  and nvl(ftvsdat_nchg_date, sysdate+1) > sysdate
                  and ftvsdat_code_level = e.code_level)
             loop
             	  tcapp.util_msg.LogEntry('Send Mail To: '||t.email||' '||l_subj);

                If l_from is not null and t.email is not null then          	
                   sokemal.p_sendemail (email_addr      => t.email,                   
                                        email_to_name   => NULL,
                                        email_from_addr => l_from,
                                        email_from_name => 'Chrome River '||g_banner||' - NO REPLY',
                                        email_host      => 'SUNMAIL.TC.COLUMBIA.EDU',
                                        email_subject   => l_subj,
                                        email_message   => p_msg,
                                        email_success   => v_mail_successful,
                                        email_reply     => v_mail_errmsg,
                                        email_error_type => v_error_type,
                                        email_error_code => v_error_code,
                                        email_error_message => v_error_message); 
                else
                   dbms_output.put_line('ERROR in p_send_error_email: Can not send email!!!'); 
                   tcapp.util_msg.LogEntry('ERROR in p_send_error_email: Can not send email!!!');                               	                                     
                end if;
             end loop;   
      end loop;   

exception when others then
   dbms_output.put_line('ERROR in p_send_error_email: '||SQLERRM);
end p_send_error_email;
--
----==============================================================================
procedure p_send_nsf_invoice_email(p_invoice_code in varchar2, p_msg in out varchar2, p_send_to_grants in varchar2, p_send_to_budget in varchar2) is
   l_from varchar2(100);
   l_subj varchar2(500);
   l_to varchar2(32000);
   l_cc varchar2(32000);
   v_mail_successful VARCHAR2(300) := 'T' ; 
   v_mail_errmsg     VARCHAR2(300) := 'F' ;     
   v_error_type varchar2(100);
   v_error_code varchar2(100);
   v_error_message varchar2(3000);

begin
--
begin
     select sys_context('USERENV','DB_NAME')
       into g_banner
       from dual;
  exception
     when others then null;
  end;
   l_subj := g_banner||' ChromeRiver: Invoice '||p_invoice_code||' is NSF.';

--
  if p_send_to_budget is not null then
   for e in(select upper(ftvsdat_sdat_code_opt_1) opt, ftvsdat_title email from ftvsdat
             where ftvsdat_sdat_code_entity = 'FZKEEXP'
               and upper(ftvsdat_sdat_code_attr) = p_send_to_budget
               and upper(ftvsdat_sdat_code_opt_1) in('FROM','TO','CC')
               and ftvsdat_status_ind = 'A'
               and ftvsdat_eff_date <= sysdate
               and nvl(ftvsdat_term_date, sysdate+1) > sysdate
               and nvl(ftvsdat_nchg_date, sysdate+1) > sysdate
             order by ftvsdat_code_level) loop
      if  e.opt='FROM' then l_from := e.email;
      elsif e.opt='TO' then l_to := l_to||trim(e.email)||', ';
      elsif e.opt='CC' then l_cc := l_cc||trim(e.email)||', '; end if;
   end loop;
   l_to := rtrim(l_to,', '); l_cc := rtrim(l_cc,', ');
     if l_from is not null and l_to is not null then
       sokemal.p_sendemail (email_addr      => l_to,                   
                        email_to_name   => NULL,
                        email_from_addr => l_from,
                        email_from_name => 'Chrome River - NO REPLY',
                        email_host      => 'SUNMAIL.TC.COLUMBIA.EDU',
                        email_subject   => l_subj,
                        email_message   => p_msg,
                        email_success   => v_mail_successful,
                        email_reply     => v_mail_errmsg,
                        email_error_type => v_error_type,
                        email_error_code => v_error_code,
                        email_error_message => v_error_message);                                             
   else
      dbms_output.put_line('ERROR in p_send_nsf_invoice_email: Can not send email!!!');
      tcapp.util_msg.LogEntry('ERROR in p_send_nsf_invoice_email: Can not send email!!!');
   end if;
  end if;
  
  if p_send_to_grants is not null then
  for f in(select upper(ftvsdat_sdat_code_opt_1) opt, ftvsdat_title email from ftvsdat
             where ftvsdat_sdat_code_entity = 'FZKEEXP'
               and upper(ftvsdat_sdat_code_attr) = p_send_to_grants
               and upper(ftvsdat_sdat_code_opt_1) in('FROM','TO','CC')
               and ftvsdat_status_ind = 'A'
               and ftvsdat_eff_date <= sysdate
               and nvl(ftvsdat_term_date, sysdate+1) > sysdate
               and nvl(ftvsdat_nchg_date, sysdate+1) > sysdate
             order by ftvsdat_code_level) loop
      if  f.opt='FROM' then l_from := f.email;
      elsif f.opt='TO' then l_to := l_to||trim(f.email)||', ';
      elsif f.opt='CC' then l_cc := l_cc||trim(f.email)||', '; end if;
   end loop;
   l_to := rtrim(l_to,', '); l_cc := rtrim(l_cc,', ');
     if l_from is not null and l_to is not null then
       sokemal.p_sendemail (email_addr      => l_to,               
                        email_to_name   => NULL,
                        email_from_addr => l_from,
                        email_from_name => 'Chrome River - NO REPLY',
                        email_host      => 'SUNMAIL.TC.COLUMBIA.EDU',
                        email_subject   => l_subj,
                        email_message   => p_msg,
                        email_success   => v_mail_successful,
                        email_reply     => v_mail_errmsg,
                        email_error_type => v_error_type,
                        email_error_code => v_error_code,
                        email_error_message => v_error_message);                                             
   else
      dbms_output.put_line('ERROR in p_send_nsf_invoice_email: Can not send email!!!');
      tcapp.util_msg.LogEntry('ERROR in p_send_nsf_invoice_email: Can not send email!!!');
      tcapp.util_msg.LogEntry('Grants email list set: '||p_send_to_grants);
      tcapp.util_msg.LogEntry('Budget email list set: '||p_send_to_budget);
   end if;
  end if; 
--
exception when others then
   dbms_output.put_line('ERROR in p_send_nsf_invoice_email: '||SQLERRM);
   tcapp.util_msg.LogEntry('ERROR in p_send_nsf_invoice_email: '||SQLERRM);
end p_send_nsf_invoice_email;
------------------------------------------------------------------------------------
procedure p_check_nsf_invoice(p_invoice_code IN varchar2) is
   l_msg varchar2(4000) := NULL;
   crlf VARCHAR2( 2 ):= CHR( 13 ) || CHR( 10 );
cursor nsf_cursor(c_invoice_code VARCHAR2) IS                          
   SELECT FARINVA_ACCI_CODE, 
          FARINVA_ACCT_CODE,
          lpad(TRIM(TO_CHAR(FARINVA_APPR_AMT,'9,999,999.00')),14) AS APPR_AMT 
   FROM 
       FARINVA
   WHERE FARINVA_INVH_CODE = p_invoice_code
     AND FARINVA_NSF_SUSP_IND = 'Y'
   ORDER BY 1; 
   nsf_info nsf_cursor%ROWTYPE;   
   send_to_grants varchar2(30) := null;
   send_to_budget varchar2(30) := null;
begin
   open nsf_cursor(p_invoice_code);
   loop
      fetch nsf_cursor into nsf_info;
      EXIT WHEN nsf_cursor%NOTFOUND;
      l_msg := l_msg||nsf_info.FARINVA_ACCI_CODE
      ||'-'
      ||nsf_info.FARINVA_ACCT_CODE
      ||nsf_info.APPR_AMT
      ||crlf; 
      if substr(nsf_info.farinva_acci_code,1,1) = '5' then
        send_to_grants := 'CR_GRANTS_ERROR';
      else
       send_to_budget := 'CR_INVNSF_MAIL';
      end if;
      
   end loop;
   close nsf_cursor;
   IF l_msg IS NOT NULL THEN
      l_msg := 'Invoice '||p_invoice_code||' has been created by Chrome River and is in NSF status.'
      ||crlf||crlf
      ||'Below is a list of the index/account combinations for this invoice:'||crlf
      ||crlf
      || l_msg
      || crlf
      || 'End of list.'||crlf;      
      
      p_send_nsf_invoice_email(p_invoice_code, l_msg, send_to_grants, send_to_budget);   
      
   END IF;
   RETURN;
exception when others then
   dbms_output.put_line('ERROR in p_send_error_email: '||SQLERRM);
end p_check_nsf_invoice;
-------------------------------------------------------------
procedure p_ProcessBatch is 

cursor dupe_inv_chk(batch_id_in varchar2) is
select distinct a.FZREEXP_INV_NUM  
from fzreexp a
where fzreexp_batch_id = batch_id_in
and exists ( select 'Y' from fzreexp b
              where a.fzreexp_batch_id = b.fzreexp_batch_id
                and a.fzreexp_external_batch_id <> b.fzreexp_external_batch_id
                and a.fzreexp_inv_num = b.fzreexp_inv_num
                and b.fzreexp_batch_id = batch_id_in );
                
dupe_inv                 fzreexp.fzreexp_inv_num%type;
dupe_external_batch_id   fzreexp.fzreexp_external_batch_id%type;
l_sum                    number;
l_count                  number;
l_msg                    varchar2(500);
new_batch_id             number;
batch_record_count       number;
batch_total_amt          number;

begin

   l_msg := null;
   
   select fimsmgr.cr_batch_seq.nextval
     into new_batch_id
     from dual;
     
   update fzbeexp
     set fzbeexp_batch_id = new_batch_id
    where fzbeexp_status = 'N'
      and fzbeexp_batch_id is null;
      
   update fzreexp
     set fzreexp_batch_id = new_batch_id
   where fzreexp_status = 'N'
     and fzreexp_batch_id is null;
           
    select sum(fzbeexp_record_count), sum(fzbeexp_batch_total)
      into batch_record_count, batch_total_amt
     from fzbeexp
     where fzbeexp_batch_id = new_batch_id;
     
    select sum(fzreexp_appr_amt), count(*)
      into l_sum, l_count
      from fzreexp
     where  fzreexp_batch_id = new_batch_id;
    
    -- Make sure the batch loaded correctly
    if l_sum <> batch_total_amt then
      l_msg := 'Batch '||new_batch_id||' totals do not match. ACK Total: '||batch_total_amt||' Import total: '||l_sum;
    elsif l_count <> batch_record_count then
      l_msg := 'Batch '||new_batch_id||' counts do not match. ACK Count: '||batch_record_count||' Import count: '||l_count;  
    end if;
    
    if l_msg is null then
    begin
    -- Check for duplicate load
       select 'Duplicate load error for batch:'||new_batch_id
         into l_msg
         from fzbeexp a
        where exists (select 'y' from fzbeexp b
                         where a.fzbeexp_external_batch_id = b.fzbeexp_external_batch_id
                            and a.fzbeexp_batch_id <> b.fzbeexp_batch_id
                            and a.fzbeexp_batch_id = new_batch_id);
                            
       update fzreexp set fzreexp_status = 'X' where fzreexp_batch_id = new_batch_id;
       
    exception
       when no_data_found then l_msg := null;
       when too_many_rows then l_msg := 'Duplicate load error for '||new_batch_id;
             update fzreexp set fzreexp_status = 'X' where fzreexp_batch_id = new_batch_id;
       when others then l_msg := substr(sqlerrm,1,80);
    end;       
    end if;
    
    if l_msg is null then
    begin
     /* check for duplicate invoices within the batch */
     open dupe_inv_chk(new_batch_id);
     fetch dupe_inv_chk into dupe_inv;
     while dupe_inv_chk%FOUND loop
     
          select max(fzreexp_external_batch_id)
             into dupe_external_batch_id
             from fzreexp
             where fzreexp_batch_id = new_batch_id
               and fzreexp_inv_num = dupe_inv;
               
             update fzreexp set fzreexp_status = 'X' , fzreexp_err_msg = 'Duplicate invoice found in batch'
             where fzreexp_batch_id = new_batch_id
               and fzreexp_inv_num = dupe_inv
               and fzreexp_external_batch_id <> dupe_external_batch_id;
      end loop;
      close dupe_inv_chk;
    exception
       when others then l_msg := substr(sqlerrm,1,80);
    end;       
    end if;      
    
    if l_msg is null then
      p_renumber_batch(new_batch_id, l_msg);
      if l_msg is null then
        p_ProcessInvoice(new_batch_id);
        update fzbeexp set fzbeexp_status = 'C' where fzbeexp_batch_id = new_batch_id;
      else 
        p_send_error_email(l_msg);
        update fzbeexp set fzbeexp_status = 'E', fzbeexp_err_msg = l_msg where fzbeexp_batch_id = new_batch_id;
        update fzreexp set fzreexp_status = 'E', fzreexp_err_msg = l_msg where fzreexp_batch_id = new_batch_id and fzreexp_status <> 'X';
      end if;
    else  
      p_send_error_email(l_msg);   
      update fzbeexp set fzbeexp_status = 'E' , fzbeexp_err_msg = l_msg  where fzbeexp_batch_id = new_batch_id;
      update fzreexp set fzreexp_status = 'E', fzreexp_err_msg = l_msg where fzreexp_batch_id = new_batch_id and fzreexp_status <> 'X';
    end if;
    
    
exception
  when others then dbms_output.put_line(sqlerrm); 
end;
----------------------------------------------------------------
procedure p_get_inv_header(p_msg in out varchar2, appr_amt in varchar2) is
   l_msg               varchar2(100);
   -- VR 9/30/2019 update for INC0089249	Added parameter 'appr_amt' (from p_processInvoice)  for l_sum
   -- VR 9/30/2019 update for INC0089249 This is the subtotal approved amount grouped on fzreexp_inv_num
   l_sum               number(17,2):=appr_amt;     
begin
--
   g_prc_name := 'p_get_inv_header';
   
   
   if not f_fspd_open(g_inv_user, g_trans_date) then
        g_trans_date := sysdate;
        g_pmt_due_date := sysdate + 1;
   end if;

--
-- Check if Invoice already exists
       
   begin
      select 'Vendor Invoice '||fabinvh_code||'/'||g_vend_inv_code||' already exists'
          -- This Vendor Invoice already exists for this vendor
        into l_msg from fabinvh
       where fabinvh_vend_pidm = g_vend_pidm
         and fabinvh_vend_inv_code = g_vend_inv_code
         and fabinvh_cancel_date is null;
      p_msg := l_msg;
      return;                                                       -- Return!!!
   exception when no_data_found then l_msg := null;
   end;

-- If summary subtotal is negative then this is a credit memo.
-- VR 9/30/2019 update for INC0089249 set values for negative transactions for INC0089249	
   if l_sum < 0 then
      g_cr_memo_ind := 'Y'; -- Credit Memo
      g_r_rucl_code := 'INNC'; -- Credit Memo without Encumbrance
   else
      g_cr_memo_ind := 'N'; -- Debit transaction 
      g_r_rucl_code := 'INNI';
   end if;
   
-- Select the address

begin
if g_inv_type = 'AMX' then
  select ftvvend_vend_check_atyp_code, ftvvend_vend_check_addr_seqno
    into g_atyp_code, g_atyp_seq_no
    from ftvvend
  where ftvvend_pidm = g_vend_pidm;
else
   select spraddr_atyp_code, spraddr_seqno
   into g_atyp_code, g_atyp_seq_no
   from spraddr
   where spraddr_pidm = g_vend_pidm
   and rowid  = f_get_address_fnc(g_vend_pidm, 'CRADDR','A',sysdate,1,'P','');
 end if;
  exception
   when others then p_msg := 'Addr Err: '||substr(sqlerrm,1,80); return;
  end;
--------------------------------------------------------------------------------
   g_header_rec.r_code                  := g_inv_code;
   g_header_rec.r_pohd_code             := null;
   g_header_rec.r_vend_pidm             := g_vend_pidm;
   g_header_rec.r_open_paid_ind         := null;
   g_header_rec.r_user_id               := g_inv_user;
   g_header_rec.r_vend_inv_code         := g_vend_inv_code;
   g_header_rec.r_invoice_date          := g_invoice_date;
   g_header_rec.r_pmt_due_date          := g_pmt_due_date;
   g_header_rec.r_trans_date            := g_trans_date;
   g_header_rec.r_cr_memo_ind           := g_cr_memo_ind;
   g_header_rec.r_adjt_code             := null;
   g_header_rec.r_adjt_date             := null;
   g_header_rec.r_1099_ind              := null; -- set by api
   g_header_rec.r_1099_id               := null;
   g_header_rec.r_ityp_seq_code         := null;
   g_header_rec.r_text_ind              := null;
   g_header_rec.r_appr_ind              := null; -- not in use
   g_header_rec.r_complete_ind          := null;
   g_header_rec.r_disc_code             := null;
   g_header_rec.r_trat_code             := null;
   g_header_rec.r_addl_chrg_amt         := null;
   g_header_rec.r_hold_ind              := null;
   g_header_rec.r_susp_ind              := null;
   g_header_rec.r_susp_ind_addl         := null;
   g_header_rec.r_cancel_ind            := null;
   g_header_rec.r_cancel_date           := null;
   g_header_rec.r_post_date             := null;
   g_header_rec.r_atyp_code             := null;
   g_header_rec.r_atyp_seq_num          := null;
   g_header_rec.r_grouping_ind          := null;
   g_header_rec.r_bank_code             := g_bank_code;
   g_header_rec.r_ruiv_ind              := null;
   g_header_rec.r_edit_defer_ind        := null;
   g_header_rec.r_override_tax_amt      := null;
   g_header_rec.r_tgrp_code             := null;
   g_header_rec.r_submission_number     := null;
   g_header_rec.r_vend_check_pidm       := null;
   g_header_rec.r_invoice_type_ind      := null;
   g_header_rec.r_curr_code             := g_currency;
   g_header_rec.r_disb_agent_ind        := null;
   g_header_rec.r_atyp_code_vend        := g_atyp_code;
   g_header_rec.r_atyp_seq_num_vend     := g_atyp_seq_no;
   g_header_rec.r_nsf_on_off_ind        := null;
   g_header_rec.r_single_acctg_ind      := 'Y'; -- <DGS 052918> Changed from 'N'
   g_header_rec.r_one_time_vend_name    := null;
   g_header_rec.r_one_time_house_number := null;
   g_header_rec.r_one_time_vend_addr1   := null;
   g_header_rec.r_one_time_vend_addr2   := null;
   g_header_rec.r_one_time_vend_addr3   := null;
   g_header_rec.r_one_time_vend_addr4   := null;
   g_header_rec.r_one_time_vend_city    := null;
   g_header_rec.r_one_time_vend_state   := null;
   g_header_rec.r_one_time_vend_zip     := null;
   g_header_rec.r_one_time_vend_natn    := null;
   g_header_rec.r_delivery_point        := null;
   g_header_rec.r_correction_digit      := null;
   g_header_rec.r_carrier_route         := null;
   g_header_rec.r_ruiv_installment_ind  := null;
   g_header_rec.r_multiple_inv_ind      := null;
   g_header_rec.r_phone_area            := null;
   g_header_rec.r_phone_number          := null;
   g_header_rec.r_phone_ext             := null;
   g_header_rec.r_email_addr            := null;
   g_header_rec.r_ctry_code_fax         := null;
   g_header_rec.r_fax_area              := null;
   g_header_rec.r_fax_number            := null;
   g_header_rec.r_fax_ext               := null;
   g_header_rec.r_cancel_code           := null;
   g_header_rec.r_attention_to          := null;
   g_header_rec.r_vendor_contact        := null;
   g_header_rec.r_invd_re_establish_ind := null;
   g_header_rec.r_ach_override_ind      := null;
   g_header_rec.r_acht_code             := null;
   g_header_rec.r_origin_code           := g_orig;
   g_header_rec.r_match_required        := null;
   g_header_rec.r_create_user           := null;
   g_header_rec.r_create_date           := null;
   g_header_rec.r_complete_user         := null;
   g_header_rec.r_complete_date         := null;
   g_header_rec.r_data_origin           := null;
   g_header_rec.r_create_source         := null;
   g_header_rec.r_cancel_user           := null;
   g_header_rec.r_cancel_activity_date  := null;
   g_header_rec.r_ctry_code_phone       := null;
   g_header_rec.r_vend_hold_ovrd_ind    := null;
   g_header_rec.r_vend_hold_ovrd_user   := null;
   g_header_rec.r_internal_record_id    := null;
--
exception when others then
 p_msg := substr(SQLERRM,1,100)||' in p_get_inv_header '|| g_vend_inv_code;
end p_get_inv_header;
--
--==============================================================================
--
procedure p_create_items(p_msg in out varchar2) as
   l_item_rec  fb_invoice_item.invoice_item_rec;
   l_msg       varchar2(4000);
   l_item      farinvc.farinvc_item%type;
--
   l_tot_item_appr_amt farinvc.farinvc_appr_qty%type;
   l_tot_item_addl_amt number;
   l_tot_item_disc_amt number;
   l_tot_item_tax_amt number;
   l_tot_actg_amt number;
   l_tot_actg_appr_amt number;
   l_tot_actg_addl_amt number;
   l_tot_actg_disc_amt number;
   l_tot_actg_tax_amt number;
   
   l_fund_code ftvacci.ftvacci_fund_code%type;
   l_orgn_code ftvacci.ftvacci_orgn_code%type;
   l_prog_code ftvacci.ftvacci_prog_code%type;
   
--
/* Formatted on 9/17/2019 3:27:50 PM (QP5 v5.336) */
-- VR 9/30/19 INC0089249 added case stment to inv_comm_cursor
-- VR 9/30/19 INC0089249 when its a credit memo (negative amt when gouped by fzreexp_inv_num) we dont want the amt to be a negative here, unless it was a positive on statement
--CURSOR inv_comm_cursor IS
--SELECT fzreexp_item_no,
--       fzreexp_comm_desc,
--       fzreexp_appr_amt
--  FROM fzreexp
-- WHERE fzreexp_inv_num = TRIM (g_vend_inv_code) AND fzreexp_status = 'N';
CURSOR inv_comm_cursor IS
SELECT fzreexp_item_no,
       fzreexp_comm_desc,
       CASE
           WHEN (SELECT SUM (b.fzreexp_appr_amt)
                   FROM fzreexp b
                  WHERE b.fzreexp_inv_num = TRIM (g_vend_inv_code)
                    AND fzreexp_status = 'N') < 0
           THEN
                CASE WHEN fzreexp_appr_amt < 0 then ABS(fzreexp_appr_amt)
                     WHEN fzreexp_appr_amt > 0 then (fzreexp_appr_amt*-1)
                     ELSE fzreexp_appr_amt
                END
           ELSE fzreexp_appr_amt
       END AS fzreexp_appr_amt
  FROM fzreexp
 WHERE fzreexp_inv_num = TRIM (g_vend_inv_code)
   AND fzreexp_status = 'N';
/* <DGS 05292018>  Replaced cursor below
cursor inv_acctg_cursor is 
select fzreexp_item_no, fzreexp_index, fzreexp_acct_code, fzreexp_appr_amt appr_amt, fzreexp_foapl foapl
from fzreexp
where fzreexp_inv_num = trim(g_vend_inv_code)
and fzreexp_status = 'N';
*/
--CURSOR inv_acctg_cursor IS
--  SELECT MAX (fzreexp_index)    fzreexp_index,
--         fzreexp_acct_code,
--    SUM (fzreexp_appr_amt) appr_amt,
--         fzreexp_foapl
--    FROM fzreexp
--   WHERE fzreexp_inv_num = TRIM (g_vend_inv_code) AND fzreexp_status = 'N'
--GROUP BY fzreexp_foapl, fzreexp_acct_code;
/* Formatted on 9/17/2019 4:48:48 PM (QP5 v5.336) */
-- VR 9/30/19 INC0089249 added case stment to inv_acctg_cursor
-- VR 9/30/19 INC0089249 when its a credit memo (negative amt when gouped by fzreexp_inv_num) we need to flip the values of the acct lines.
CURSOR inv_acctg_cursor IS
SELECT MAX (fzreexp_index)    fzreexp_index,
         fzreexp_acct_code,
         CASE
             WHEN invoiceSum < 0
                THEN CASE  WHEN SUM(fzreexp_appr_amt) < 0 then ABS(SUM(fzreexp_appr_amt))
                           WHEN SUM(fzreexp_appr_amt) > 0 then (SUM(fzreexp_appr_amt)*-1)
                           ELSE SUM(fzreexp_appr_amt)
                           END
             ELSE SUM(fzreexp_appr_amt)
         END AS appr_amt,
         fzreexp_foapl
    FROM fzreexp, (SELECT SUM (b.fzreexp_appr_amt) invoiceSum
                   FROM fzreexp b
                  WHERE b.fzreexp_inv_num = TRIM (g_vend_inv_code)
                    AND fzreexp_status = 'N') invoice
   WHERE fzreexp_inv_num = TRIM (g_vend_inv_code)
     AND fzreexp_status = 'N'
GROUP BY fzreexp_foapl, fzreexp_acct_code, invoiceSum;
/* <DGS 05292018>  New Cursor for acctg error */
cursor acctg_error_cursor (p_foapl varchar2, p_acct_code varchar2) is
select fzreexp_trans_date,fzreexp_comm_desc,sum(fzreexp_appr_amt) appr_amt,fzreexp_foapl||'-'||FZREEXP_ACCT_CODE foapl_code
        from fzreexp
      where fzreexp_inv_num = trim(g_vend_inv_code)
        and fzreexp_foapl = p_foapl
        and fzreexp_acct_code = p_acct_code
        and fzreexp_status = 'N'
        group by fzreexp_trans_date,fzreexp_comm_desc,fzreexp_foapl||'-'||FZREEXP_ACCT_CODE
        order by fzreexp_trans_date;


g_eoy_accr_status_ind ftvfsyr.ftvfsyr_eoy_accr_status_ind%TYPE;
g_fsyr_code ftvfspd.FTVFSPD_FSYR_CODE%TYPE;
g_fspd_code ftvfspd.ftvfspd_fspd_code%TYPE;

acctg_seq number:= 0;

begin -- p_create_items
--
   g_prc_name := 'p_create_items';
--
    select ftvfsyr_eoy_accr_status_ind, ftvfspd.FTVFSPD_FSYR_CODE, ftvfspd.ftvfspd_fspd_code 
      into g_eoy_accr_status_ind, g_fsyr_code, g_fspd_code
      from ftvfsyr, ftvfspd
     where ftvfsyr_coas_code = ftvfspd_coas_code
       and ftvfsyr_coas_code = '1'
       and ftvfsyr_fsyr_code = ftvfspd_fsyr_code
       and trunc(g_trans_date) between ftvfspd_prd_start_date and ftvfspd_prd_end_date;

   for i in inv_comm_cursor loop


-- create g_comm_rec
    g_comm_rec.r_invh_code                     := g_inv_code;
    g_comm_rec.r_pohd_code                     := null;
    g_comm_rec.r_item                          := i.fzreexp_item_no;
    g_comm_rec.r_po_item                       := null;
    g_comm_rec.r_open_paid_ind                 := 'O';
    g_comm_rec.r_user_id                       := g_inv_user;
    g_comm_rec.r_comm_code                     := null;
    g_comm_rec.r_comm_desc                     := i.fzreexp_comm_desc;
    g_comm_rec.r_uoms_code                     := null;
    g_comm_rec.r_adjt_code                     := null;
    g_comm_rec.r_adjt_date                     := null;
    g_comm_rec.r_part_pmt_ind                  := null;
    g_comm_rec.r_prev_paid_qty                 := 1;
    g_comm_rec.r_recvd_qty                     := null;
    g_comm_rec.r_invd_unit_price               := null;
    g_comm_rec.r_invd_qty                      := null;
    g_comm_rec.r_accept_qty                    := null;
    g_comm_rec.r_accept_unit_price             := null;
    g_comm_rec.r_disc_amt                      := 0;
    g_comm_rec.r_tax_amt                       := 0;
    g_comm_rec.r_appr_qty                      := 1;
    g_comm_rec.r_appr_unit_price               := i.fzreexp_appr_amt;
    g_comm_rec.r_tol_override_ind              := null;
    g_comm_rec.r_hold_ind                      := 'N';
    g_comm_rec.r_susp_ind                      := 'N';
    g_comm_rec.r_ttag_num                      := null;
    g_comm_rec.r_addl_chrg_amt                 := null;
    g_comm_rec.r_convert_unit_price            := null;
    g_comm_rec.r_convert_disc_amt              := null;
    g_comm_rec.r_convert_tax_amt               := null;
    g_comm_rec.r_convert_addl_chrg_amt         := null;
    g_comm_rec.r_tgrp_code                     := null;
    g_comm_rec.r_tag_cap_code                  := 'N';
    g_comm_rec.r_vend_inv_code                 := g_vend_inv_code;
    g_comm_rec.r_vend_inv_date                 := g_invoice_date;
    g_comm_rec.r_vend_inv_item                 := i.fzreexp_item_no;
    g_comm_rec.r_prev_amt                      := null;
    g_comm_rec.r_desc_chge_ind                 := null;
    g_comm_rec.r_last_rcvr_ind                 := null;
    g_comm_rec.r_override_tax_amt              := null;
    g_comm_rec.r_data_origin                   := g_orig;
    g_comm_rec.r_create_user                   := g_inv_user;
    g_comm_rec.r_create_date                   := trunc(sysdate);
     
--  Select error msg info     
    begin
      select fzreexp_trans_date||chr(9)||fzreexp_comm_desc||chr(9)||chr(9)||fzreexp_appr_amt||chr(9)||fzreexp_index||'-'||FZREEXP_ACCT_CODE
        into l_msg
        from fzreexp
      where fzreexp_inv_num = g_vend_inv_code
        and fzreexp_item_no = i.fzreexp_item_no
        and fzreexp_status = 'N';
    exception
      when others then l_msg := 'Error selecting msg info '||sqlerrm;
    end;
    
--  Create the commodity lines
    fb_invoice_item.p_create_item(g_comm_rec);
   
--
   end loop;
   
   for j in inv_acctg_cursor loop
    acctg_seq := acctg_seq + 1;
    l_msg :=  null;
    l_fund_code := substr(j.fzreexp_foapl,1,instr(j.fzreexp_foapl,'-',1,1)-1); 
    l_orgn_code := substr(j.fzreexp_foapl,instr(j.fzreexp_foapl,'-',1,1)+1, (instr(j.fzreexp_foapl,'-',1,2) - (instr(j.fzreexp_foapl,'-',1,1)+1))) ;
    l_prog_code := substr(j.fzreexp_foapl,instr(j.fzreexp_foapl,'-',1,2)+1,length(j.fzreexp_foapl)); 

--dbms_output.put_line('Invoice '||g_inv_code);
--dbms_output.put_line('Vend Inv '||g_vend_inv_code);
--dbms_output.put_line('Item no '||j.fzreexp_item_no);

--   Create the accounting record
    g_acctg_rec.r_invh_code                     := g_inv_code;
    g_acctg_rec.r_pohd_code                     := null;
    g_acctg_rec.r_item                          :=  0;     --<DGS 05292018> changed from j.fzreexp_item_no;
    g_acctg_rec.r_po_item                       := null;
    g_acctg_rec.r_seq_num                       := acctg_seq;
    g_acctg_rec.r_user_id                       := g_inv_user;
    g_acctg_rec.r_fsyr_code                     := g_fsyr_code;
    g_acctg_rec.r_period                        := g_fspd_code;
    g_acctg_rec.r_eoy_accr_status_ind           := g_eoy_accr_status_ind;    
    --VR 9/16/2019 update rucl based on g_r_rucl_code set in get_inv header if its a credit memo for INC0089249	
    g_acctg_rec.r_rucl_code                     := g_r_rucl_code;
    g_acctg_rec.r_disc_rucl_code                := null;
    g_acctg_rec.r_tax_rucl_code                 := null;
    g_acctg_rec.r_addl_rucl_code                := null;
    g_acctg_rec.r_coas_code                     := '1';
    g_acctg_rec.r_acci_code                     := j.fzreexp_index;
    g_acctg_rec.r_fund_code                     := l_fund_code;
    g_acctg_rec.r_orgn_code                     := l_orgn_code;
    g_acctg_rec.r_acct_code                     := j.fzreexp_acct_code;
    g_acctg_rec.r_prog_code                     := l_prog_code;
    g_acctg_rec.r_actv_code                     := null;
    g_acctg_rec.r_locn_code                     := null;
    g_acctg_rec.r_bank_code                     := g_bank_code;
    g_acctg_rec.r_invd_amt                      := null;
    g_acctg_rec.r_disc_amt                      := 0;
    g_acctg_rec.r_tax_amt                       := 0;
    g_acctg_rec.r_addl_chrg_amt                 := 0;
    g_acctg_rec.r_appr_amt                      := j.appr_amt;
    g_acctg_rec.r_prev_paid_amt                 := j.appr_amt;
    g_acctg_rec.r_nsf_override_ind              := 'N';
    g_acctg_rec.r_ityp_seq_code                 := null;
    g_acctg_rec.r_proj_code                     := null;
    g_acctg_rec.r_susp_ind                      := 'N';
    g_acctg_rec.r_nsf_susp_ind                  := 'N';
    g_acctg_rec.r_partial_liq_ind               := null;
    g_acctg_rec.r_appr_ind                      := 'N';
    g_acctg_rec.r_open_paid_ind                 := 'O';
    g_acctg_rec.r_convert_amt                   := null;
    g_acctg_rec.r_convert_disc_amt              := null;
    g_acctg_rec.r_convert_tax_amt               := null;
    g_acctg_rec.r_convert_addl_chrg_amt         := null;
    g_acctg_rec.r_appr_amt_pct                  := null;
    g_acctg_rec.r_disc_amt_pct                  := null;
    g_acctg_rec.r_addl_amt_pct                  := null;
    g_acctg_rec.r_tax_amt_pct                   := null;
    g_acctg_rec.r_cap_amt                       := null;
    g_acctg_rec.r_data_origin                   := g_orig;

-- Create error message info
-- Change to a cursor and string together
     begin
--       select fzreexp_trans_date||chr(9)||fzreexp_comm_desc||chr(9)||chr(9)||fzreexp_appr_amt||chr(9)||fzreexp_index||'-'||FZREEXP_ACCT_CODE
/*        select fzreexp_appr_amt||chr(9)||fzreexp_index||'-'||FZREEXP_ACCT_CODE
        into l_msg
        from fzreexp
      where fzreexp_inv_num = g_vend_inv_code
        and fzreexp_foapl = j.fzreexp_foapl         -- <DGS 05292018>  constraint changed from item no to foapl and acct_code
        and fzreexp_acct_code = j.fzreexp_acct_code
        and fzreexp_status = 'N';
*/
        for k in acctg_error_cursor(j.fzreexp_foapl, j.fzreexp_acct_code) loop
           l_msg := l_msg||k.fzreexp_trans_date||chr(9)||k.fzreexp_comm_desc||chr(9)||chr(9)||k.appr_amt||chr(9)||k.foapl_code||chr(10);
        end loop;
        
    exception
      when others then l_msg := 'Error selecting msg info '||sqlerrm;
    end;

--   Create the accounting lines
     fb_invoice_acctg.p_create_accounting(g_acctg_rec);
--  if no error is thrown, wipe out l_msg;     
     l_msg := null;
   end loop;
  
--
exception when others then
   p_msg := substr(SQLERRM,1,100)||' '||g_vend_inv_code||Chr(10);
   p_msg := p_msg||'Expense Date'||chr(9)||'Commodity'||chr(9)||chr(9)||'Amount'||chr(9)||'Fund-Org-Prog-Acct'||chr(10);
 --  p_msg := 'Amount'||chr(9)||'Index-Acct'||chr(10);
   p_msg := p_msg||l_msg;
   l_msg := null;
end p_create_items;
--
--==============================================================================
--
procedure p_ProcessInvoice(p_batch_id in varchar2) is
   l_msg varchar2(4000);
   l_str varchar2(4000);
   p_msg varchar2(4000);
   l_sum number(12,2);
   found_vend_pidm varchar2(1) := 'N';
   debug_stmt varchar2(1000);
   
--<DGS 05112018>  Start
   fye_test varchar2(1) := 'N';
   g_enc_roll_ind varchar2(1) := 'N';
--<DGS 05112018> End

cursor inv_cursor is
   select fzreexp_inv_num vend_inv_code, max(upper(fzreexp_vend_ID)) vend_id, max(fzreexp_exp_export_date) exp_inv_date,
          max(fzreexp_amx_stmt_date) amx_inv_date, max(fzreexp_amx_export_date) amx_trans_date, max(fzreexp_amx_chid) amx_chid,
          max(fzreexp_inv_type) inv_type,
          max(fzreexp_inv_due_date) inv_due_date, max(fzreexp_bank_code) bank_code, sum(fzreexp_appr_amt) appr_amt
    from fzreexp
    where fzreexp_status = 'N'
     and fzreexp_batch_id = p_batch_id
    group by fzreexp_inv_num;
    
--------------------------------------------------------------------------------
procedure p_update_log is
begin
    update fzreexp
      set fzreexp_invh_code   = g_inv_code,
          fzreexp_status      = g_doc_status,   
          fzreexp_err_msg     = g_err_msg,
          fzreexp_load_date   = sysdate
    where fzreexp_inv_num     = g_vend_inv_code
      and fzreexp_batch_id    = p_batch_id;
   commit;
end;
--------------------------------------------------------------------------------
procedure p_create_header(p_msg in out varchar2) is
begin
--
   g_prc_name := 'p_create_header';
--
--dbms_output.put_line('  Invoice Header');
--dbms_output.put_line('    invoiceID           '||g_vend_inv_code);
--dbms_output.put_line('    FABINVH_CODE        '||g_inv_code);
--dbms_output.put_line('    FABINVH_USER_ID     '||g_header_rec.r_user_id);
--dbms_output.put_line('    FABINVH_CREATE_USER '||g_header_rec.r_create_user);
--dbms_output.put_line('    fabinvh_vend_pidm  '|| g_vend_pidm);
--dbms_output.put_line('invoice trans date '||g_trans_date);
--dbms_output.put_line('payment due date '||g_pmt_due_date);

  -- dbms_output.put_line('Vendor InvCode in '||g_header_rec.r_vend_inv_code);
   fb_invoice_header.p_create_header(g_header_rec);
--
-- Assign returned values to global variables
   g_inv_code             := g_header_rec.r_code;
   g_vend_inv_code        := g_header_rec.r_vend_inv_code;
   
  -- dbms_output.put_line('Vendor inv code: '||g_vend_inv_code);
  
exception when others then
   p_msg := substr(SQLERRM,1,80)||'For '||g_vend_inv_code||'TransDt: '||g_trans_date||' T-ID: '||TCAPP.TC_ARGOS_UTIL.tc_get_T_from_UNI(g_vend_id);
  --p_msg := substr(SQLERRM,1,100)||' '||g_vend_inv_code||' T-ID: '||g_vend_id;
end;
--------------------------------------------------------------------------------
procedure p_complete(p_msg in out varchar2) is
   l_succ_msg gb_common_strings.err_type;
   l_err_msg  gb_common_strings.err_type;
   l_warn_msg gb_common_strings.err_type;
-----------------------------------------
procedure p_set_post(p_set varchar2) is
begin gb_common.p_set_context('FP_INVOICE.P_COMPLETE', 'BATCH_TRAN_POST', p_set); end;
-----------------------------------------
function f_cln_msg(p_msg varchar2) return varchar2 is
begin return replace(rtrim(ltrim(p_msg,':'),':'),'::::',chr(10)); end;
-----------------------------------------
procedure p_complete_invoice is
begin
dbms_output.put_line('Call fp_invoice.p_complete');
   fp_invoice.p_complete
             (p_code => g_inv_code,
           p_user_id => g_inv_user,
    p_completion_ind => 'Y',
   p_success_msg_out => l_succ_msg,
     p_error_msg_out => l_err_msg,
      p_warn_msg_out => l_warn_msg);
      
--dbms_output.put_line('l_succ_msg: '||l_succ_msg);
--dbms_output.put_line('l_err_msg: '||l_err_msg);
--dbms_output.put_line('l_warn_msg: '||l_warn_msg);
      
end;
-----------------------------------------
begin -- p_complete
--  
   p_set_post('TRUE');  
--
   p_complete_invoice;
--
   if l_err_msg is not null then
--    Got error trying to post. Try now just to complete.
      p_set_post('FALSE');
      p_complete_invoice;
   end if;
--
   g_nsf_appr_byp  := 'N';
   if upper(l_err_msg) like '%CANNOT COMPLETE A DOCUMENT HAVING TRANSACTIONS IN NSF STATUS WHEN APPROVALS ARE BYPASSED%' then
      l_succ_msg := 'Cannot complete a document having transactions in NSF status when approvals are bypassed.';
      l_err_msg := null;
      g_nsf_appr_byp  := 'Y';
   end if;
--
   p_msg := f_cln_msg(l_err_msg);
--
exception when others then
   p_msg := substr(SQLERRM,1,80)||' in p_complete '||g_vend_inv_code;
end;
---------------------------------------------------------------------------------------
procedure p_done is
begin
--
   if l_msg is null then
      p_check_nsf_invoice(g_inv_code);
      
        g_doc_status := 'C';
        g_err_msg := null;
      
   else
      if l_msg like 'Vendor Invoice%already exists%' then
        g_doc_status := 'D';
      else
        g_doc_status := 'E';
      end if;
      g_err_msg := substr(l_msg,1,400);  --<DGS 08202018>
      l_msg := null;
   end if;
--
end;

---------------------------------------------------------------------------------------
procedure p_insrt_vend_record
is
/* Will only create for employees.  Students will raise an error */
begin
insert into ftvvend (
FTVVEND_PIDM,
FTVVEND_EFF_DATE,
FTVVEND_TERM_DATE,
FTVVEND_ACTIVITY_DATE,
FTVVEND_USER_ID,
FTVVEND_ITYP_SEQ_CODE,
FTVVEND_DISC_CODE,
FTVVEND_1099_RPT_ID,
FTVVEND_TRAT_CODE,
FTVVEND_IN_ST_IND,
FTVVEND_GROUPING_IND,
FTVVEND_CARRIER_IND,
FTVVEND_CONTACT,
FTVVEND_PHONE_AREA,
FTVVEND_PHONE_NUMBER,
FTVVEND_PHONE_EXT,
FTVVEND_ATYP_CODE,
FTVVEND_ADDR_SEQNO,
FTVVEND_FED_WHOLD_PCT,
FTVVEND_ST_WHOLD_PCT,
FTVVEND_COMBINED_FED_ST_FILER,
FTVVEND_COLLECT_TAX,
FTVVEND_VEND_CHECK_PIDM,
FTVVEND_VEND_CHECK_ATYP_CODE,
FTVVEND_VEND_CHECK_ADDR_SEQNO,
FTVVEND_CURR_CODE,
FTVVEND_ENTITY_IND,
FTVVEND_PIDM_OWNER,
FTVVEND_EPROC_IND,
FTVVEND_DATA_ORIGIN,
FTVVEND_CTRY_CODE_PHONE,
FTVVEND_PO_HOLD_RSN_CODE,
FTVVEND_PMT_HOLD_RSN_CODE,
FTVVEND_1099_ATYP_CODE,
FTVVEND_1099_ADDR_SEQNO,
FTVVEND_TAX_FORM_STATUS,
FTVVEND_TAX_FORM_DATE,
FTVVEND_SURROGATE_ID,
FTVVEND_VERSION,
FTVVEND_VPDI_CODE
)
select
a.spriden_pidm,     --ftvvend_pidm
pebempl_first_hire_date,   --ftvvend_eff_date
null,                      --ftvvend_term_date
sysdate,         --ftvvend_activity_date
user,            --ftvvend_user_id
null,            --ftvvend_ityp_seq_code  
null,            --ftvvend_disc_code
null,            --ftvvend_1099_rpt_id
null,            --ftvvend_trat_code
decode(spraddr_stat_code, 'NY','I','O'),            --ftvvend_in_st_ind    can be I, O or null
'M',             --ftvvend_grouping_ind 
null,            --ftvvend_carrier_ind
null,            -- ftvvend_contact
null,            --ftvvend_phone_area
null,            --ftvvend_phone_number
null,            --ftvvend_phone_ext
spraddr_atyp_code,             --ftvvend_atyp_code
spraddr_seqno,                 --ftvvend_addr_seqno
null,           --ftvvend_fed_whold_pct
null,           --ftvvend_st_whold_pct
null,           --ftvvend_combined_fed_st_filer
'N',            -- ftvvend_collect_tax
null,           -- ftvvend_vend_check_pidm
null,           --ftvvend_vend_check_atyp_code, 
null,           --ftvvend_vend_check_addr_seqno, 
null,           --ftvvend_curr_code,
'P',            --ftvvend_entity_ind,
null,           --ftvvend_pidm_owner
'N',            --ftvvend_eproc_ind
'Banner',           --ftvvend_data_origin
null,           --ftvvend_ctry_code_phone
null,           --ftvvend_po_hold_rsn_code
null,           --ftvvend_pmt_hole_rsn_code
null,           --ftvvend_1099_atyp_code
null,           --ftvvend_1099_addr_seqno
null,           --ftvvend_tax_form_status
null,           --ftvvend_tax_form_date
null,           --ftvvend_surrogate_id
null,           --ftvvend_version
null            --ftvvend_vpdi_code
from spriden a, nbrbjob n, nbbposn, nbrjobs, pebempl, spraddr
where a.spriden_change_ind is null
and a.spriden_entity_ind = 'P'
and a.spriden_pidm = pebempl_pidm
and a.spriden_pidm = n.nbrbjob_pidm
and n.nbrbjob_contract_type = 'P'
and trunc(sysdate) between trunc(n.nbrbjob_begin_date) and trunc(nvl(n.nbrbjob_end_date,sysdate))
and n.nbrbjob_posn = nbbposn_posn
and a.spriden_pidm = nbrjobs_pidm
and nbrjobs_status <> 'T'
and nbrjobs_pidm = nbrbjob_pidm
and nbrjobs_posn = nbrbjob_posn
and nbrjobs_suff = nbrbjob_suff
and nbrjobs_effective_date =
          (select MAX(n.nbrjobs_effective_date)
             from nbrjobs n
            where n.nbrjobs_suff = nbrbjob_suff
              and n.nbrjobs_posn = nbrbjob_posn
              and n.nbrjobs_pidm = nbrbjob_pidm
              and trunc(n.nbrjobs_effective_date) <= trunc(sysdate))
and exists (select 'y' from spriden b where b.spriden_pidm = a.spriden_pidm
                     and b.spriden_ntyp_code = 'UNI'
                     and b.spriden_entity_ind = 'P')
and not exists (select 'y' from ftvvend
                 where ftvvend_pidm = a.spriden_pidm
                  )
and a.spriden_pidm = spraddr_pidm
and spraddr.rowid = f_get_address_fnc(a.spriden_pidm, 'CRADDR','A',sysdate,1,'P','')
and a.spriden_pidm = g_vend_pidm;

exception
  when no_data_found then l_msg := 'Unable to create vendor: '||sqlerrm;
  when others then l_msg := 'Error creating vendor: '||sqlerrm;
end;

---------------------------------------------------------------------------------------
begin -- p_ProcessInvoice  The real start for processing.
--
   g_prc_name := 'p_ProcessInvoice';
   g_user_coas := '1';

--
--<DGS 05112018> Start
/* selects for fiscal year end adjustment */
  debug_stmt := 'stmt 1';
  begin
      select 'Y'
        into fye_test 
        from ftvfspd
        where trunc(sysdate) between trunc(ftvfspd_prd_start_date) and trunc(ftvfspd_prd_end_date)
        and ftvfspd_fspd_code = '01';
   exception
      when no_data_found then fye_test := 'N';
      when others then
         l_msg := 'Unable to determine FSPD';
    end;

 if fye_test = 'Y' then
   debug_stmt := 'stmt 2';
   begin            
     select decode(fgbyrlm_encb_per_date,null,'N','Y')  
     into  g_enc_roll_ind
     from ftvfsyr, fgbyrlm
    where ftvfsyr_fsyr_code = (select ftvfsyr_fsyr_code-1 
                                from ftvfsyr 
                               where trunc(sysdate) between trunc(ftvfsyr_start_date) and trunc(ftvfsyr_end_date)
                                 and ftvfsyr_coas_code = '1')
      and ftvfsyr_coas_code = '1'
      and fgbyrlm_fsyr_code (+) = ftvfsyr_fsyr_code
      and fgbyrlm_coas_code (+) = '1';
      
   exception
     when no_data_found then
      g_enc_roll_ind := 'N';
     when others then
      l_msg := 'Unable to determine enc roll status'; 
   end;
 end if;
 
 --dbms_output.put_line('fye_test completed');
 
 -- <DGS 05112018> end
 
 for i in inv_cursor loop
 
   debug_stmt := 'stmt 3';
   g_vend_id := i.vend_id;
   g_vend_inv_code := i.vend_inv_code;
   g_invoice_date := case i.inv_type when 'AMX' then 
                           i.amx_inv_date
                           else
                           i.exp_inv_date
                      end;
   g_trans_date :=  case i.inv_type when 'AMX' then
                                     i.amx_trans_date
                                    else
                                    i.exp_inv_date
                      end;
                      
    g_pmt_due_date := i.inv_due_date;
      
-- <DGS 05112018> start                     
  /* adjust the transaction date during fiscal year end */
   if fye_test = 'Y' and g_enc_roll_ind = 'N' then
   debug_stmt := 'stmt 4';
      select trunc(to_date('31-AUG-'||to_char(sysdate,'yyyy'),'dd-mon-yyyy'))
    -- select trunc(to_date('10-JUL-'||to_char(sysdate,'yyyy'),'dd-mon-yyyy'))
        into g_trans_date
        from dual;
        
      select greatest(i.inv_due_date,trunc(sysdate +1))
        into g_pmt_due_date 
        from dual;
   end if;
   
   
-- <DGS 05112018> end   

   g_bank_code := i.bank_code;
   l_sum := i.appr_amt;
   g_inv_type := i.inv_type;
   l_msg := null;
   g_doc_status := null;
   g_inv_code := null;
   fokutil.p_gen_doc_code(3, g_inv_code);       

-- Get payee pidm  
  if i.inv_type = 'AMX' then
     debug_stmt := 'stmt 5';
     begin
       select spraddr_pidm 
         into g_vend_pidm
         from spraddr
        where spraddr_atyp_code = 'AM'
          and spraddr_to_date is null
          and trim(spraddr_street_line1) = (select a.spriden_id from spriden a
                                      where a.spriden_change_ind is null
                                      and a.spriden_pidm = (select b.spriden_pidm from spriden b
                                                            where b.spriden_ntyp_code = 'UNI'
                                                              and b.spriden_id = i.amx_chid));
     exception
       when no_data_found then l_msg := 'Amex Vendor for'||i.amx_chid||' not found.';
       when others then l_msg := substr(sqlerrm,1,80);
     end;   
   else
     begin
       debug_stmt := 'stmt 6';
       select distinct spriden_pidm
         into g_vend_pidm
         from spriden
        where spriden_id = g_vend_id;
     exception
       when no_data_found then l_msg := 'Vendor '||g_vend_id||' not found.';
       when others then l_msg := substr(sqlerrm,1,80);
     end;   
  end if;
  
  -- <DGS 041318> check for employee vendor record here and create if it doesn't exist
 
  begin
    debug_stmt := 'stmt 7';
    select 'y'
      into found_vend_pidm
      from ftvvend
      where ftvvend_pidm = g_vend_pidm;
  exception
     when no_data_found then p_insrt_vend_record;
     when others then l_msg := 'Error checking vendor: '||g_vend_pidm;
  end;
  
--If this is an Amex invoice adjust the invoice due date if necessary
if g_inv_type = 'AMX' then
  if g_pmt_due_date <= g_trans_date then
     debug_stmt := 'stmt 8';
     g_pmt_due_date := case when to_number(to_char(g_trans_date,'dd')) < 17 then
                                 g_trans_date + 18- to_number(to_char(g_trans_date,'dd'))
                            when to_number(to_char(g_trans_date,'dd')) >= 17 then 
                                 add_months(g_trans_date,1) + 18- to_number(to_char(g_trans_date,'dd'))
                       end;     
  end if;
end if;
    
-- Create the invoice
  if l_msg is null then
   debug_stmt := 'stmt 9';
-- VR 9/30/19 INC0089249 added 'i.appr.amt' parameter to pass amount and set CR memo indicator, and rucl code conditionally
   p_get_inv_header(l_msg,i.appr_amt);
 --  dbms_output.put_line('Get inv header msg ');
 --  dbms_output.put_line(l_msg);
   
   if l_msg is null then   
--
 --  dbms_output.put_line('Create inv header');
      debug_stmt := 'stmt 10';
      p_create_header(l_msg);
--
--   dbms_output.put_line(l_msg);
 
      if l_msg is null then
        debug_stmt := 'stmt 11';    
         p_create_items(l_msg);
  -- dbms_output.put_line('Create lines msg ');
  -- dbms_output.put_line(l_msg);
   
      end if;
--
     if l_msg is null then
  --      dbms_output.put_line(' calling p_complete '||l_msg);
        debug_stmt := 'stmt 12';
        p_complete(l_msg); 
  --      dbms_output.put_line('p_complete '||l_msg);
     end if;
--
    end if;
  end if;
  
--  dbms_output.put_line('just before update status');
-- Update status
  debug_stmt := 'stmt 13';
   p_done;  
   debug_stmt := 'stmt 13a';
   
--   dbms_output.put_line('just after p_done');
   debug_stmt := 'stmt 14';
   p_update_log;
--   dbms_output.put_line('just after p_update_log');
   
-- Send Error email if needed
   begin
     debug_stmt := 'stmt 15';
     select distinct fzreexp_err_msg
       into l_msg
       from fzreexp
       where fzreexp_err_msg is not null
         and fzreexp_inv_num = i.vend_inv_code
         and fzreexp_batch_id = p_batch_id;
      
      debug_stmt := 'stmt 16';  
      l_msg := i.vend_inv_code||': '||l_msg;
      p_send_error_email(l_msg);
      
   exception
      when no_data_found then null;
      when others then p_send_error_email(sqlerrm);
   end;
--
end loop;

exception when others then
  p_msg := substr(SQLERRM,1,80)|| ' '||debug_stmt;  -- <DGS 08202018>
  update fzreexp
      set fzreexp_invh_code   = g_inv_code,
          fzreexp_status      = 'E',   
          fzreexp_err_msg     = p_msg||' '|| g_vend_inv_code,
          fzreexp_load_date = trunc(sysdate)
    where fzreexp_inv_num     = g_vend_inv_code
      and fzreexp_batch_id    = p_batch_id;
      
  p_send_error_email(g_vend_inv_code||' '||p_msg);  --  <DGS 08202018>
  
end p_ProcessInvoice;
-----------------------------------------------------------------------
procedure p_Reset_Error(p_msg in out varchar2, p_err_type in varchar2) is

batch_count number;
batch_total number(12,2);
next_batch number;
inv_posted varchar2(1) := 'N';
item_num number := 0;
l_msg varchar2(100) := null;

cursor reset_batch is 
select distinct fzreexp_invh_code invh_code, fzreexp_batch_id
  from fzreexp
  where fzreexp_status = 'E'
    and fzreexp_err_msg like '%NSF%'
    and p_err_type in ('N','A')
union
select distinct fzreexp_invh_code invh_code, fzreexp_batch_id
  from fzreexp
  where fzreexp_status = 'E'
    and fzreexp_err_msg not like '%NSF%'
    and p_err_type in ('O','A');
  
begin
/* select the next batch number */

if l_msg is null then 
select fimsmgr.cr_batch_seq.nextval
  into next_batch
  from dual;
end if;

for i in reset_batch loop
l_msg := null;
/* check that the invoice didn't post */
begin
select 'Y' 
 into inv_posted
 from fgbtrnd
 where fgbtrnd_doc_code = i.invh_code;
exception
   when too_many_rows then inv_posted := 'Y'; -- VR 9/30/2019 INC0089249 Hotfix - added "when too_many_rows then inv_posted := 'Y';"
   when no_data_found then inv_posted := 'N';
   when others then l_msg := substr(sqlerrm,1,100); 
end;

    if inv_posted <> 'Y' then 

    /* clean out the records from the Banner tables */
    /* Note - invoices that have already posted will fail when the new batch is processed.  
    This is intentional so that we have the processing history */

        begin
          delete from farinva
          where farinva_invh_code = i.invh_code;
        exception
           when no_data_found then null;
           when others then l_msg := substr(sqlerrm,1,100);
        end;

        begin
          delete from farinvc
          where farinvc_invh_code = i.invh_code;
        exception
           when no_data_found then null;
           when others then l_msg := substr(sqlerrm,1,100);
        end;

        begin
          delete from fabinvh
          where fabinvh_code = i.invh_code;
        exception
           when no_data_found then null;
           when others then l_msg := substr(sqlerrm,1,100);
        end;
        commit;
        --end if;

             --VR 9/30/2019 INC0089249 Hotfix - Commented out if statement
             --IF L_MSG is not null (in the case that inv_posted query return too many records , or if deletion fails) it may get overwritten in the loop and never reported. VR 9/30
             
          -- for j in (select fzreexp_item_no item_no from fzreexp where fzreexp_invh_code = i.invh_code and fzreexp_status = 'E') loop
          --   item_num := item_num + 1;
                 insert into fzreexp
                 select next_batch, null,fzreexp_inv_num, fzreexp_external_doc_id, 
                 fzreexp_inv_type, 
                 FZREEXP_EXP_EXPORT_DATE,
                 FZREEXP_AMX_STMT_DATE,
                 FZREEXP_AMX_EXPORT_DATE,
                 fzreexp_amx_chid,
                 fzreexp_inv_due_date,
                 fzreexp_inv_desc, fzreexp_foapl, fzreexp_trans_date, fzreexp_item_no, 
                 fzreexp_comm_desc, fzreexp_index,
                 fzreexp_acct_code, fzreexp_appr_amt, fzreexp_bank_code,decode(l_msg,null,'N','E'),l_msg,
                 trunc(sysdate),null, fzreexp_vend_id,null,null
                 from fzreexp
                 where fzreexp_invh_code = i.invh_code
            --    and fzreexp_item_no = j.item_no
                   and fzreexp_status = 'E';
                 
            --  end loop;     
                 /* Update the prior batch record status */
                 update fzreexp
                 set fzreexp_status = 'R'
                 where fzreexp_invh_code = i.invh_code
                   and fzreexp_batch_id = i.fzreexp_batch_id
                   and fzreexp_status = 'E';     
             --else 
             --end if; --VR 9/30/2019 INC0089249 Hotfix - Commented out if statement
    else /*Posted, no further processing */
    /* Update the prior batch record status */
    /* VR 9/30/2019 INC0089249 Hotfix added else statement*/
             update fzreexp
             set fzreexp_status = 'R'
             where fzreexp_invh_code = i.invh_code
               and fzreexp_batch_id = i.fzreexp_batch_id
               and fzreexp_status = 'E'; 
    end if; 
--
end loop;

begin
select count(*), sum(fzreexp_appr_amt)
  into batch_count, batch_total
  from fzreexp
where fzreexp_status = 'N';
end;

begin
 insert into fzbeexp values(next_batch, null, 'FZKEEXP '||to_char(sysdate,'yyyymmdd'), batch_count, batch_total, 'N', null);
end;

p_ProcessInvoice(next_batch);

update fzbeexp set fzbeexp_status = 'C' where fzbeexp_batch_id = next_batch;

p_msg := 'Batch Id '||to_char(next_batch)||nvl(l_msg, ': Error Processing Completed Successfully');

exception
when others then p_msg := substr(sqlerrm,1,100);
end p_Reset_Error;
--------------------------------------------------------------------------------
-- <DGS 06042018> Start
procedure p_Reprocess_DupeBatch(p_batch_id in varchar2, p_external_batch_id in varchar2, p_msg in out varchar2) is

batch_count number;
batch_total number(12,2);
 
begin

    select count(*), sum(fzreexp_appr_amt)
      into batch_count, batch_total
      from fzreexp
     where fzreexp_status = 'X'
       and fzreexp_external_batch_id = p_external_batch_id
       and fzreexp_batch_id = p_batch_id;

     insert into fzbeexp values(null, null, 'FZKEEXP Dupe '||to_char(sysdate,'yyyymmdd'), batch_count, batch_total, 'N', null);   
  
     insert into fzreexp
     select null, null,fzreexp_inv_num, fzreexp_external_doc_id, 
     fzreexp_inv_type, 
     FZREEXP_EXP_EXPORT_DATE,
     FZREEXP_AMX_STMT_DATE,
     FZREEXP_AMX_EXPORT_DATE,
     fzreexp_amx_chid,
     fzreexp_inv_due_date,
     fzreexp_inv_desc, fzreexp_foapl, fzreexp_trans_date, fzreexp_item_no, 
     fzreexp_comm_desc, fzreexp_index,
     fzreexp_acct_code, fzreexp_appr_amt, fzreexp_bank_code,'N',null,
     trunc(sysdate),null, fzreexp_vend_id,null,null
     from fzreexp
     where fzreexp_batch_id = p_batch_id
       and fzreexp_external_batch_id = p_external_batch_id
       and fzreexp_status = 'X';

    p_ProcessBatch;

    p_msg := 'Batch Id '||to_char(p_batch_id)||' for '||p_external_batch_id||' created successfully';

exception
when others then p_msg := substr(sqlerrm,1,100);
end p_Reprocess_DupeBatch;

-- <DGS 06042018> End
--------------------------------------------------------------------------------
procedure p_renumber_batch(p_batch_id in varchar2, p_msg in out varchar2)
is 
cursor distinct_invoices(p_batch_id in varchar2) is
select distinct fzreexp_inv_num  inv_num
from fzreexp
where fzreexp_batch_id = p_batch_id;

cursor inv_lines(p_batch_id in varchar2, p_inv_num in varchar2) is
select fzreexp_item_no
from fzreexp
where fzreexp_batch_id = p_batch_id
and fzreexp_inv_num = p_inv_num
order by fzreexp_item_no;

cntr number :=0;
begin
for i in distinct_invoices(p_batch_id) loop
   cntr := 0;
   for j in inv_lines(p_batch_id, i.inv_num) loop
     begin
     cntr := cntr + 1;
     
     update fzreexp
       set fzreexp_item_no = cntr
      where fzreexp_batch_id = p_batch_id
        and fzreexp_inv_num = i.inv_num
        and fzreexp_item_no = j.fzreexp_item_no;      
     exception
       when others then p_msg := substr(sqlerrm,1,80);
     end;
    end loop;
end loop;
exception
when others then p_msg := substr(sqlerrm,1,80);
end;
---------------------------------------------------------------------
procedure get_paid_expenses
is
cursor paid_expense_list is
select distinct fabinck_invh_code, fabinck_check_num, fabinck_bank_code,fzreexp_inv_num, 
fzreexp_external_doc_id, fabinck_net_amt, fzreexp_inv_type, fabinck_check_date
from fabinck, fabinvh, fzreexp
where fabinck_invh_code = fabinvh_code
and fabinck_invh_code  = fzreexp_invh_code
and fzreexp_status = 'C'
and fzreexp_feed_date is null
and fabinvh_open_paid_ind = 'P'
and fabinck_cancel_date is null
and fabinvh_cancel_date is null;

cursor get_voids is
select max(fabinck_cancel_date) cancel_date, fabinck_invh_code 
from fabinck,fzreexp
where fabinck_invh_code = fzreexp_invh_code
and fabinck_cancel_date is not null
and fzreexp_status = 'C'
and not exists (select 'y' from fzrexpd
                  where fzrexpd_invh_code = fabinck_invh_code
                    and fzrexpd_check_num = 'VOID '||fabinck_check_num)
group by fabinck_invh_code;

cursor get_reissues is
select max(fabinck_check_date) check_date, fabinck_invh_code 
from fabinck,fzreexp
where fabinck_invh_code = fzreexp_invh_code
and fabinck_cancel_date is null
and fzreexp_status = 'C'
and not exists (select 'y' from fzrexpd
                  where fzrexpd_invh_code = fabinck_invh_code
                    and fzrexpd_check_num = fabinck_check_num)
group by fabinck_invh_code;

cursor get_cash_adv_returns is
select tbrmisd_receipt_number, tbrmisd_pidm, tbrmisd_amount, tbrmisd_feed_date, tbrmisd_payment_id
from taismgr.tbrmisd, fimsmgr.cr_inv_hdr_vw
where 'CR'||tbrmisd_payment_id = fzreexp_inv_num 
and fzreexp_inv_type = 'ADV' 
and fzreexp_status = 'C'
and tbrmisd_charge_detail_code = 'CRAR'
and not exists (select 'y' from fzrexpd 
                 where fzrexpd_inv_num = tbrmisd_payment_id
                   and fzrexpd_check_num = to_char(tbrmisd_receipt_number));

g_line_cntr     number := 0;
g_ck_num        fzrexpd.fzrexpd_check_num%TYPE;
g_net_amt       fzrexpd.fzrexpd_check_amt%TYPE;
g_bank_code     fzrexpd.fzrexpd_bank_id%TYPE;
g_inv_num       fzrexpd.fzrexpd_inv_num%TYPE;
g_external_doc_id   fzrexpd.fzrexpd_external_doc_id%TYPE;
adv_external_doc_id fzrexpd.fzrexpd_external_doc_id%TYPE;
l_msg          varchar2(100);

begin
  g_line_cntr := 0; 
for i in paid_expense_list loop
   g_line_cntr := g_line_cntr + 1;
  --insert to fzrexpd table
    p_insrt_fzrexpd(p_invh_code => i.fabinck_invh_code,
                    p_check_num => i.fabinck_check_num,
                    p_check_amt => i.fabinck_net_amt, 
                    p_bank_id  => i.fabinck_bank_code,
                    p_inv_num => i.fzreexp_inv_num,
                    p_check_date => i.fabinck_check_date,
                    p_external_doc_id => i.fzreexp_external_doc_id,
                    p_feed_lineno => g_line_cntr,
                    p_msg => l_msg);
end loop;

for j in get_voids loop

   g_line_cntr := g_line_cntr + 1;
   
   select 'VOID '||fabinck_check_num, fabinck_net_amt, fabinck_bank_code, 
          fzreexp_inv_num, fzreexp_external_doc_id
     into g_ck_num, g_net_amt, g_bank_code, g_inv_num, g_external_doc_id
     from fabinck, fzreexp
    where fabinck_invh_code = fzreexp_invh_code
      and fzreexp_status = 'C'
      and fabinck_invh_code = j.fabinck_invh_code
      and fabinck_cancel_date = j.cancel_date;
      
  -- insert to fzrexpd table
    p_insrt_fzrexpd(p_invh_code => j.fabinck_invh_code,
                    p_check_num => g_ck_num,
                    p_check_amt => g_net_amt, 
                    p_bank_id  => g_bank_code,
                    p_inv_num => g_inv_num,
                    p_check_date => j.cancel_date,
                    p_external_doc_id => g_external_doc_id,
                    p_feed_lineno => g_line_cntr,
                    p_msg => l_msg);
end loop;

for k in get_reissues loop

select fabinck_check_num, fabinck_net_amt, fabinck_bank_code, 
          fzreexp_inv_num, fzreexp_external_doc_id
     into g_ck_num, g_net_amt, g_bank_code, g_inv_num, g_external_doc_id
     from fabinck, fzreexp
    where fabinck_invh_code = fzreexp_invh_code
      and fzreexp_status = 'C'
      and fabinck_invh_code = k.fabinck_invh_code
      and fabinck_check_date = k.check_date;

   g_line_cntr := g_line_cntr + 1;
  -- insert to fzrexpd table
    p_insrt_fzrexpd(p_invh_code => k.fabinck_invh_code,
                    p_check_num => g_ck_num,
                    p_check_amt => g_net_amt, 
                    p_bank_id  => g_bank_code,
                    p_inv_num => g_inv_num,
                    p_check_date => k.check_date,
                    p_external_doc_id => g_external_doc_id,
                    p_feed_lineno => g_line_cntr,
                    p_msg => l_msg); 
end loop;

for l in get_cash_adv_returns loop

   select fzreexp_external_doc_id
     into adv_external_doc_id
     from fzreexp
    where fzreexp_inv_num = l.tbrmisd_payment_id;

   g_line_cntr := g_line_cntr + 1;
  
   p_insrt_fzrexpd(p_invh_code => null,
                    p_check_num => l.tbrmisd_receipt_number,
                    p_check_amt => l.tbrmisd_amount, 
                    p_bank_id  => '14',
                    p_inv_num => l.tbrmisd_payment_id,
                    p_check_date => l.tbrmisd_feed_date,
                    p_external_doc_id => adv_external_doc_id,
                    p_feed_lineno => g_line_cntr,
                    p_msg => l_msg);
  if l_msg is not null then
    tcapp.util_msg.LogEntry('CashAdvReturn error '||l_msg);
    dbms_output.put_line(l_msg);
  end if;
end loop;

exception 
when others then dbms_output.put_line(sqlerrm);
end;

procedure p_insrt_fzrexpd (p_invh_code in varchar2,
                    p_check_num in varchar2,
                    p_check_amt in varchar2, 
                    p_bank_id in varchar2,
                    p_inv_num in varchar2,
                    p_check_date in date,
                    p_external_doc_id in varchar2,
                    p_feed_lineno in number,
                    p_msg in out varchar2) is
begin
   p_msg := null;
   insert into fzrexpd p_insrt_fzrexpd(fzrexpd_invh_code,
                    fzrexpd_check_num,
                    fzrexpd_check_amt, 
                    fzrexpd_bank_id,
                    fzrexpd_inv_num,
                    fzrexpd_check_date,
                    fzrexpd_external_doc_id ,
                    fzrexpd_feed_date,
                    fzrexpd_feed_doc_id,
                    fzrexpd_feed_lineno) values 
                    (p_invh_code,
                    p_check_num,
                    p_check_amt, 
                    p_bank_id ,
                    p_inv_num ,
                    p_check_date,
                    p_external_doc_id,
                    null,
                    null,
                    p_feed_lineno);                    
  exception 
      when others then p_msg := substr(sqlerrm, 1,80);
      dbms_output.put_line(p_msg);
end;

  FUNCTION f_chrome_multi_sup_code(p_pidm NUMBER, p_code VARCHAR2)
    RETURN VARCHAR2 IS
    /*-------------------------------------------------------------------------
    -- Use to identify employees in the CR person file feed with multiple
    -- supervisors and supervisors of employees who have multiple supervisors
    --------------------------------------------------------------------------*/
  
    --count of supervisors and list of supervisors
    CURSOR c_sup(v_pidm NUMBER) IS
    
      SELECT COUNT(supervisor) cnt,
             listagg(supervisor, ';') within GROUP(ORDER BY supervisor) sup
        FROM (SELECT DISTINCT decode(n.nbrbjob_posn,
                                     '999999',
                                     '',
                                     (SELECT c.spriden_id
                                        FROM spriden c
                                       WHERE c.spriden_pidm =
                                             (nvl(tcapp.hr_report_util.f_hrfindjqueapprover(a.spriden_pidm,
                                                                                            n.nbrbjob_posn,
                                                                                            n.nbrbjob_suff,
                                                                                            'WTE'),
                                                  tcapp.hr_report_util.f_hrfindposnsupervisor(n.nbrbjob_posn,
                                                                                              trunc(SYSDATE))))
                                         AND c.spriden_ntyp_code = 'UNI'
                                         AND c.spriden_entity_ind = 'P'
                                         AND c.spriden_pidm <> a.spriden_pidm)) supervisor
                FROM spriden a, nbrbjob n
               WHERE a.spriden_change_ind IS NULL
                 AND a.spriden_entity_ind = 'P'
                 AND a.spriden_pidm = n.nbrbjob_pidm
                 AND trunc(SYSDATE) BETWEEN trunc(n.nbrbjob_begin_date) AND
                     trunc(nvl(n.nbrbjob_end_date, SYSDATE))
                 AND spriden_pidm = v_pidm
              UNION
              SELECT fzkeexp.f_daa_lkup(nbrjobs_orgn_code_ts, a.spriden_pidm)
                FROM spriden a, nbrbjob n, nbbposn, nbrjobs
               WHERE a.spriden_change_ind IS NULL
                 AND a.spriden_entity_ind = 'P'
                 AND a.spriden_pidm = n.nbrbjob_pidm
                 AND n.nbrbjob_contract_type = 'P'
                 AND trunc(SYSDATE) BETWEEN trunc(n.nbrbjob_begin_date) AND
                     trunc(nvl(n.nbrbjob_end_date, SYSDATE))
                 AND n.nbrbjob_posn = nbbposn_posn
                 AND a.spriden_pidm = nbrjobs_pidm
                 AND nbrjobs_status <> 'T'
                 AND nbrbjob_contract_type = 'P'
                 AND nbrjobs_pidm = nbrbjob_pidm
                 AND nbrjobs_posn = nbrbjob_posn
                 AND nbrjobs_suff = nbrbjob_suff
                 AND nbrjobs_effective_date =
                     (SELECT MAX(n.nbrjobs_effective_date)
                        FROM nbrjobs n
                       WHERE n.nbrjobs_suff = nbrbjob_suff
                         AND n.nbrjobs_posn = nbrbjob_posn
                         AND n.nbrjobs_pidm = nbrbjob_pidm
                         AND n.nbrjobs_status <> 'T'
                         AND trunc(n.nbrjobs_effective_date) <=
                             trunc(SYSDATE))
                 AND nbbposn_ecls_code IN
                     ('11', '20', '24', '25', '30', '31', '35', '39', '81')
                 AND EXISTS (SELECT 'y'
                        FROM spriden b
                       WHERE b.spriden_pidm = a.spriden_pidm
                         AND b.spriden_ntyp_code = 'UNI'
                         AND b.spriden_entity_ind = 'P')
                 AND a.spriden_pidm = v_pidm);
  
    --get employees for position of supervisor
    CURSOR c_emp(posn VARCHAR2) IS
      SELECT a.nbrjobs_pidm pidm
        FROM nbrjobs a
        JOIN (SELECT *
                FROM nbbposn a1
               WHERE a1.nbbposn_posn_reports = posn
                 AND trunc(SYSDATE) BETWEEN trunc(a1.nbbposn_begin_date) AND
                     trunc(nvl(a1.nbbposn_end_date, SYSDATE))
                 AND a1.nbbposn_status = 'A') b
          ON a.nbrjobs_posn = b.nbbposn_posn
       WHERE a.nbrjobs_effective_date =
             (SELECT MAX(nbrjobs_effective_date)
                FROM nbrjobs
               WHERE nbrjobs_pidm = a.nbrjobs_pidm
                 AND nbrjobs_posn = a.nbrjobs_posn
                 AND nbrjobs_suff = a.nbrjobs_suff
                 AND nbrjobs_effective_date <= SYSDATE)
         AND nbrjobs_status = 'A'
      UNION
      SELECT b.nbrrjqe_pidm pidm
        FROM nbrjobs a
        LEFT OUTER JOIN nbrrjqe b
          ON nbrjobs_pidm = nbrrjqe_appr_pidm
       WHERE a.nbrjobs_pidm IN (p_pidm)
         AND a.nbrjobs_effective_date =
             (SELECT MAX(nbrjobs_effective_date)
                FROM nbrjobs
               WHERE nbrjobs_pidm = a.nbrjobs_pidm
                 AND nbrjobs_posn = a.nbrjobs_posn
                 AND nbrjobs_suff = a.nbrjobs_suff
                 AND nbrjobs_effective_date <= SYSDATE)
         AND nbrjobs_status = 'A';
  
    --get positions for emp passed in 
    CURSOR c_posn IS
      SELECT a.nbrjobs_posn
        FROM nbrjobs a
       WHERE a.nbrjobs_pidm = p_pidm
         AND a.nbrjobs_effective_date =
             (SELECT MAX(nbrjobs_effective_date)
                FROM nbrjobs
               WHERE nbrjobs_pidm = a.nbrjobs_pidm
                 AND nbrjobs_posn = a.nbrjobs_posn
                 AND nbrjobs_suff = a.nbrjobs_suff
                 AND nbrjobs_effective_date <= SYSDATE)
         AND nbrjobs_status = 'A';
  
    v_posn        VARCHAR2(20);
    v_return      VARCHAR2(30);
    v_chk_sub_sup VARCHAR2(1);
  BEGIN
  
    IF p_pidm IN (237145, 1150, 9027, 79745) AND p_code = 'CODE' THEN
      v_return := 'SP';
    ELSE
      v_chk_sub_sup := 'N';
      FOR r_posn IN c_posn LOOP
        FOR r_emp IN c_emp(r_posn.nbrjobs_posn) LOOP
          FOR r_sup IN c_sup(r_emp.pidm) LOOP
            dbms_output.put_line(r_sup.cnt);
            IF r_sup.cnt > 1 THEN
              v_chk_sub_sup := 'Y';
              EXIT;
            END IF;
          END LOOP;
          IF v_chk_sub_sup = 'Y' THEN
            EXIT;
          END IF;
        END LOOP;
        IF v_chk_sub_sup = 'Y' THEN
          EXIT;
        END IF;
      END LOOP;
    
      FOR r_sup IN c_sup(p_pidm) LOOP
      
        BEGIN
          SELECT CASE
                 --More than 1 supervisor and no subordinates with multiple 
                 --supervisors
                   WHEN p_code = 'CODE' AND r_sup.cnt > 1 AND v_chk_sub_sup = 'N' THEN
                    'EE'
                 --1 or less supervisor and at least one subordinate with more
                 --than 1 supervisor    
                   WHEN p_code = 'CODE' AND r_sup.cnt <= 1 AND v_chk_sub_sup = 'Y' THEN
                    'SP'
                 --more than 1 supervisor and at least one subordinate with more
                 --than 1 supervisor     
                   WHEN p_code = 'CODE' AND r_sup.cnt > 1 AND v_chk_sub_sup = 'Y' THEN
                    'ES'
                 --only return list of supervisors if emp has more 
                 --than 1    
                   WHEN p_code = 'LIST' AND r_sup.cnt > 1 THEN
                    r_sup.sup
                   ELSE
                    NULL
                 END
            INTO v_return
            FROM dual;
        EXCEPTION
          WHEN no_data_found THEN
            v_return := NULL;
            GOTO end_loop;
        END;
      
        <<end_loop>>
        NULL;
      END LOOP;
    END IF;
    RETURN v_return;
  END;
--==============================================================================
--
END FZKEEXP;
/
