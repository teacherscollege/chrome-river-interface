load data
INFILE 'crDATA.dat'
into TABLE fzreexp
append
fields terminated by '|'
(fzreexp_external_batch_id,fzreexp_inv_num, fzreexp_external_doc_id, 
fzreexp_inv_type,fzreexp_vend_id,fzreexp_exp_export_date date "yyyymmdd", 
fzreexp_amx_stmt_date date "yyyymmdd",
fzreexp_amx_export_date date "yyyymmdd",
fzreexp_inv_due_date date "yyyymmdd",
fzreexp_inv_desc,fzreexp_foapl,fzreexp_trans_date date "yyyymmdd",
fzreexp_item_no, fzreexp_comm_desc, fzreexp_index, 
fzreexp_acct_code, fzreexp_appr_amt,fzreexp_bank_code,
fzreexp_amx_chid,fzreexp_status constant 'N')
