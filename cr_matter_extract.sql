set define off;
set echo off;
set head off;
set feedback off;
set trimspool on;
set pagesize 0;
set linesize 500;

spool MatterExtract.lis;

/* The Extract */

select the_data.dataline 
from (
select 1, 'MatterNumber'||'|'||
'ClientNumber'||'|'||
'ParentClientNumber'||'|'||
'ClientName'||'|'||
'MatterName'||'|'||
'Type'||'|'||
'RouteIndependent'||'|'||
'ClosedDate'||'|'||
'GLAccount'||'|'||
'Entity1'||'|'||
'Entity2'||'|'||
'Entity3'||'|'||
'LanguageCode_639-1'||'|'||
'LanguageCode_639-2'||'|'||
'CurrencyCode'||'|'||
'UDF1'||'|'||
'UDF2'||'|'||
'UDF3'||'|'||
'Project'||'|'||
'UDF1PersonID'||'|'||
'UDF2PersonID'||'|'||
'UDF3PersonID'||'|'||
'ExtraLineItemData1'  as dataline
from dual
union
select 2, ftvacci_acci_code||'|'||
ftvacci_fund_code ||'|'||
null ||'|'||
ftvfund_title ||'|'||
ftvacci_title ||'|'||
'NON-GRANT' ||'|'||
null ||'|'||
to_char(ftvacci_term_date,'mm/dd/yyyy') ||'|'||
ftvacci.FTVACCI_FUND_CODE||'-'||ftvacci.ftvacci_orgn_code||'-'||ftvacci.ftvacci_prog_code ||'|'||
--ftvacci_orgn_code ||'|'||
null ||'|'||
null ||'|'||
null ||'|'||
null ||'|'||
null ||'|'||
null ||'|'||
null ||'|'||
null ||'|'||
null ||'|'||
null ||'|'||
cr_3k_appr_uni ||'|'||
cr_25k_appr_uni ||'|'||
cr_100k_appr_uni ||'|'||
null 
from ftvacci, ftvfund, ftvftyp,tcapp.fin_chrome_river_role_vw 
where ftvacci_fund_code = ftvfund_fund_code
and trunc(ftvacci_nchg_date) = trunc(to_date('31-DEC-2099','dd-mon-yyyy'))
and trunc(ftvfund_nchg_date) = trunc(to_date('31-DEC-2099','dd-mon-yyyy'))
and ftvacci_coas_code = '1'
and ftvfund_coas_code = '1'
and nvl(trunc(ftvacci_term_date), trunc(sysdate)) >= trunc(sysdate)
and nvl(trunc(ftvfund_term_date), trunc(sysdate)) >= trunc(sysdate)
and ftvacci_status_ind = 'A'
and ftvfund_status_ind = 'A'
and ftvfund_ftyp_code = ftvftyp_ftyp_code
and ftvftyp_coas_code = '1'
and trunc(ftvftyp_nchg_date) = trunc(to_date('31-DEC-2099','dd-mon-yyyy'))
and ftvftyp_status_ind = 'A'
and nvl(trunc(ftvfund_term_date), trunc(sysdate)) >= trunc(sysdate) 
and substr(ftvacci_acci_code,1,1) in ('1','2','6','7')
and cr_index = ftvacci_acci_code
union
select 2, ftvacci_acci_code ||'|'|| 
ftvacci_fund_code ||'|'||
null ||'|'||
ftvfund_title ||'|'||
ftvacci_title||'|'||
'GRANT' ||'|'||
null ||'|'||
to_char(ftvacci_term_date,'mm/dd/yyyy')||'|'||
ftvacci.FTVACCI_FUND_CODE||'-'||ftvacci.ftvacci_orgn_code||'-'||ftvacci.ftvacci_prog_code ||'|'||
--ftvacci_orgn_code ||'|'||
null ||'|'||
null ||'|'||
null ||'|'||
null||'|'||
null ||'|'||
null ||'|'||
null ||'|'||
null ||'|'||
null ||'|'||
null ||'|'||
cr_3k_appr_uni ||'|'||
cr_25k_appr_uni ||'|'||
cr_100k_appr_uni ||'|'||
null 
from ftvacci, ftvfund, ftvftyp, tcapp.fin_chrome_river_role_vw, frbgrnt
where ftvacci_fund_code = ftvfund_fund_code
--and trunc(ftvacci_nchg_date) = trunc(to_date('31-DEC-2099','dd-mon-yyyy'))
and trunc(sysdate) between trunc(ftvacci_eff_date) and trunc(ftvacci_nchg_date)
and trunc(ftvfund_nchg_date) = trunc(to_date('31-DEC-2099','dd-mon-yyyy'))
and ftvacci_coas_code = '1'
and ftvfund_coas_code = '1'
and nvl(trunc(ftvacci_term_date), trunc(sysdate)) >= trunc(sysdate)
and nvl(trunc(ftvfund_term_date), trunc(sysdate)) >= trunc(sysdate)
and ftvacci_status_ind = 'A'
and ftvfund_status_ind = 'A'
and ftvfund_ftyp_code = ftvftyp_ftyp_code
and ftvftyp_coas_code = '1'
and trunc(ftvftyp_nchg_date) = trunc(to_date('31-DEC-2099','dd-mon-yyyy'))
and ftvftyp_status_ind = 'A'
and nvl(trunc(ftvfund_term_date), trunc(sysdate)) >= trunc(sysdate)
and ftvfund_grnt_code = FRBGRNT.FRBGRNT_CODE
and ftvacci_acci_code = cr_index
union
/*Union select which selects those index/funds that have terminated
over 90 days ago */
select 2,ftvacci_acci_code ||'|'||
ftvacci_fund_code||'|'||
null||'|'||
ftvfund_title ||'|'||
ftvacci_title||'|'||
'NON-GRANT'||'|'||
null ||'|'||
to_char(ftvacci_term_date,'mm/dd/yyyy') ||'|'||
ftvacci.FTVACCI_FUND_CODE||'-'||ftvacci.ftvacci_orgn_code||'-'||ftvacci.ftvacci_prog_code ||'|'||
--ftvacci_orgn_code ||'|'||
null ||'|'||
null ||'|'||
null ||'|'||
null ||'|'||
null ||'|'||
null ||'|'||
null ||'|'||
null ||'|'||
null ||'|'||
null ||'|'||
cr_3k_appr_uni||'|'||
cr_25k_appr_uni ||'|'||
cr_100k_appr_uni ||'|'||
null
from ftvacci, ftvfund, ftvftyp,tcapp.fin_chrome_river_role_vw 
where ftvacci_fund_code = ftvfund_fund_code
and trunc(ftvacci_nchg_date) = trunc(to_date('31-DEC-2099','dd-mon-yyyy'))
and trunc(ftvfund_nchg_date) = trunc(to_date('31-DEC-2099','dd-mon-yyyy'))
and ftvacci_coas_code = '1'
and ftvfund_coas_code = '1'
and nvl(trunc(ftvacci_term_date), trunc(sysdate-90)) <= trunc(sysdate-90)
and ftvacci_status_ind = 'A'
and ftvfund_status_ind = 'A'
and ftvfund_ftyp_code = ftvftyp_ftyp_code
and ftvftyp_coas_code = '1'
and trunc(ftvftyp_nchg_date) = trunc(to_date('31-DEC-2099','dd-mon-yyyy'))
and ftvftyp_status_ind = 'A'
and substr(ftvacci_acci_code,1,1) in ('1','2','6','7')
and ftvacci_acci_code = cr_index
union
select 2, ftvacci_acci_code ||'|'||
ftvacci_fund_code||'|'||
null ||'|'||
ftvfund_title ||'|'||
ftvacci_title||'|'||
'GRANT'||'|'||
null ||'|'||
to_char(ftvacci_term_date,'mm/dd/yyyy')||'|'||
ftvacci.FTVACCI_FUND_CODE||'-'||ftvacci.ftvacci_orgn_code||'-'||ftvacci.ftvacci_prog_code||'|'||
--ftvacci_orgn_code||'|'||
null ||'|'||
null||'|'||
null||'|'||
null||'|'||
null||'|'||
null||'|'||
null||'|'||
null||'|'||
null||'|'||
null||'|'||
cr_3k_appr_uni||'|'||
cr_25k_appr_uni||'|'||
cr_100k_appr_uni||'|'||
null 
from ftvacci, ftvfund, ftvftyp, tcapp.fin_chrome_river_role_vw, frbgrnt
where ftvacci_fund_code = ftvfund_fund_code
--and trunc(ftvacci_nchg_date) = trunc(to_date('31-DEC-2099','dd-mon-yyyy'))
and trunc(sysdate) between trunc(ftvacci_eff_date) and trunc(ftvacci_nchg_date)
and trunc(ftvfund_nchg_date) = trunc(to_date('31-DEC-2099','dd-mon-yyyy'))
and ftvacci_coas_code = '1'
and ftvfund_coas_code = '1'
and ftvacci_status_ind = 'A'
and ftvfund_status_ind = 'A'
and ftvfund_ftyp_code = ftvftyp_ftyp_code
and ftvftyp_coas_code = '1'
and trunc(ftvftyp_nchg_date) = trunc(to_date('31-DEC-2099','dd-mon-yyyy'))
and ftvftyp_status_ind = 'A'
and ftvfund_grnt_code = FRBGRNT.FRBGRNT_CODE
and nvl(trunc(ftvacci_term_date),trunc(sysdate-90)) <= trunc(sysdate-90)
and ftvacci_acci_code = cr_index
order by 1)the_data;
spool off;
exit;
