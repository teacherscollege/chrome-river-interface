
  CREATE OR REPLACE VIEW "TCAPP"."CR_100K_APPR_VW" ("CR_100K_APPR_PIDM", "CR_100K_APPR_UNI", "CR_100K_APPR_NAME", "CR_100K_APPR_INDEX", "CR_100K_APPR_ROLE") AS 
  select spriden_pidm as "100K_appr_pidm",
       spriden_id as "100K_appr_uni",
       spriden_last_name||', '||spriden_first_name as "100K_appr_name",
       a.fzrappr_index as "100K_appr_index",
       a.fzrappr_sys_role as "100K_appr_role"
  from fzrappr a, spriden
  where a.fzrappr_pidm = spriden_pidm
  and spriden_ntyp_code = 'UNI'
  and a.fzrappr_system = 'CHROMERVR'
  and a.fzrappr_sys_role = '100KAPPR'
  and a.fzrappr_exp_date = (select min(fzrappr_exp_date)
                                    from fzrappr b
                                    where b.fzrappr_index = a.fzrappr_index
                                      and b.fzrappr_system = 'CHROMERVR'
                                      and b.fzrappr_sys_role = '100KAPPR'
                                      and trunc(b.fzrappr_exp_date) > trunc(sysdate));